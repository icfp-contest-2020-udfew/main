// Copyright 2020 Aras Ergus

// This file is part of icfp-contest-mmxx-udfew.
//
// icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


function bF(f) {
  function bF1(g) {
    function bF2(x) {
      return f(g(x))
    }
    return bF2;
  }
  return bF1;
};

function cF(f) {
  function cF1(x) {
    function cF2(y) {
      return f(y)(x);
    }
    return cF2;
  }
  return cF1;
};

function sF(f) {
  function sF1(g) {
    function sF2(x) {
      return f(x)(g(x));
    }
    return sF2;
  }
  return sF1;
};

tF = true;
fF = false;

function carF(l) {
  return l[0];
};

function cdrF(l) {
  return l.slice(1, l.length);
};

function consF(x) {
  function consF1(xs) {
    return [x].concat(xs);
  }
  return consF1;
};

var nilF = [];

function isNilF(x) {
  return x.length == 0;
};

function divF(x) {
  function divF1(y) {
    return Math.sign(x) * Math.sign(y) * Math.floor(Math.abs(x) / Math.abs(y));
  }
  return divF1;
};

function eqF(x) {
  function eqF1(y) {
    return x == y;
  }
  return eqF1;
};

function lTF(x) {
  function ltF1(y) {
    return x < y;
  }
  return ltF1;
};

function mulF(x) {
  function mulF1(y) {
    return x * y;
  }
  return mulF1;
};

function addF(x) {
  function addF1(y) {
    return x + y;
  }
  return addF1;
};

function negF(x) {
  return -x;
};

function iF(x) {
  return x;
};

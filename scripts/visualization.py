#!/usr/bin/python3

# Copyright 2020 Bernhard Reinke, Sílvia Cavadas

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import math
import pygame
import sys

from collections import Counter

pygame.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# TODO: fix general convention
def parseLine(line):
    return eval(line) # TODO: hack at the moment
    
def redraw_single(ps, scale, xmin, xmax, ymin, ymax):
    screen.fill(BLACK)
    draw_picture(ps, scale, xmin, xmax, ymin, ymax, WHITE)
    pygame.display.flip()

def draw_picture(ps, scale, xmin, xmax, ymin, ymax, color):
    for x, y in ps:
        pygame.draw.rect(screen, color, [(x-xmin + 1)*scale, (y-ymin + 1)*scale, scale, scale])


def redraw_blended(c, scale, xmin, xmax, ymin, ymax):
    screen.fill(BLACK)
    m = c.most_common()[0][1]
    for p, num in c.items(): 
        x, y = p
        intensity = int(255*num / m)
        color = (intensity, intensity, intensity)
        pygame.draw.rect(screen, color, [(x-xmin + 1)*scale, (y-ymin + 1)*scale, scale, scale])
    pygame.display.flip()

def mergepixels(pixels):
    return Counter([p for ps in pixels for p in ps])

def bounds(pixels):
    xmin = min(x for ps in pixels for x, y in ps)
    xmax = max(x for ps in pixels for x, y in ps)
    ymin = min(y for ps in pixels for x, y in ps)
    ymax = max(y for ps in pixels for x, y in ps)
    return xmin, xmax, ymin, ymax

def transform(pos, scale, xmin, xmax, ymin, ymax):
    x, y = pos
    return math.floor(x / scale + xmin - 1), math.floor(y / scale + ymin - 1)

for line in sys.stdin:
    mode = 0 #0=blended, 1=single
    pixels = parseLine(line)
    c = mergepixels(pixels)
    xmin, xmax, ymin, ymax = bounds(pixels)
    scale = 5 # TODO: hardcoded
    frame = 0
    screen = pygame.display.set_mode(((xmax-xmin + 3)*scale,(ymax - ymin + 3)*scale))
    redraw_blended(c, scale, xmin, xmax, ymin, ymax) 
    while True:
        e = pygame.event.wait()
        if e.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
            print(transform(pygame.mouse.get_pos(), scale, xmin, xmax, ymin, ymax))
        if e.type == pygame.KEYDOWN and e.key == pygame.K_RETURN:
            break
        if e.type == pygame.KEYDOWN and e.key == pygame.K_s:
            mode = (mode +1)%2
            if mode == 0:
                redraw_blended(c, scale, xmin, xmax, ymin, ymax)
            elif mode == 1:
                redraw_single(pixels[0], scale, xmin, xmax, ymin, ymax)
        if e.type == pygame.KEYDOWN and e.key == pygame.K_RIGHT and mode == 1:
            frame +=1
            frame %= len(pixels)
            redraw_single(pixels[frame], scale, xmin, xmax, ymin, ymax)
        if e.type == pygame.KEYDOWN and e.key == pygame.K_LEFT and mode == 1:
            frame += len(pixels)-1
            frame %= len(pixels)
            redraw_single(pixels[frame], scale, xmin, xmax, ymin, ymax)

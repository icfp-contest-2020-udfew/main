#!/bin/bash

# Copyright 2020 Mark Pedron <marktpedron@gmail.com>

# Copyright 2020 Tashi Walde

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

set -eu

cd -- "$(dirname "$(readlink -f -- "$0")")" || \
  { >&2 echo "Failed to change to script directory" ; exit 1 ; }

logfile=play.log

if [ -z "${API_KEY:-}" ]
then
  >&2 echo "Please set your api key via 'export API_KEY=<key>' before executing this script"
  exit 1
fi

if [ -z "${GALAXY_FILE:-}" ]
then
  >&2 echo "Please set the path of your galaxy.txt file via 'export GALAXY_FILE=</path/to/file>' before executing this script"
  exit 1
fi

uibin="${1:-./ui-stub}"
galaxybin="${2:-run-galaxy $GALAXY_FILE}"

function log {
  >>"$logfile" echo "$(date +%Y-%M-%dT%H:%M:%S)" $@
}

function ui {
  local data=$1
  log "Calling $uibin { data: \"$data\" }"
  $uibin $data
}

function galaxy {
  local state=$1
  local vector=$2
  log "Calling $galaxybin { state: \"$state\", vector: \"$vector\" }"
  $galaxybin $state $vector
}

function send {
  local data=$1
  log "Calling curl { data: \"$data\" }"
  curl -X POST "https://icfpc2020-api.testkontur.ru/aliens/send?apiKey=$API_KEY" -H "accept: */*" -H "Content-Type: text/plain" -d "$data" --silent
}

state="00"
vector="11010010"
flag="0"

while true
do
  flagstatedata=$(galaxy $state $vector | paste -sd " " -)
  flag=$(echo $flagstatedata | cut -d' ' -f1)
  state=$(echo $flagstatedata | cut -d' ' -f2)
  data=$(echo $flagstatedata | cut -d' ' -f3)
  if [ "$flag" == 010 ]
  then
    vector=$(ui $data)
  else
    vector=$(send $data)
  fi
done

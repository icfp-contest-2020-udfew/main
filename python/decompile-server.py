#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Web interface for the decompiler
"""


import os
import sys
import argparse
import logging
import time
import signal
import bottle

import udfew.alienmessages.decompiler


argparser = argparse.ArgumentParser(description='Runs a web server with a decompiler interface for'
                                                ' alien assembly')
argparser.add_argument('--input', help='Input file with alien disassembly', required=True)
argparser.add_argument('--port', help='Port for the server', required=True)


args = argparser.parse_args()
logging.basicConfig(stream=sys.stderr, level=logging.WARN)


dir_path = os.path.dirname(os.path.realpath(__file__))
web_path = os.path.join(dir_path, 'decompiler-web/')

VarRHSNotImpl = udfew.alienmessages.decompiler.VariableOnRightHandSideNotImplemented

def timeout_signal_handler(signum, frame):
    raise Exception('Execution took too long')
signal.signal(signal.SIGALRM, timeout_signal_handler)

with open(args.input, 'r') as filehandle:
    file_content = filehandle.read()

assembly = udfew.alienmessages.decompiler.Assembly(file_content)

#def print_parse(line):
#    '''Prints the parsed line, or reports failure'''
#    if timeout > 0:
#        signal.alarm(timeout)
#    time_start = time.perf_counter()
#    try:
#        result = assembly.parse_line_decompiled_string(line)
#    except VarRHSNotImpl:
#        time_end = time.perf_counter()
#        time_elapsed = time_end - time_start
#        print(f'Line {line:3} {time_elapsed}s: FAILED - Variable use on RHS')
#    except: # pylint: disable=bare-except
#        time_end = time.perf_counter()
#        time_elapsed = time_end - time_start
#        print(f'Line {line:3} {time_elapsed}s: FAILED')
#        if args.raise_on_failure:
#            raise
#    else:
#        time_end = time.perf_counter()
#        time_elapsed = time_end - time_start
#        if args.check_success:
#            print(f'Line {line:3} {time_elapsed}s: SUCCESS')
#        else:
#            print(f'Line {line:3} {time_elapsed}s:\n    {result}')

bottle.TEMPLATE_PATH.append(web_path)
filename = os.path.basename(args.input)
number_of_lines = len(file_content.splitlines())

@bottle.route('/')
def index():
    return bottle.template('index', filename=filename, lines=number_of_lines)

@bottle.route('/decompiler.js')
def javascript():
    return bottle.static_file('decompiler.js', root=web_path)

@bottle.route('/api/initial')
def initial():
    response = { 'lines': {} }
    # FIXME: handle the last line lhs
    for i in range(0, number_of_lines - 1):
        response['lines'][i] = {}
        response['lines'][i]['name'] = assembly.get_lhs_global_name(i)
    return response

port = args.port
bottle.run(host='localhost', port=port, debug=True)

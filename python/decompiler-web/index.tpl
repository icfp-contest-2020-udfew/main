<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Alien Assembly Decompiler - {{filename}}</title>
  </head>
  <body>
    <h1>Alien Assembly Decompiler: {{filename}}</h1>
% for i in range(0, lines):
    <hr>
    <div name="line{{i}}">
    </div>
% end
  <script src="decompiler.js"></script>   
  </body>
</html>

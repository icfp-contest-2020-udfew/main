// Copyright 2020 Malte Leip <malte@leip.net>

// This file is part of icfp-contest-mmxx-udfew.
//
// icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


function request(path)
{
  var httpRequest;
  function make_request()
  {
    httpRequest = new XMLHttpRequest();
    if(!httpRequest)
    {
      alert("Problem creating XMLHttpRequest!");
    }
    httpRequest.onreadystatechange = handle_response;
    httpRequest.open('GET', path);
    httpRequest.send();
  }
  function handle_response()
  {
    if(httpRequest.readyState == XMLHttpRequest.DONE)
    {
      if(httpRequest.status == 200)
      {
        var response = JSON.parse(httpRequest.responseText);
        update(response);
      }
      else
      {
        console.error('Got status code ' + httpRequest.status + " on request for path " + path);
      }
    }
  }
  make_request();
}

function update(data)
{
  for(var update_type in data)
  {
    if(update_type === "lines")
    {
      console.log("Got updates for lines.")
      for(var line_number in data.lines)
      {
        update_line(line_number, data.lines[line_number]);
      }
    }
    else
    {
      console.error("Unrecognized update type: " + update_type);
    }
  }
}

function update_line(line_number, data)
{
  console.log("Refreshing line " + line_number);
  var line_element = document.querySelector('div[name=line' + line_number + ']');
  line_element.innerHTML = '';

  // Line number
  var line_number_element = document.createElement('div');
  line_number_element.classList.add('line-number');
  var line_number_text_element = document.createTextNode('Line ' + line_number + ':');
  line_number_element.append(line_number_text_element);
  line_element.append(line_number_element);

  // Left hand side, name of global variable that is being defined
  var lhs_element = document.createElement('div');
  lhs_element.classList.add('global-variable');
  var lhs_text_element = document.createTextNode(data['name']);
  lhs_element.append(lhs_text_element);
  line_element.append(lhs_element);
}

function initial_load()
{
  request('api/initial')
}

initial_load();

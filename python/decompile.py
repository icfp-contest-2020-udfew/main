#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Load alien assembly and decompile
"""


import sys
import argparse
import logging
import time
import signal

import udfew.alienmessages.decompiler


argparser = argparse.ArgumentParser(description='Decompiles alien assembly')
argparser.add_argument('--debug', help='Output debug information', action='store_true')
argparser.add_argument('--info', help='Output info information', action='store_true')
argparser.add_argument('--input', help='Input file with alien disassembly', required=True)
argparser.add_argument('--output-sugared', help='Write full input file in sugared version')
argparser.add_argument('--line', help='Single line number to decompile')
argparser.add_argument('--up-to', help='Decompile up to that line')
argparser.add_argument('--start-from', help='Decompile from that line')
argparser.add_argument('--all', help='Decompile all lines', action='store_true')
argparser.add_argument('--check-success', help='Only show failed/succeeded', action='store_true')
argparser.add_argument('--raise-on-failure', help='Raise exception if thrown by decompiler',
                       action='store_true')
argparser.add_argument('--timeout', help='How many seconds per line before stopping')


args = argparser.parse_args()
if args.debug:
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
elif args.info:
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
else:
    logging.basicConfig(stream=sys.stderr, level=logging.WARN)

VarRHSNotImpl = udfew.alienmessages.decompiler.VariableOnRightHandSideNotImplemented

timeout = 0
if args.timeout is not None:
    timeout = int(args.timeout)

def timeout_signal_handler(signum, frame):
    raise Exception('Execution took too long')
signal.signal(signal.SIGALRM, timeout_signal_handler)

with open(args.input, 'r') as filehandle:
    assembly = udfew.alienmessages.decompiler.Assembly(filehandle.read())
    if args.output_sugared is not None:
        assembly.sugar_file(args.output_sugared)

    def print_parse(line):
        '''Prints the parsed line, or reports failure'''
        if timeout > 0:
            signal.alarm(timeout)
        time_start = time.perf_counter()
        try:
            result = assembly.parse_line_decompiled_string(line)
        except VarRHSNotImpl:
            time_end = time.perf_counter()
            time_elapsed = time_end - time_start
            print(f'Line {line:3} {time_elapsed}s: FAILED - Variable use on RHS')
        except: # pylint: disable=bare-except
            time_end = time.perf_counter()
            time_elapsed = time_end - time_start
            print(f'Line {line:3} {time_elapsed}s: FAILED')
            if args.raise_on_failure:
                raise
        else:
            time_end = time.perf_counter()
            time_elapsed = time_end - time_start
            if args.check_success:
                print(f'Line {line:3} {time_elapsed}s: SUCCESS')
            else:
                print(f'Line {line:3} {time_elapsed}s:\n    {result}')

    start = 0 # pylint: disable=invalid-name
    end = len(assembly.lines)
    if args.up_to is not None:
        end = int(args.up_to) + 1
    if args.start_from is not None:
        start = int(args.start_from)
    if args.all or args.up_to is not None or args.start_from is not None:
        for i in range(start, end):
            print_parse(i)
    elif args.line is not None:
        print_parse(int(args.line))

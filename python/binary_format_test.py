#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# Copyright 2020 Sílvia Cavadas

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Test binary encoding
"""


import sys
import logging


import udfew.alienmessages.binary


logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

def show_binary_encoding(data):
    '''Output data and its binary encoding'''
    binary_data = udfew.alienmessages.binary.BinaryData(from_data=data)
    print(f'{data} encodes to {binary_data.binary_string}')

def show_decoded_data(binary_string):
    '''Output binary data and its decoding to Python types'''
    binary_data = udfew.alienmessages.binary.BinaryData(from_binary_string=binary_string)
    print(f'{binary_string} decodes to {binary_data.data}')

show_binary_encoding(0)
show_binary_encoding(1)
show_decoded_data("010")
show_decoded_data("10100001")

# Copyright 2020 Malte Leip

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


'''Tests for the udfew.alienmessages.geometry module'''

import os
import pathlib
import unittest

TOP_DIR = pathlib.Path(__file__)
TOP_DIR = TOP_DIR.parent.joinpath("../../")
TOP_DIR = TOP_DIR.resolve()
os.sys.path.append(str(TOP_DIR))


from udfew.alienmessages.geometry import Rectangle # pylint: disable=import-error,wrong-import-position


class RectangleTestCase(unittest.TestCase):
    '''Tests for the udfew.alienmessages.geometry.Rectangle class'''
    def test_bool(self):
        '''Rectangle class to bool conversion'''
        self.assertTrue(Rectangle((-1, -1), (0, 0)))
        self.assertTrue(Rectangle((5, 17), (7, 19)))
        self.assertFalse(Rectangle())

    def test_join(self):
        '''Rectangle & Rectangle calculating the intersection'''
        self.assertEqual(Rectangle((0, 0), (1, 1)) | Rectangle((10, 10), (12, 13)),
                         Rectangle((0, 0), (12, 13)))
        self.assertEqual(Rectangle((0, 0), (10, 10)) | Rectangle((3, 3), (3, 13)),
                         Rectangle((0, 0), (10, 13)))
        self.assertEqual(Rectangle((0, 0), (1, 1)) | Rectangle((-2, -2), (-1, 0)),
                         Rectangle((-2, -2), (1, 1)))
        self.assertEqual(Rectangle((0, 0), (1, 1)) | Rectangle((3, -4), (5, -2)),
                         Rectangle((0, -4), (5, 1)))
        self.assertEqual(Rectangle((0, 0), (1, 1)) | Rectangle((-10, 10), (-5, 12)),
                         Rectangle((-10, 0), (1, 12)))

    def test_intersect(self):
        '''Rectangle | Rectangle calculating the smallest rectangle containing both'''
        self.assertEqual(Rectangle((0, 0), (1, 1)) & Rectangle((1, 1), (2, 2)),
                         Rectangle((1, 1), (1, 1)))
        self.assertEqual(Rectangle((0, 0), (10, 10)) & Rectangle((3, 3), (3, 13)),
                         Rectangle((3, 3), (3, 10)))
        self.assertEqual(Rectangle((0, 0), (1, 1)) & Rectangle((-2, -2), (-1, 0)),
                         Rectangle())

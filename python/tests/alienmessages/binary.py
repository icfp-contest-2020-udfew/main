# Copyright 2020 Malte Leip, Sílvia Cavadas

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


'''Tests for the udfew.alienmessages.geometry module'''


import os
import pathlib
import unittest

TOP_DIR = pathlib.Path(__file__)
TOP_DIR = TOP_DIR.parent.joinpath("../../")
TOP_DIR = TOP_DIR.resolve()
os.sys.path.append(str(TOP_DIR))


from udfew.alienmessages.binary import BinaryData # pylint: disable=import-error,wrong-import-position


class BinaryDataTestCase(unittest.TestCase):
    '''Tests for the udfew.alienmessages.binary.BinaryData class'''
    def test_to_string(self):
        '''Tests to_string conversion'''
        binary_data = BinaryData()
        binary_data.add_bits([True, False, False])
        self.assertEqual(binary_data.binary_string, '100')

        binary_data = BinaryData()
        binary_data.add_bits([True, False, False, False, True, True])
        self.assertEqual(binary_data.binary_string, '100011')

    def test_encode(self):
        '''Tests encoding of integers'''
        binary_data = BinaryData(from_data=1)
        self.assertEqual(binary_data.binary_string, '01100001')
        binary_data = BinaryData(from_data=15)
        self.assertEqual(binary_data.binary_string, '01101111')
        binary_data = BinaryData(from_data=16)
        self.assertEqual(binary_data.binary_string, '0111000010000')
        binary_data = BinaryData(from_data=(15, (15, ())))
        self.assertEqual(binary_data.binary_string, '1101101111110110111100')
        binary_data = BinaryData(from_data=())
        self.assertEqual(binary_data.binary_string, '00')

    def test_encode_decode_int(self):
        '''Test encoding-decoding ints is identity'''
        for i in range(-20, 20):
            binary_data = BinaryData(from_data=i)
            binary_data2 = BinaryData(from_binary_string=binary_data.binary_string)
            self.assertEqual(binary_data2.data, i)

    def test_encode_decode_tuple(self):
        '''Test encoding-decoding tuple is identity'''
        from_datas = [((14, ())), 15]
        for from_data in from_datas:
            binary_data = BinaryData(from_data=from_data)
            binary_data2 = BinaryData(from_binary_string=binary_data.binary_string)
            self.assertEqual(binary_data2.data, from_data)


    def test_decode(self):
        '''Tests encoding of integers'''
        binary_data = BinaryData(from_binary_string="010")
        self.assertEqual(binary_data.data, 0)
        binary_data = BinaryData(from_binary_string="00")
        self.assertEqual(binary_data.data, ())
        binary_data = BinaryData(from_binary_string="0111000010000")
        self.assertEqual(binary_data.data, 16)
        binary_data = BinaryData(from_binary_string="11010010")
        self.assertEqual(binary_data.data, (0, 0))

#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Contains classes to handle the alien messages
"""


import logging
import functools
import PIL.Image
import PIL.ImageDraw


from . import geometry


PIXEL_EMPTY = 'PIXEL_EMPTY'
PIXEL_FILLED = 'PIXEL_FILLED'


class AlienCharacter:
    '''A character *in* an image'''
    def __init__(self, image, rectangle_with_border):
        '''image: AlienImage instance, rectangle_with_border: what it says'''
        self._image = image
        self._shift = (rectangle_with_border.upper_left[0] + 1,
                       rectangle_with_border.upper_left[1] + 1)
        self.width = rectangle_with_border.lower_right[0] - rectangle_with_border.upper_left[0] - 1
        self.height = rectangle_with_border.lower_right[1] - rectangle_with_border.upper_left[1] - 1

    @functools.lru_cache(maxsize=None)
    def getpixel(self, x_coordinate, y_coordinate):
        '''The coordinates are character-internally and don't include the empty border'''
        return self._image.getpixel(x_coordinate + self._shift[0],
                                    y_coordinate + self._shift[1])

    @functools.cached_property
    def is_number(self):
        '''Whether the character represents a number'''
        if self.getpixel(0, 0) != PIXEL_EMPTY:
            return False
        if self.height not in [self.width, self.width + 1]:
            return False
            # raise ValueError("Character looks like number but unexpected top and left border")
        for i in range(1, self.width):
            if self.getpixel(0, i) != PIXEL_FILLED or self.getpixel(i, 0) != PIXEL_FILLED:
                return False
                #raise ValueError("Character looks like number but unexpected top and left border:"
                #                 f"i={i}, width={self.width}")
        return True

    @functools.cached_property
    def is_non_number(self):
        '''Whether the character represents a non-number'''
        return not self.is_number

    @functools.cached_property
    def is_variable(self): # pylint: disable=too-many-return-statements
        '''Whether the character represents a variable'''
        for x_coordinate in range(0, self.width):
            if self.getpixel(x_coordinate, 0) != PIXEL_FILLED:
                return False
            if self.getpixel(x_coordinate, self.width - 1) != PIXEL_FILLED:
                return False
        for y_coordinate in range(0, self.height):
            if self.getpixel(0, y_coordinate) != PIXEL_FILLED:
                return False
            if self.getpixel(self.height - 1, y_coordinate) != PIXEL_FILLED:
                return False
        # Now check that the interior is the negative of a number
        if self.width < 4 or self.height < 4:
            return False
        if self.getpixel(1, 1) != PIXEL_FILLED:
            return False
        for x_coordinate in range(2, self.width - 1):
            if self.getpixel(x_coordinate, 1) != PIXEL_EMPTY:
                return False
        for y_coordinate in range(2, self.height - 1):
            if self.getpixel(1, y_coordinate) != PIXEL_EMPTY:
                return False
        return True

    @functools.lru_cache(maxsize=None)
    def number_value(self, override=False, variable_mode=False):
        '''Value of the number'''
        if not self.is_number and not override:
            raise ValueError("Trying to get number value of a character that is not a number!")
        check_for = PIXEL_FILLED
        border = 0
        if variable_mode:
            check_for = PIXEL_EMPTY
            border = 1
        current_power_of_two = 1
        value = 0
        for y_coordinate in range(1 + border, self.height - border):
            for x_coordinate in range(1 + border, self.width - border):
                if self.getpixel(x_coordinate, y_coordinate) == check_for:
                    value += current_power_of_two
                current_power_of_two *= 2
        if self.height > self.width:
            value *= -1
        return value

    @functools.cached_property
    def is_ellipsis(self):
        '''Whether the character is the . . . . ellipsis'''
        if not self.height == 1:
            return False
        if not self.width == 7:
            return False
        for x_coordinate in range(0, self.width):
            if x_coordinate % 2 == 0 and self.getpixel(x_coordinate, 0) != PIXEL_FILLED:
                return False
            if x_coordinate % 2 == 1 and self.getpixel(x_coordinate, 0) != PIXEL_EMPTY:
                return False
        return True

    def __str__(self): # pylint: disable=too-many-return-statements, too-many-branches
        '''Return string interpretation of the character'''
        if self.is_number:
            return str(self.number_value())
        if self.is_ellipsis:
            return "[...]"
        if self.is_variable:
            num = self.number_value(override=True, variable_mode=True)
            return f"VAR[{num}]"
        if self.is_non_number:
            if self.number_value(override=True) == 12 and self.width == 3 and self.height == 3:
                return "="
            if self.number_value(override=True) == 0 and self.width == 2 and self.height == 2:
                return "AP"
            if self.number_value(override=True) == 417 and self.width == 4 and self.height == 4:
                return "(λx: x+1)"
            if self.number_value(override=True) == 401 and self.width == 4 and self.height == 4:
                return "(λx: x-1)"
            if self.number_value(override=True) == 365 and self.width == 4 and self.height == 4:
                return "(λx,y: x+y)"
            if self.number_value(override=True) == 146 and self.width == 4 and self.height == 4:
                return "(λx,y: x*y)"
            if self.number_value(override=True) == 40 and self.width == 4 and self.height == 4:
                return "(λx: (λy: x // y))"
            if self.number_value(override=True) == 2 and self.width == 3 and self.height == 3:
                return "True/first"
            if self.number_value(override=True) == 8 and self.width == 3 and self.height == 3:
                return "False/second"
            if self.number_value(override=True) == 448 and self.width == 4 and self.height == 4:
                return "(λx,y: x==y)"
            if self.number_value(override=True) == 416 and self.width == 4 and self.height == 4:
                return "(λx: (λy: x<y))"
            if self.number_value(override=True) == 10 and self.width == 3 and self.height == 3:
                return "(λx: (-1)*x)"
            if self.number_value(override=True) == 7 and self.width == 3 and self.height == 3:
                return "(λx,y,z: x(y,y(z)))"
            if self.number_value(override=True) == 6 and self.width == 3 and self.height == 3:
                return "(λx,y,z: x(z,y))"
            if self.number_value(override=True) == 5 and self.width == 3 and self.height == 3:
                return "(λx,y,z: x(y(z)))"
            if self.number_value(override=True) == 5 and self.width == 3 and self.height == 3:
                return "(λx,y,z: x(y(z)))"
            if self.number_value(override=True) == 1 and self.width == 2 and self.height == 2:
                return "id"
            if self.number_value(override=True) == 64170 and self.width == 5 and self.height == 5:
                return "(λx,y,z: z,x,y)"
            if self.number_value(override=True) == 14 and self.width == 3 and self.height == 3:
                return "(λx: True)"
            if self.number_value(override=True) == -191 and self.width == 3 and self.height == 5:
                return "["
            if self.number_value(override=True) == -15 and self.width == 2 and self.height == 5:
                return ","
            if self.number_value(override=True) == -29 and self.width == 3 and self.height == 5:
                return "]"
            if (self.number_value(override=True) == 17043521
                    and self.width == 6 and self.height == 6):
                return "Coordinate"
            if (self.number_value(override=True) == 33047056
                    and self.width == 6 and self.height == 6):
                return "DrawPixelList"
            if (self.number_value(override=True) == 11184810
                    and self.width == 6 and self.height == 6):
                return "(λx,y: DrawChessboard(width=height=x))"
            if (self.number_value(override=True) == 68259412260
                    and self.width == 7 and self.height == 7):
                return "map DrawPixelList"
            if self.number_value(override=True) == 174 and self.width == 4 and self.height == 4:
                return "Oracle"
            if self.number_value(override=True) == 58336 and self.width == 5 and self.height == 5:
                return "(λx,y,z: if x==0 then y, if x==1 then z)"
            if self.number_value(override=True) == 341 and self.width == 4 and self.height == 4:
                return "Demodulate"
            if self.number_value(override=True) == 170 and self.width == 4 and self.height == 4:
                return "Modulate"

        return f"?[{self.number_value(override=True)},({self.width}, {self.height})]?"

    def text_image(self):
        '''Print out a text representation of the character image'''
        print(f"Character with top left pixel {self._shift} and "
              f"height {self.height} and width {self.width}")
        for y_coordinate in range(0, self.height):
            for x_coordinate in range(0, self.width):
                if self.getpixel(x_coordinate, y_coordinate) == PIXEL_FILLED:
                    print("#", end="")
                else:
                    print(" ", end="")
            print("")


class AlienImage:
    """Can load an image in the alien format and provides various decoding
    functionalities.
    """

    __slots__ = ['_image', '__dict__']
    def __init__(self, filename):
        logging.info('Loading image from file "%s"...', filename)
        self._image = PIL.Image.open(filename)

    @functools.cached_property
    def height(self):
        '''Height of the image in what we interpret as pixels, not of the original image'''
        return (self._image.height // self._zoom) - 2

    @functools.cached_property
    def width(self):
        '''Width of the image in what we interpret as pixels, not of the original image'''
        return (self._image.width // self._zoom) - 2

    @functools.cached_property
    def _zoom(self):
        def finalize_zoom(zoom):
            logging.info('Zoom level found to be %s', zoom)
            if (self._image.height % zoom != 0) or (self._image.width % zoom != 0):
                raise Exception('''Image dimensions not divisible by border
depth - failed to find zoom level.''')
            return zoom
        logging.debug('Calculating the zoom level...')
        border_depth = 0
        while True:
            logging.debug('Checking border depth of %s', border_depth + 1)
            # See whether we can increase the recognized border depth by one
            for x_coordinate in [border_depth, self._image.width - border_depth - 1]:
                for y_coordinate in range(border_depth, self._image.height - border_depth - 1):
                    if self._image.getpixel((x_coordinate, y_coordinate)) == (0, 0, 0, 255):
                        logging.debug('Not border at %s: %s',
                                      str((x_coordinate, y_coordinate)),
                                      str(self._image.getpixel((x_coordinate, y_coordinate))))
                        return finalize_zoom(border_depth)
            for y_coordinate in [border_depth, self._image.height - border_depth - 1]:
                for x_coordinate in range(border_depth, self._image.width - border_depth - 1):
                    if self._image.getpixel((x_coordinate, y_coordinate)) == (0, 0, 0, 255):
                        logging.debug('Not border at %s: %s',
                                      str((x_coordinate, y_coordinate)),
                                      str(self._image.getpixel((x_coordinate, y_coordinate))))
                        return finalize_zoom(border_depth)
            border_depth += 1

    @functools.lru_cache(maxsize=None)
    def getpixel(self, x_coordinate, y_coordinate):
        """Returns PIXEL_EMPTY or PIXEL_FILLED
        """
        pixel = self._image.getpixel((self._zoom * (x_coordinate + 1),
                                      self._zoom * (y_coordinate + 1)))
        if pixel == (255, 255, 255, 255):
            return PIXEL_FILLED
        if pixel == (0, 0, 0, 255):
            return PIXEL_EMPTY
        raise Exception("Image has an unexpected pixel value: {pixel}")

    @functools.cached_property
    def _filled_interior_pixels(self):
        '''Returns the filled pixels in the interior (that is, not including the boundary)'''
        filled_interior_pixels = set()
        for x_coordinate in range(0, self.width):
            for y_coordinate in range(0, self.height):
                if self.getpixel(x_coordinate, y_coordinate) == PIXEL_FILLED:
                    filled_interior_pixels.add((x_coordinate, y_coordinate))
        logging.debug('The following pixels in the interior are filled: %s',
                      str(filled_interior_pixels))
        return filled_interior_pixels

    @functools.cached_property
    def _character_bounding_boxes(self):
        '''Returns a set of bounding boxes of characters identified in the image.
        This box contains a one-pixel wide empty border around the character.
        Each element of the set is a Rectangle'''
        # Create a bounding box for every filled pixel
        bounding_boxes = list()
        for pixel in self._filled_interior_pixels:
            bounding_boxes.append(geometry.Rectangle((pixel[0] - 1, pixel[1] - 1),
                                                     (pixel[0] + 1, pixel[1] + 1)))

        # Now join intersecting bounding boxes

        # FIXME: This algorithm is O(n^3) I think. # pylint: disable=W0511
        # Is there a linear way of doing this?
        # TODO: At least implement some smaller speed improvements. # pylint: disable=W0511
        changed_something_last_pass = True
        while changed_something_last_pass:
            changed_something_last_pass = False
            for i, bounding_box_1 in enumerate(bounding_boxes):
                for bounding_box_2 in bounding_boxes[i + 1:]:
                    if bounding_box_1 & bounding_box_2:
                        logging.debug('Joining intersectiong bounding boxes '
                                      '%s and %s to %s (intersection was %s)',
                                      bounding_box_1, bounding_box_2,
                                      bounding_box_1 | bounding_box_2,
                                      bounding_box_1 & bounding_box_2)
                        bounding_boxes.remove(bounding_box_1)
                        bounding_boxes.remove(bounding_box_2)
                        bounding_boxes.append(bounding_box_1 | bounding_box_2)
                        changed_something_last_pass = True
                        break
                if changed_something_last_pass:
                    # We changed the list we are iterating over, so continueing
                    # would lead to problems
                    break
        return bounding_boxes

    @functools.cached_property
    def _bounding_boxes_ordered(self):
        '''Returns a list of lists of bounding boxes of characters,
        representing the image split up into lines.'''
        result = list()
        lines = self._lines_raw
        bounding_boxes = self._character_bounding_boxes
        while lines:
            lines_current = list()
            lines_current.append(lines.pop(0))
            while lines and lines_current and lines[0] == lines_current[-1] + 1:
                lines_current.append(lines.pop(0))
            # lines_current is the list of contiguous non-empty lines
            bounding_boxes_current = list()
            for bounding_box in bounding_boxes:
                if bounding_box.upper_left[1] + 1 in lines_current:
                    bounding_boxes_current.append(bounding_box)
            bounding_boxes = [x for x in bounding_boxes if x not in bounding_boxes_current]
            # bounding_boxes_current is the bounding boxes in the current line.
            # Now we need to order them.
            line_characters = list()
            for x_coordinate in range(0, self.width):
                bounding_boxes_current_this_x = \
                        [box for box in bounding_boxes_current if
                         box.upper_left[0] < x_coordinate < box.lower_right[0]]
                if len(bounding_boxes_current_this_x) > 1:
                    raise Exception("Characters in one logical horizontal line of text are \
                            not separated, i.e. there are two on top of each other vertically.")
                line_characters.extend(bounding_boxes_current_this_x)
                bounding_boxes_current = [box for box in bounding_boxes_current if
                                          box not in bounding_boxes_current_this_x]
            result.append(line_characters)
        return result

    @functools.cached_property
    def characters_ordered(self):
        '''List of lists of AlienCharacters, representing ordered lines'''
        lines_bounding_boxes = self._bounding_boxes_ordered
        result = list()
        for line in lines_bounding_boxes:
            converted_line = list()
            for box in line:
                converted_line.append(AlienCharacter(self, box))
            result.append(converted_line)
        return result

    @functools.cached_property
    def _lines_raw(self):
        '''Returns a list of lines that are not completely empty.'''
        lines = list()
        for y_coordinate in range(0, self.height):
            completely_empty = True
            logging.debug("Checking Line %s", str(y_coordinate))
            for x_coordinate in range(0, self.width):
                if self.getpixel(x_coordinate, y_coordinate) == PIXEL_FILLED:
                    completely_empty = False
                    logging.debug("Line %s is not completely empty because of pixel "
                                  "(%s, %s)",
                                  str(y_coordinate), str(x_coordinate), str(y_coordinate))
                    break
            if not completely_empty:
                lines.append(y_coordinate)
        return lines


    def save_image(self, filename, zoom=None, with_bounding_boxes=False):
        '''Saves the image to a file. The filled border is removed.'''
        if zoom is None:
            zoom = self._zoom
        # Note: As we remove the border around the image of width 1,
        # we have to be careful to subtract 1 to x / y coordinates everywhere.
        border = 0
        def rectangle_helper(upper_left, lower_right):
            return [(zoom * (upper_left[0] - border), zoom * (upper_left[1] - border)),
                    (zoom * (lower_right[0] - border + 1) - 1,
                     zoom * (lower_right[1] - border + 1) - 1)]

        image = PIL.Image.new('RGB', (zoom * self.width, zoom * self.height))
        draw = PIL.ImageDraw.Draw(image)
        for pixel in self._filled_interior_pixels:
            draw.rectangle(rectangle_helper(pixel, pixel),
                           fill="white", outline="white")

        if with_bounding_boxes:
            for bounding_box in self._character_bounding_boxes:
                draw.rectangle(rectangle_helper(bounding_box.upper_left, bounding_box.lower_right),
                               outline="blue")
        image.save(filename)

#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


"""Helper classes for geometry stuff
"""


class Rectangle:
    """A rectangle with integer values as upper left and lower right corners.
    This class helps calculating intersection etc."""
    __slots__ = ['upper_left', 'lower_right']
    def __init__(self, upper_left=None, lower_right=None):
        # If one of the two is None the other one should be two, as we
        # don't have the data of a rectangle
        if upper_left is None or lower_right is None:
            upper_left = None
            lower_right = None
        # If upper_left and lower_right are not geometrically
        # arranged correctly, we interpret this as the
        # empty rectangle. This makes sense e.g. for the implementation of
        # for example & below.
        elif (upper_left[0] > lower_right[0] or
              upper_left[1] > lower_right[1]):
            upper_left = None
            lower_right = None
        self.upper_left = upper_left
        self.lower_right = lower_right

    def __bool__(self):
        if self.upper_left is None or self.lower_right is None:
            return False
        if (self.upper_left[0] > self.lower_right[0] or
                self.upper_left[1] > self.lower_right[1]):
            return False
        return True

    def __or__(self, other):
        '''Returns the smallest rectangle containing both'''
        upper_left = (min(self.upper_left[0], other.upper_left[0]),
                      min(self.upper_left[1], other.upper_left[1]))
        lower_right = (max(self.lower_right[0], other.lower_right[0]),
                       max(self.lower_right[1], other.lower_right[1]))
        return Rectangle(upper_left, lower_right)

    def __and__(self, other):
        '''Returns the largest rectangle contained in both'''
        upper_left = (max(self.upper_left[0], other.upper_left[0]),
                      max(self.upper_left[1], other.upper_left[1]))
        lower_right = (min(self.lower_right[0], other.lower_right[0]),
                       min(self.lower_right[1], other.lower_right[1]))
        return Rectangle(upper_left, lower_right)

    def __str__(self):
        return f'Rectangle({self.upper_left}, {self.lower_right})'

    def __eq__(self, other):
        return (self.upper_left == other.upper_left and
                self.lower_right == other.lower_right)

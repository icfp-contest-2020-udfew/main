#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# Copyright 2020 Bernhard Reinke, Sílvia Cavadas

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Contains classes to handle encoding the alien binary format
"""


import logging
import math


class BinaryData:
    '''Container for alien-style binary data'''
    def __init__(self, *, from_data=None, from_binary_string=None):
        self.data = None
        self.binary_string = ''
        if from_data is not None:
            self.data = from_data
            self.encode()
        if from_binary_string is not None:
            self.binary_string = from_binary_string
            self.decode()

    def add_bits(self, bits):
        '''Adds bits to the binary representation'''
        for bit in bits:
            if bit:
                self.binary_string += '1'
            else:
                self.binary_string += '0'

    def get_bit(self):
        '''Gets the next number bit'''
        if self.binary_string is not None:
            for char in self.binary_string:
                if char == "1":
                    yield True
                else:
                    yield False

    def encode_recursive(self, data):
        '''Recursively encodes Python data'''
        logging.debug("Trying to encode data: %s", str(data))
        if isinstance(data, int):
            logging.debug('Trying to encode an int')
            if data < 0:
                self.add_bits([True, False])
                data *= -1
            else:
                self.add_bits([False, True])
            width = math.ceil(math.log(data + 1, 2) / 4)
            logging.debug("Encoding int with width %d groups of 4 bits", width)
            self.add_bits([True]*(width) + [False])
            number_bits_reversed = list()
            for _ in range(0, width * 4):
                if data % 2 == 1:
                    number_bits_reversed.append(True)
                else:
                    number_bits_reversed.append(False)
                data = data >> 1
            number_bits_reversed.reverse()
            self.add_bits(number_bits_reversed)
        if isinstance(data, tuple):
            logging.debug('Trying to encode a tuple')
            if len(data) == 0:
                self.add_bits([False, False])
            elif len(data) == 2:
                self.add_bits([True, True])
                self.encode_recursive(data[0])
                self.encode_recursive(data[1])
        if isinstance(data, list):
            logging.debug('Trying to encode a list')
            for elem in data:
                self.add_bits([True, True])
                self.encode_recursive(elem)
            self.add_bits([False, False])

    def encode(self):
        '''Encodes Python Data'''
        self.encode_recursive(self.data)

    def decode_recursive(self, bits):
        '''Recursively Decodes binary data'''
        first_two_bits = (next(bits), next(bits))
        if first_two_bits in [(False, True), (True, False)]:
            logging.debug("Decoding a number")
            number = 0
            if first_two_bits == (False, True):
                sign = 1
            else:
                sign = -1
            width = 0
            while next(bits):
                width += 1
            logging.debug("Width of number is %d", width)
            current_two_power = 2 ** (4*width - 1)
            for _ in range(0, width * 4):
                if next(bits):
                    number += current_two_power
                current_two_power = current_two_power >> 1
            number *= sign
            return number
        if first_two_bits == (False, False):
            return ()
        if first_two_bits == (True, True):
            head = self.decode_recursive(bits)
            tail = self.decode_recursive(bits)
            return (head, tail)
        raise NotImplementedError

    def decode(self):
        '''Decodes binary data'''
        self.data = self.decode_recursive(self.get_bit())

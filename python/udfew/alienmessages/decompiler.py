#!/usr/bin/python3

# Copyright 2020 Malte Leip <malte@leip.net>

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

"""Contains classes to decompile the alien message
"""

# TODO: fix those:
# pylint: disable=abstract-method, too-many-lines, global-statement, invalid-name

import logging
import sys
import math
import copy


sys.setrecursionlimit(10000)
INDENTATION = "    "
DISPLAYDEPTH = 30

global_debug_track = None

#class UndefinedBehaviour:
#    def __init__(self, reason):
#        self.reason = reason
#
class VariableOnRightHandSideNotImplemented(Exception):
    '''A variable was used on the right hand side, but this is not implemented yet'''

class CanNotSimplifyFunctionEvaluation(Exception):
    '''Most likely a function application with (lambda) variables as arguments that
    can not be simplified, so output the original thing'''

class NeedToSimplify(Exception):
    '''When we do something like trying to extract the value of a function that
    we can perhaps apply to get it, but have not done so yet'''


def extract_tokens(string):
    '''Extracts tokens from galaxy message type input'''
    gen = (word for word in string.split())
    return gen

def extract_simplify(obj, type_restriction=None):
    '''Extracts a value, and in doing so simplifies if necessary. Might fail'''
    if type_restriction is None:
        return obj.extract_value()
    simplify_round = 0
    logging.debug("%s in %s?", type(obj), type_restriction)
    while type(obj) not in type_restriction:
        simplify_round += 1
        logging.debug('Extraction round %d currently at %s of type %s (need type in %s)',
                      simplify_round, obj, type(obj), type_restriction)
        if simplify_round >= 100:
            raise Exception('Infinite extration loop!')
        try:
            obj = obj.extract_value(type_restriction=type_restriction)
        except NeedToSimplify:
            logging.debug('Cought NeedToSimplify, so doing that.')
            if obj.is_fully_simplified():
                logging.debug('NeedToSimplify but already fully simplified... Probably a global.')
                raise
            obj = obj.simplify()
        logging.debug('After extracting value got %s of type %s', obj, type(obj))
    return obj

def fancy_str_arguments(arguments, pre="", depth=math.inf):
    '''Fancy representation of a list of arguments, with brackets'''
    if not arguments:
        return ""
    string_parts = []
    with_newline = any([(argument is None or '\n' in argument.fancy_str(pre="", depth=depth - 1))
                        for argument in arguments])
    if with_newline:
        string_parts.extend(['\n', pre, '(\n'])
        for argument in arguments:
            if argument is None:
                string_parts.extend([pre, INDENTATION, '_'])
            else:
                string_parts.append(argument.fancy_str(pre=pre + INDENTATION, depth=depth - 1))
            if string_parts[-1][-1] == '\n':
                string_parts[-1] = string_parts[-1][0:-1]
            string_parts.append(',\n')
        string_parts.pop()
        string_parts.append('\n')
        string_parts.extend([pre, ')\n'])
    else:
        string_parts.extend(['('])
        for argument in arguments:
            if argument is None:
                string_parts.append('_')
            else:
                string_parts.append(argument.fancy_str(pre='', depth=depth - 1))
            string_parts.append(', ')
        string_parts.pop()
        string_parts.extend([')'])
    return "".join(string_parts)


#class Term: # pylint: disable=too-many-instance-attributes
#    '''Term in the message in the style of the alien galaxy message'''
#    def __init__(self, tokens):
#        token = next(tokens)
#        self.is_variable = False
#        self.is_ap = False
#        self.is_function = False
#        self.is_number = False
#        self.is_special = False
#        if token[0] == ":":
#            # A variable
#            self.is_variable = True
#            self.variable_number = int(token[1:])
#        elif token.isnumeric():
#            self.is_number = True
#            self.number = int(token)
#        elif token == "ap":
#            self.is_ap = True
#            self.ap_function = Term(tokens)
#            self.ap_parameter = Term(tokens)
#        elif token in ['nil']:
#            self.is_special = True
#            self.special_string = token
#        elif token in defined_functions.keys():
#            self.is_function = True
#            self.function_string = token
#        else:
#            raise NotImplementedError(f'Can not parse token {token}')
#
#    def __str__(self):
#        if self.is_variable:
#            return f'V[{self.variable_number}]'
#        if self.is_number:
#            return str(self.number)
#        if self.is_ap:
#            return "".join([str(self.ap_function), '(', str(self.ap_parameter), ')'])
#        if self.is_function:
#            return self.function_string
#        if self.is_special:
#            return self.special_string
#        raise NotImplementedError("Cant print token")
#
#    def parse_to_higher_level_code(self): # pylint: disable=too-many-return-statements, too-many-branches
#        '''Parse the term, output is higher level code tree'''
#        if self.is_variable:
#            raise VariableOnRightHandSideNotImplemented
#        if self.is_number:
#            return HigherLevelCodeInt(self.number)
#        if self.is_ap:
#            function = self.ap_function.parse_to_higher_level_code()
#            argument = self.ap_parameter.parse_to_higher_level_code()
#            logging.debug('Applying function %s to argument %s', str(function), str(argument))
#            function.plug_in(argument)
#            logging.debug('Function is now %s', str(function))
#            return function
#        if self.is_function:
#            return construct_hlc_concrete_function(self.function_string)
#        if self.is_special:
#            if self.special_string == 'nil':
#                return HigherLevelCodeList([])
#            raise NotImplementedError(f'Do not know how to parse special {self.special_string}')
#        raise NotImplementedError("Cant print token")

def tokens_to_higher_level_code(tokens):
    '''Converts tokens (as a generator) to HLC representation'''
    token = next(tokens)
    if token[0] == ":":
        return HigherLevelCodeGlobalVariable(token)
    if token.isnumeric() or (token[0] == '-' and token[1:].isnumeric()):
        number = int(token)
        return HigherLevelCodeInt(number)
    if token == "ap":
        function = tokens_to_higher_level_code(tokens)
        argument = tokens_to_higher_level_code(tokens)
        logging.debug('Applying function %s to argument %s', str(function), str(argument))
        function.plug_in(argument)
        logging.debug('Function is now %s', str(function))
        return function
    if token in ['nil']:
        return HigherLevelCodeList([])
    if token in defined_functions.keys():
        function_string = token
        return construct_hlc_concrete_function(function_string)
    if token == '[':
        logging.debug('Got start of a list')
        list_ = list()
        next_bit = tokens_to_higher_level_code(tokens)
        while next_bit != "]":
            list_.append(next_bit)
            next_bit = next(tokens)
            if next_bit not in (',', ']'):
                raise Exception('Did not see expected comma or close brackets'
                                f'when parsing list... Was up to {list_} and got token {next_bit}')
            if next_bit == ',':
                next_bit = tokens_to_higher_level_code(tokens)
        logging.debug('Parsed list %s', list_)
        return HigherLevelCodeList(list_)
    raise NotImplementedError(f'Can not parse token {token}')


class HigherLevelCode:
    '''A higher level representation of decompiled code like galaxy.txt'''
    def fancy_str(self, pre="", depth=math.inf):
        '''String reprenting the decompiled code'''
        raise NotImplementedError(f'Need to implement fancy_str member function for class '
                                  f'{type(self).__name__}')

    def __eq__(self, other):
        '''Checks whether two objects are equivalent'''
        raise NotImplementedError(f'Need to implement __eq__ member function for class '
                                  f'{type(self).__name__}')

    def extract_value(self, type_restriction=None):
        '''Extracts an actual Python type out of possibly nested wrappers of
        HigherLevelCode objects, if possible'''
        value = self.extract_value_raw()
        while isinstance(value, HigherLevelCode):
            last_id = id(value)
            value = value.extract_value_raw()
            if id(value) == last_id:
                break
        if type_restriction is not None and type(value) not in type_restriction:
            logging.debug('Types required are %s but got %s', type_restriction, type(value))
            if isinstance(value, HigherLevelCodeFunction):
                logging.debug('Have function, so will need to simplify that first')
                raise NeedToSimplify
            raise TypeError
        return value

    def extract_value_raw(self):
        '''Extracts the "actual value" of the HLC class. Might fail if no sensible
        interpretation exists'''
        raise NotImplementedError(f'Need to implement extract_value_raw member function for class '
                                  f'{type(self).__name__}')

    def name_variables(self, generator):
        '''Recursively names lambda variables using the generator'''
        raise NotImplementedError(f'Need to implement name_variables member function for class '
                                  f'{type(self).__name__}')

    def sugar_string(self):
        '''Return a sugared version that can be read again'''
        raise NotImplementedError(f'Need to implement sugar_string member function for class '
                                  f'{type(self).__name__}')

    def __copy__(self):
        '''Copy'''
        raise NotImplementedError(f'Need to implement __copy__ member function for class '
                                  f'{type(self).__name__}')


    def __str__(self):
        return self.fancy_str(pre="", depth=DISPLAYDEPTH)

    def __repr__(self):
        return str(self)

    def all_childs_fully_simplified(self):
        '''Return whether all childs are fully simplified'''
        raise NotImplementedError(f'Need to implement all_childs_fully_simplified member function '
                                  f'for class {type(self).__name__}')

    def is_fully_simplified(self):
        '''Return whether self and all childs are fully simplified'''
        if not self._fully_simplified: # pylint: disable=no-member
            logging.debug('Returning False to is_fully_simplified due to object '
                          '%s of type %s', self, type(self))
        return self._fully_simplified and self.all_childs_fully_simplified() # pylint: disable=no-member

    #def setup_unbound_variables(self, unbound_variables, unbound_variables_names):
    #    '''Sets up the unbound variables store. Call first thing in parse.'''
    #    if unbound_variables is None:
    #        unbound_variables = dict()
    #    self.unbound_variables = unbound_variables
    #    if unbound_variables_names is None:
    #        unbound_variables_names = list()
    #    self.unbound_variables_names = unbound_variables_names

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        '''Parses self to a nicer representation (e.g. figure out raw functions to
        a greater degree) inject_variable_depth=0 means can inject, but childs not,
        math.inf means infinite depth'''
        raise NotImplementedError(f'Need to implement simplify member function for class '
                                  f'{type(self).__name__}')

class HigherLevelCodeInt(HigherLevelCode):
    '''HLC integer'''
    def __init__(self, number):
        self._fully_simplified = True
        self._int_value = number

    def __eq__(self, other):
        if not isinstance(other, HigherLevelCodeInt):
            return False
        return self._int_value == other._int_value

    def __copy__(self):
        copy_ = HigherLevelCodeInt(self._int_value)
        return copy_

    def extract_value_raw(self):
        return self._int_value

    def all_childs_fully_simplified(self):
        return True

    def name_variables(self, generator):
        return

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        self._fully_simplified = True
        return self

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        return "".join([pre, str(self._int_value)])

    def sugar_string(self):
        return str(self._int_value)

class HigherLevelCodeFunction(HigherLevelCode):
    '''A class for general functions where one can plug in things.'''
    def plug_in(self, argument):
        '''Plug in the argument into the first (remaining) slot of the function'''
        raise NotImplementedError(f'Need to implement plug_in member function for class '
                                  f'{type(self).__name__}')

    def return_type(self):
        '''The return type of the function'''
        raise NotImplementedError(f'Need to implement return_type member function for class '
                                  f'{type(self).__name__}')

class HigherLevelCodeGlobalVariable(HigherLevelCodeFunction):
    '''A global variable'''
    # TODO: pass global variable store to simplify, then use that to interpret if we already
    # have it. Will need to adapt all of the below for that.
    def __init__(self, name):
        self._fully_simplified = True
        self._name = name
        self._arguments = []

    def name(self):
        return self._name

    def __copy__(self):
        copy_ = HigherLevelCodeGlobalVariable(self._name)
        for argument in self._arguments:
            copy_.plug_in(copy.copy(argument))
        return copy_

    def extract_value_raw(self):
        return self

    def name_variables(self, generator):
        for argument in self._arguments:
            argument.name_variables(generator)

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = [pre, 'G{', self._name, '}']
        string_parts.append(fancy_str_arguments(self._arguments, pre=pre, depth=depth - 1))
        return "".join(string_parts)

    def plug_in(self, argument):
        self._arguments.append(argument)

    def all_childs_fully_simplified(self):
        return all([argument.is_fully_simplified() for argument in self._arguments])

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        if self.is_fully_simplified():
            return self
        for i in range(0, len(self._arguments)):
            self._arguments[i] = self._arguments[i].simplify(
                inject_variable_depth=inject_variable_depth - 1, decided_conditions=decided_conditions)
        return self


class HigherLevelCodeLambdaFunction(HigherLevelCodeFunction): # pylint: disable=too-many-instance-attributes
    '''Functions of the form λx1,..,xn:f(x1,...,xn)'''
    def __init__(self, value):
        self._fully_simplified = False
        self._value = value
        self._lambda_variables = list()

    def all_childs_fully_simplified(self):
        return (all([var.is_fully_simplified() for var in self._lambda_variables])
                and self._value.is_fully_simplified())

    def inject_lambda_variable(self):
        '''Add an external lambda variable and inject it to the inner function'''
        self._fully_simplified = False
        lambda_variable_for_record = HigherLevelCodeLambdaVariable()
        lambda_variable_for_inject = copy.copy(lambda_variable_for_record)
        self._lambda_variables.append(lambda_variable_for_record)
        self.plug_in_internally(lambda_variable_for_inject)

    def plug_in_internally(self, argument):
        '''Plugs an argument into the internal function'''
        self._fully_simplified = False
        if not isinstance(self._value, HigherLevelCodeFunction):
            raise TypeError(f'Can not plug in to the non-function {self._value} '
                            f'of type {type(self._value)}')
        self._value.plug_in(argument)

    def value(self):
        '''Returns the value, i.e. the actual function'''
        return self._value

    def lambda_variables(self):
        '''Returns lambda variable list'''
        return self._lambda_variables

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        if self.is_fully_simplified():
            return self
        global global_debug_track
        logging.debug('Status of global debug track:\n%s', global_debug_track)
        logging.debug('Simplifying the lambda function %s with %d lambda variables',
                      self, len(self._lambda_variables))

        if not self._lambda_variables:
            logging.debug('Lambda function has no lambda variables, returning value')
            if id(global_debug_track) == id(self):
                global_debug_track = self._value
            return self._value
        if isinstance(self._value, HigherLevelCodeLambdaFunction):
            logging.debug('Lambda function of a lambda function, squashing them together')
            self._lambda_variables.extend(self.value().lambda_variables())
            self._value = self.value().value()
            return self
        if not self.all_childs_fully_simplified():
            logging.debug('Simplifying lambda variables...')
            for i in range(0, len(self._lambda_variables)):
                self._lambda_variables[i] = self._lambda_variables[i].simplify(
                    inject_variable_depth=inject_variable_depth, decided_conditions=decided_conditions)
            logging.debug('Simplifying value of lambda function')
            self._value = self._value.simplify(inject_variable_depth=inject_variable_depth,
                decided_conditions=decided_conditions)
            self._fully_simplified = False
            return self
        if (isinstance(self._value, HigherLevelCodeLambdaVariable)
                and (self._value.unique_id() in
                    [variable.unique_id() for variable in self._lambda_variables])
                and not self._value._arguments):
            variable_projected_to = [variable for variable in self._lambda_variables if
                    variable.unique_id() == self._value.unique_id()]
            if len(variable_projected_to) != 1:
                raise Exception
            variable_projected_to = variable_projected_to[0]
            index = self._lambda_variables.index(variable_projected_to)
            logging.debug('Lambda function is a projection of %d variables to variable %d',
                len(self._lambda_variables), index + 1)
            if len(self._lambda_variables) == 2:
                if index == 0:
                    return construct_hlc_concrete_function("t")
                else:
                    return construct_hlc_concrete_function("f")
            elif len(self._lambda_variables) == 1:
                raise NotImplementedError
            raise NotImplementedError
        self._fully_simplified = True
        logging.debug('Simplified lambda function is now: %s', self)
        return self

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = []
        if self._lambda_variables:
            string_parts.append(pre)
            string_parts.append('(λ ')
            for lambda_variable in self._lambda_variables:
                string_parts.append(lambda_variable.fancy_str(depth=depth - 1))
                string_parts.append(', ')
            string_parts.pop()
            string_parts.append(':\n')
            string_parts.append(self._value.fancy_str(pre + INDENTATION, depth=depth - 1))
            string_parts.extend(['\n', pre, ')\n'])
        else:
            string_parts.append(self._value.fancy_str(pre=pre, depth=depth - 1))
        return "".join(string_parts)

    def name_variables(self, generator):
        for lambda_variable in self._lambda_variables:
            lambda_variable.name_variables(generator)
        self._value.name_variables(generator)


class HigherLevelCodeConcreteFunction(HigherLevelCodeFunction): # pylint: disable=too-many-instance-attributes
    '''A HLC function that takes a couple of arguments and returns something'''
    def __init__(self, function, number_of_arguments, # pylint: disable=too-many-arguments
                 types=None, return_type=None, name=None):
        self._fully_simplified = False
        if number_of_arguments is None:
            raise Exception(f'Need to provide number of arguments for function {function}')
        if types is None:
            types = [None] * number_of_arguments
        self._function = function
        self._number_of_arguments = number_of_arguments
        self._types = types
        self._arguments = [None] * number_of_arguments
        self._post_arguments = [] # This is for arguments to plug in to the return value, which
                                 # may itself be a function. This needs to be carried over
                                 # when evaluating this function.
        self._name = name
        self._return_type = return_type
        self._evaluated = False
        self._return_value = None
        self._can_not_evaluate = False

    def return_type(self):
        return self._return_type

    def all_childs_fully_simplified(self):
        arguments_simplified = all([arg.is_fully_simplified() for arg in self._arguments
                                    if arg is not None])
        post_arguments_simplified = all([arg.is_fully_simplified() for arg in self._post_arguments
                                         if arg is not None])
        if self._return_value is None:
            return_simplified = True
        else:
            return_simplified = self._return_value.is_fully_simplified()
        return arguments_simplified and post_arguments_simplified and return_simplified

    def __copy__(self):
        logging.debug('Copying conrete function %s', self)
        if self._evaluated:
            logging.debug('Function we are trying to copy is evaluated, returning copy of return value')
            return copy.copy(self._return_value)
        function = copy.copy(self._function)
        number_of_arguments = copy.copy(self._number_of_arguments)
        types = copy.copy(self._types)
        arguments = [copy.copy(argument) for argument in self._arguments]
        name = copy.copy(self._name)
        return_type = copy.copy(self._return_type)
        obj = HigherLevelCodeConcreteFunction(function, number_of_arguments, types=types,
                                              return_type=return_type, name=name)
        obj._arguments = arguments
        obj._fully_simplified = self._fully_simplified
        for arg in self._post_arguments:
            obj._post_arguments.append(copy.copy(arg))
        logging.debug('Copy of %s is %s', self, obj)
        return obj

    def name_variables(self, generator):
        if self._evaluated:
            self._return_value.name_variables(generator)
        else:
            for argument in [argument for argument in self._arguments if argument is not None]:
                argument.name_variables(generator)
            for argument in self._post_arguments:
                argument.name_variables(generator)


    def extract_value_raw(self):
        return_value = self
        if self._evaluated:
            return_value = self._return_value
        return return_value

    def index_unused_argument(self):
        '''Find the smalles index of a not yet used slot'''
        unused_arguments = [i for i in range(0, self._number_of_arguments)
                            if self._arguments[i] is None]
        if unused_arguments:
            return unused_arguments[0]
        return None

    def plug_in(self, argument):
        '''Plug in an argument into the next free slot of this function
        (or what this function has been replaced with)'''
        self._fully_simplified = False
        if self._evaluated:
            logging.debug('Plugging in to the evaluated function %s, '
                          'plugging into the return value instead', self)
            self._return_value.plug_in(argument)
            return
        index = self.index_unused_argument()
        logging.debug('Plugging in argument %s into index %r of function %s',
                      str(argument), index, str(self))
        if index is None:
            logging.debug('Function: %s, arguments: %s', self, self._arguments)
            self._post_arguments.append(argument)
            logging.debug('Remembering argument as post-argument, list of post arguments '
                          'is now %s', self._post_arguments)
            return
        logging.debug('Type is supposed to be %r and is %r', self._types[index], type(argument))
        if self._types[index] is None or isinstance(argument, self._types[index]):
            self._arguments[index] = argument
        elif isinstance(argument, HigherLevelCodeLambdaVariable):
            # It is important that this elif comes before the one for functions,
            # as LambdaVariables now also derive from Function.
            argument.restrict_type(self._types[index])
            self._arguments[index] = argument
        elif isinstance(argument, HigherLevelCodeFunction):
            if (argument.return_type() is None
                    or issubclass(argument.return_type(), self._types[index])):
                self._arguments[index] = argument
            else:
                raise TypeError('Argument is function with the wrong return type!')
        else:
            raise TypeError('Wrong type of argument!')

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = list(pre)
        if self._evaluated:
            string_parts.append('ε')
            string_parts.append(self._return_value.fancy_str(depth=depth - 1))
        else:
            if self._name is None:
                string_parts.append(str(self._function))
            else:
                if self._name == 't':
                    string_parts.append('True')
                elif self._name == 'f':
                    string_parts.append('False')
                else:
                    string_parts.append(self._name)
            if self._name not in ['t', 'f']:
                string_parts.append(str(self._number_of_arguments))
            if self._arguments and not all([argument is None for argument in self._arguments]):
                string_parts.append(
                    fancy_str_arguments(self._arguments, pre=pre, depth=depth - 1))
            string_parts.append(
                fancy_str_arguments(self._post_arguments, pre=pre, depth=depth - 1))
        return "".join(string_parts)

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        if self.is_fully_simplified():
            return self
        global global_debug_track
        logging.debug('Status of global debug track:\n%s', global_debug_track)
        logging.debug('Simplifying function %s...', self)
        if self._evaluated:
            logging.debug('This function is an evaluated one, returning return value %s...',
                          self._return_value)
            if id(global_debug_track) == id(self):
                global_debug_track = self._return_value
            return self._return_value

        if all([argument is None for argument in self._arguments]):
            logging.debug('Function has no arguments and is not evaluated, returning self.')
            if self._post_arguments:
                raise NotImplementedError
            self._fully_simplified = True
            return self

        logging.debug('Simplifying arguments of %s without injecting variables there...', self)
        for i in range(0, self._number_of_arguments):
            if self._arguments[i] is not None:
                self._arguments[i] = self._arguments[i].simplify(decided_conditions=decided_conditions)

        logging.debug('Simplifying post arguments of %s without injecting variables there...', self)
        for i in range(0, len(self._post_arguments)):
            self._post_arguments[i] = self._post_arguments[i].simplify(decided_conditions=decided_conditions)

        if self.index_unused_argument() is not None:
            if inject_variable_depth < 0:
                logging.debug('Missing arguments but am not supposed to inject.')
                #self._fully_simplified = True # TODO: What should we do here? Perhaps not set it?
                return self
            logging.debug('Injecting missing arguments into %s', self)

        index = self.index_unused_argument()
        if index is not None:
            logging.debug('Injecting missing variables, so creating wrapper lambda function')
            if self._post_arguments:
                raise NotImplementedError
            lambda_function = HigherLevelCodeLambdaFunction(self)
            while index is not None:
                logging.debug('Injecting lambda variable into slot %s', index)
                lambda_function.inject_lambda_variable()
                index = self.index_unused_argument()
            if id(global_debug_track) == id(self):
                global_debug_track = lambda_function
            return lambda_function

        if not self._can_not_evaluate:
            logging.debug('Simplifying a function with full arguments by evaluating: %s', self)
            try:
                self._return_value = self._function(self._arguments)
                self._evaluated = True
                self._fully_simplified = False
                for argument in self._post_arguments:
                    self._return_value.plug_in(argument)
                return self
            except CanNotSimplifyFunctionEvaluation:
                logging.debug('Evaluating the function does not simplify, so not doing that.')
                # FIXME: only set this to true if all childs are fully simplified?
                self._can_not_evaluate = True
        else:
            logging.debug('Function %s is marked as can-not-evaluate, so do not try that', self)
        self._fully_simplified = True
        return self

class HigherLevelCodeIf(HigherLevelCodeFunction): # pylint: disable=too-many-instance-attributes
    '''A HLC if block. Derives from HLCFunction as one can apply into both branches.'''
    def __init__(self, condition, true_branch, false_branch):
        self._fully_simplified = False
        self._condition = condition
        self._true_branch = true_branch
        self._false_branch = false_branch

    def all_childs_fully_simplified(self):
        return (self._condition.is_fully_simplified() and
                self._true_branch.is_fully_simplified() and
                self._false_branch.is_fully_simplified())

    def __copy__(self):
        condition = copy.copy(self._condition)
        true_branch = copy.copy(self._true_branch)
        false_branch = copy.copy(self._false_branch)
        copy_ = HigherLevelCodeIf(condition, true_branch, false_branch)
        copy_._fully_simplified = self._fully_simplified
        return copy_

    def name_variables(self, generator):
        self._condition.name_variables(generator)
        self._true_branch.name_variables(generator)
        self._false_branch.name_variables(generator)

    def extract_value_raw(self):
        raise NotImplementedError

    def plug_in(self, argument):
        '''Plug in an argument into both branches'''
        self._fully_simplified = False
        logging.debug('Plugging argument %s into both branches of an if block:\n'
                      'condition: %s\ntrue branch:  %s\nfalse branch: %s',
                      argument, self._condition, self._true_branch, self._false_branch)
        logging.debug('id argument: %d, id self: %d', id(argument), id(self))
        if id(argument) == id(self):
            raise Exception('Infinite loop detected')
        argument_for_true_branch = argument
        argument_for_false_branch = copy.copy(argument)
        logging.debug('if: Argument for true branch: %s', argument_for_true_branch)
        logging.debug('if: Argument for false branch: %s', argument_for_false_branch)
        self._true_branch.plug_in(argument_for_true_branch)
        logging.debug('if: Argument for false branch, after plugging into true branch: %s',
            argument_for_false_branch)
        self._false_branch.plug_in(argument_for_false_branch)

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = [pre, 'if(', self._condition.fancy_str(depth=depth - 1), ')\n',
                        pre, '{\n',
                        self._true_branch.fancy_str(pre + INDENTATION, depth=depth -1)]
        if string_parts[-1][-1] != '\n':
            string_parts.append('\n')
        string_parts.extend([pre, '}\n',
                             pre, 'else\n',
                             pre, '{\n',
                             self._false_branch.fancy_str(pre + INDENTATION, depth=depth - 1)])
        if string_parts[-1][-1] != '\n':
            string_parts.append('\n')
        string_parts.extend([pre, '}'])
        return "".join(string_parts)

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        if self.is_fully_simplified():
            return self
        global global_debug_track
        logging.debug('Status of global debug track:\n%s', global_debug_track)
        logging.debug('Simplifying if block:\n%s', self)
        if decided_conditions is not None:
            logging.debug('Conditions that are known to be true: %s, known to be false: %s',
                decided_conditions['t'], decided_conditions['f'])
            for true_condition in decided_conditions['t']:
                if true_condition == self._condition:
                    return self._true_branch
            for false_condition in decided_conditions['f']:
                if false_condition == self._condition:
                    return self._false_branch
        if decided_conditions is None:
            decided_conditions = { 't': [], 'f': [] }
        self._condition = self._condition.simplify(inject_variable_depth=inject_variable_depth - 1,
            decided_conditions=decided_conditions)
        true_decided_conditions = { 't': [], 'f': [] }
        false_decided_conditions = { 't': [], 'f': [] }
        true_decided_conditions['t'] = [condition for condition in decided_conditions['t']]
        false_decided_conditions['t'] = [condition for condition in decided_conditions['t']]
        true_decided_conditions['f'] = [condition for condition in decided_conditions['f']]
        false_decided_conditions['f'] = [condition for condition in decided_conditions['f']]
        true_decided_conditions['t'].append(self._condition)
        false_decided_conditions['f'].append(self._condition)
        self._true_branch = self._true_branch.simplify(
            inject_variable_depth=inject_variable_depth - 1, decided_conditions=true_decided_conditions)
        self._false_branch = self._false_branch.simplify(
            inject_variable_depth=inject_variable_depth - 1, decided_conditions=false_decided_conditions)
        if isinstance(self._condition, HigherLevelCodeBool):
            raise NotImplementedError
        self._fully_simplified = True
        return self

def unpack_type_to_str(type_):
    '''Unpack type recursively to find a concise description of what type the actual value is'''
    type_to_str = {
        list: "list",
        HigherLevelCodeList: list,
        HigherLevelCodeInt: int,
        int: "int",
        }
    while type_ in type_to_str:
        type_ = type_to_str[type_]
    if isinstance(type_, str):
        return "".join(["<", type_, ">"])
    return str(type_)


class HigherLevelCodeLambdaVariable(HigherLevelCodeFunction):
    '''A variable used in a lambda function'''
    def __init__(self, name=None, var_type=None):
        self._fully_simplified = False
        # This list will not be deep copied when copying, leaving the name the same!
        # This is important - lambda variables are bound and should not be split up
        # into independent copies
        # Two entries: the original id, that never changes and is unique, and the name
        # which might be None
        self._name = [id(self), name]
        self._type = [var_type] # Similarly here, the variable can only have a single type
        self._arguments = []
        self._list_slice = [0, math.inf]
        self._list_index = None

    def __eq__(self, other):
        if not isinstance(other, HigherLevelCodeLambdaVariable):
            return False
        if self._name[0] != other._name[0]:
            return False
        if self._arguments != other._arguments:
            return False
        if self._list_index != other._list_index:
            return False
        if self._list_slice != other._list_slice:
            return False
        return True

    def list_slice(self, start, end):
        self._fully_simplified = False
        self._list_slice[1] = self._list_slice[0] + end
        self._list_slice[0] += start

    def list_index(self, index):
        self._fully_simplified = False
        self._list_index = index
        # New list, to decouple from other occurences
        # FIXME, as this is only needed as other copies might be the full list...
        self._type = [None]

    def unique_id(self):
        return self._name[0]

    def return_type(self):
        if (self._type[0] is not None and issubclass(self._type[0], HigherLevelCodeList)
                and self._list_slice[0] == self._list_slice[1]):
            # This is an element of a list!
            return None
        return self._type[0]

    def plug_in(self, argument):
        self._fully_simplified = False
        self._arguments.append(argument)

    def extract_value_raw(self):
        return self

    def all_childs_fully_simplified(self):
        return all([argument.is_fully_simplified() for argument in self._arguments])

    def __copy__(self):
        # Lambda variables are bound and should not be split up into independent copies
        copy_ = HigherLevelCodeLambdaVariable()
        copy_.list_index(self._list_index)
        copy_.list_slice(self._list_slice[0], self._list_slice[1])
        copy_._name = self._name
        copy_._type = self._type
        for argument in self._arguments:
            copy_.plug_in(copy.copy(argument))
        return copy_

    def bare_copy(self):
        '''No argument etc., just the variable'''
        copy_ = HigherLevelCodeLambdaVariable()
        copy_.list_index(self._list_index)
        copy_.list_slice(self._list_slice[0], self._list_slice[1])
        copy_._name = self._name
        copy_._type = self._type
        return copy_

    def name_variables(self, generator):
        if self._name[1] is None:
            self._name[1] = next(generator)
        for argument in self._arguments:
            argument.name_variables(generator)

    def restrict_type(self, var_type):
        '''There is an external constraint on what type this variable can be, so remember this.'''
        self._fully_simplified = False
        if var_type is None:
            return
        if (self._type[0] is not None and not issubclass(var_type, self._type[0])
                and not issubclass(self._type[0], HigherLevelCodeFunction)):
            raise Exception(f'Restriction lambda variable {self} of type '
                            f'{self._type[0]} to incompatible type {var_type}')
        logging.debug('Restricting lambda variable %s to type %s', self, var_type)
        self._type[0] = var_type

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = [pre]
        if self._name[1] is None:
            string_parts.extend(['x_', str(self._name[0])])
        else:
            string_parts.append(self._name[1])
        if (self._type[0] is not None and issubclass(self._type[0], HigherLevelCodeList)
                and (self._list_slice[0] != 0 or self._list_slice[1] != math.inf)):
            string_parts.append('[')
            string_parts.append(str(self._list_slice[0]))
            string_parts.append(':')
            if self._list_slice[1] != math.inf:
                string_parts.append(str(self._list_slice[1]))
            string_parts.append(']')
        if self._list_index is not None:
            string_parts.append('[')
            string_parts.append(str(self._list_index))
            string_parts.append(']')
        if self._type[0] is not None:
            string_parts.append(unpack_type_to_str(self._type[0]))
        string_parts.append(fancy_str_arguments(self._arguments, pre=pre, depth=depth - 1))
        return "".join(string_parts)

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        logging.debug('Simplifying lambda variable...')
        if self.is_fully_simplified():
            return self
        for i in range(0, len(self._arguments)):
            self._arguments[i] = self._arguments[i].simplify(
                inject_variable_depth=inject_variable_depth - 1, decided_conditions=decided_conditions)
        if self._arguments:
            logging.debug('Lambda variable with type %s and arguments %s',
                self._type[0], self._arguments)
        if (self._type[0] is not None and issubclass(self._type[0], HigherLevelCodeList)
                and self._arguments):
            head = self.bare_copy()
            head.list_index(0)
            tail = self.bare_copy()
            tail.list_slice(1, math.inf)
            function = self._arguments[0]
            logging.debug('Plugging in split up list, %s and %s into %s of type %s',
                head, tail, function, type(function))
            if not isinstance(function, HigherLevelCodeFunction):
                raise TypeError
            function.plug_in(head)
            function.plug_in(tail)
            for argument in self._arguments[1:]:
                function.plug_in(argument)
            return function
        elif (self._type[0] is not None and issubclass(self._type[0], HigherLevelCodePair)
                and self._arguments):
            raise NotImplementedError
        self._fully_simplified = True
        return self


class HigherLevelCodeBool(HigherLevelCodeConcreteFunction):
    '''A boolean, so true or false.'''


class HigherLevelCodeList(HigherLevelCode):
    '''A HLC list'''
    def __init__(self, argument):
        self._fully_simplified = False
        self._value = argument

    def __copy__(self):
        copy_ = HigherLevelCodeList([])
        copy_._fully_simplified = self._fully_simplified
        for element in self._value:
            copy_._value.append(copy.copy(element))
        return copy_

    def all_childs_fully_simplified(self):
        return all([element.is_fully_simplified() for element in self._value])

    def name_variables(self, generator):
        for item in self._value:
            item.name_variables(generator)

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        return "".join([pre, str(self._value)])

    def extract_value_raw(self):
        return self._value

    def sugar_string(self):
        string_parts = ['[ ']
        for element in self._value:
            string_parts.append(element.sugar_string())
            string_parts.append(' , ')
        if self._value:
            string_parts.pop()
        string_parts.append(' ]')
        return "".join(string_parts)

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        for i in range(0, len(self._value)):
            self._value[i] = self._value[i].simplify(
                inject_variable_depth=inject_variable_depth - 1, decided_conditions=decided_conditions)
        self._fully_simplified = True
        return self


class HigherLevelCodePair(HigherLevelCode):
    '''A HLC pair'''
    def __init__(self, value_left, value_right):
        self._fully_simplified = False
        self._value_left = value_left
        self._value_right = value_right

    def all_childs_fully_simplified(self):
        return self._value_left.is_fully_simplified() and self._value_right.is_fully_simplified()

    def name_variables(self, generator):
        self._value_left.name_variables(generator)
        self._value_right.name_variables(generator)

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        return "".join([pre, '(', str(self._value_left), ', ', str(self._value_right), ')'])

    def extract_value_raw(self):
        return (self._value_left, self._value_right)

    def sugar_string(self):
        raise NotImplementedError
        #string_parts = ['[ ']
        #for element in self.value:
        #    string_parts.append(element.sugar_string())
        #    string_parts.append(' , ')
        #if self.value:
        #    string_parts.pop()
        #string_parts.append(' ]')
        #return "".join(string_parts)

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        self._value_left = self._value_left.simplify(
            inject_variable_depth=inject_variable_depth - 1, decided_conditions=decided_conditions)
        self._value_right = self._value_right.simplify(
            inject_variable_depth=inject_variable_depth - 1, decided_conditions=decided_conditions)
        self._fully_simplified = True
        return self


class HigherLevelCodeCondition(HigherLevelCode):
    '''A generic condition'''

    def is_evaluated(self):
        '''Returns whether the conditon is actually just True or False'''
        raise NotImplementedError
    
    def __eq__(self, other):
        '''Checks whether two conditons are equivalent'''
        raise NotImplementedError(f'Need to implement __eq__ member function for class '
                                  f'{type(self).__name__}')


class HigherLevelCodeConditionComparision(HigherLevelCodeCondition):
    '''A generic condition'''
    def __init__(self, operator, lhs, rhs):
        self._fully_simplified = False
        self._operator = operator # A string
        self._lhs = lhs
        self._rhs = rhs

    def __eq__(self, other):
        if not isinstance(other, HigherLevelCodeConditionComparision):
            return False
        # TODO: improve this. for example with == the order doesn't matter.
        return self._operator == other._operator and self._lhs == other._lhs and self._rhs == other._rhs

    def all_childs_fully_simplified(self):
        return self._lhs.is_fully_simplified() and self._rhs.is_fully_simplified()

    def __copy__(self):
        operator = copy.copy(self._operator)
        lhs = copy.copy(self._lhs)
        rhs = copy.copy(self._rhs)
        copy_ = HigherLevelCodeConditionComparision(operator, lhs, rhs)
        copy_._fully_simplified = self._fully_simplified
        return copy_

    def simplify(self, inject_variable_depth=-1, decided_conditions=None):
        if self.is_fully_simplified():
            return self
        self._lhs = self._lhs.simplify(inject_variable_depth=inject_variable_depth - 1,
            decided_conditions=decided_conditions)
        self._rhs = self._rhs.simplify(inject_variable_depth=inject_variable_depth - 1,
            decided_conditions=decided_conditions)
        if self.is_evaluated():
            raise NotImplementedError
        self._fully_simplified = True
        return self

    def name_variables(self, generator):
        self._lhs.name_variables(generator)
        self._rhs.name_variables(generator)

    def fancy_str(self, pre="", depth=math.inf):
        if depth < 0:
            return "Ω"
        string_parts = [pre, self._lhs.fancy_str(depth=depth - 1),
                        ' ', self._operator, ' ', self._rhs.fancy_str(depth=depth - 1)]
        return "".join(string_parts)

    def is_evaluated(self):
        logging.debug('Trying to see if the condition %s is already evaluated', self)
        # TODO: Implement this
        return False


def function_cons(arguments):
    '''Implements the alien function cons'''
    logging.debug('Calling cons with arguments %s', arguments)
    if len(arguments) != 2:
        raise Exception(f'Cons expects 2 arguments, but got {len(arguments)}: {arguments}')
    for argument in arguments:
        if not isinstance(argument, HigherLevelCode):
            raise TypeError('Trying to call cons with arguments not of type HigherLevelCode')
    try:
        second_argument = extract_simplify(arguments[1], type_restriction=[list, int, tuple])
    except TypeError:
        raise CanNotSimplifyFunctionEvaluation
    else:
        if isinstance(second_argument, list):
            new_list = [arguments[0]]
            new_list.extend(second_argument)
            logging.debug('Return value of cons is %s', new_list)
            return HigherLevelCodeList(new_list)
        if isinstance(second_argument, int):
            # Should counstruct a pair!
            value_left = arguments[0]
            value_right = HigherLevelCodeInt(second_argument)
            return HigherLevelCodePair(value_left, value_right)
        raise NotImplementedError


def function_true(arguments):
    '''Implements the true function'''
    logging.debug('Calling true with arguments %s', arguments)
    return arguments[0]

def function_false(arguments):
    '''Implements the false function'''
    logging.debug('Calling true with arguments %s', arguments)
    return arguments[1]


def function_b(arguments):
    '''Implements the alien function b'''
    logging.debug('Called function b with arguments %s', str(arguments))
    argument_x = arguments[0]
    argument_y = arguments[1]
    argument_z = arguments[2]
    argument_y.plug_in(argument_z)
    argument_x.plug_in(argument_y)
    result = argument_x
    logging.debug('result of b is %s', str(result))
    return result

def function_c(arguments):
    '''Implements the alien function c'''
    logging.debug('Called function c with arguments %s', str(arguments))
    argument_x = arguments[0]
    argument_y = arguments[1]
    argument_z = arguments[2]
    argument_x.plug_in(argument_z)
    logging.debug('Middle step of c, now want to apply %s to %s', str(argument_x), str(argument_y))
    argument_x.plug_in(argument_y)
    result = argument_x
    logging.debug('Result of applying c is %s', str(result))
    return result

def function_s(arguments):
    '''Implements the alien function s'''
    logging.debug('Called function s with arguments %s', str(arguments))
    argument_x = arguments[0]
    argument_y = arguments[1]
    argument_z_for_x = arguments[2]
    argument_z_for_y = copy.copy(arguments[2])
    argument_x.plug_in(argument_z_for_x)
    argument_y.plug_in(argument_z_for_y)
    argument_x.plug_in(argument_y)
    return argument_x

def function_neg(arguments):
    '''Implements the neg function'''
    logging.debug('Called function neg with arguments %s', str(arguments))
    try:
        argument = extract_simplify(arguments[0], type_restriction=[int])
    except (TypeError, NeedToSimplify):
        raise CanNotSimplifyFunctionEvaluation
    else:
        return HigherLevelCodeInt((-1) * argument)

def function_mul(arguments):
    '''Implements the mul function'''
    logging.debug('Called function mul with arguments %s', str(arguments))
    try:
        argument_left = extract_simplify(arguments[0], type_restriction=[int])
        argument_right = extract_simplify(arguments[1], type_restriction=[int])
    except (TypeError, NeedToSimplify):
        raise CanNotSimplifyFunctionEvaluation
    else:
        return HigherLevelCodeInt(argument_left * argument_right)

def function_div(arguments):
    '''Implements the div function'''
    logging.debug('Called function div with arguments %s', str(arguments))
    try:
        argument_left = extract_simplify(arguments[0], type_restriction=[int])
        argument_right = extract_simplify(arguments[1], type_restriction=[int])
    except (TypeError, NeedToSimplify):
        raise CanNotSimplifyFunctionEvaluation
    else:
        if argument_right == 0:
            raise ValueError('Division by zero in div function')
        if argument_left == 0:
            return HigherLevelCodeInt(0)
        sign_left = argument_left // abs(argument_left)
        sign_right = argument_right // abs(argument_right)
        return HigherLevelCodeInt(
            (abs(argument_left) // abs(argument_right)) * sign_left * sign_right)


def function_add(arguments):
    '''Implements the add function'''
    logging.debug('Called function add with arguments %s', str(arguments))
    try:
        argument_left = extract_simplify(arguments[0], type_restriction=[int])
        argument_right = extract_simplify(arguments[1], type_restriction=[int])
    except (TypeError, NeedToSimplify):
        raise CanNotSimplifyFunctionEvaluation
    else:
        return HigherLevelCodeInt(argument_left + argument_right)


def construct_hlc_concrete_function(name):
    '''Takes a name and constructs a concrete hlc function from defined_functions'''
    function, number_of_arguments, types, type_return = defined_functions[name]
    if name in ["t", "f"]:
        return HigherLevelCodeBool(function, number_of_arguments,
                                   types, type_return, name=name)
    return HigherLevelCodeConcreteFunction(function, number_of_arguments, types,
                                           type_return, name=name)



def function_isnil(arguments):
    '''Implements the isnil function'''
    argument = arguments[0]
    logging.debug('Called function isnil with argument %s', str(argument))
    if isinstance(argument, HigherLevelCodeLambdaVariable):
        logging.debug('isnil will be applied to a lambda variable, creating if block')
        condition = HigherLevelCodeConditionComparision("==",
                                                        argument,
                                                        HigherLevelCodeList([]))
        true_branch = construct_hlc_concrete_function("t")
        false_branch = construct_hlc_concrete_function("f")
        if_block = HigherLevelCodeIf(condition, true_branch, false_branch)
        return if_block
    raise NotImplementedError


def function_lt(arguments):
    '''Implements the lt function'''
    logging.debug('Called function lt with arguments %s', str(arguments))
    argument_left = arguments[0]
    argument_right = arguments[1]
    condition = HigherLevelCodeConditionComparision("<",
                                                    argument_left,
                                                    argument_right)
    true_branch = construct_hlc_concrete_function("t")
    false_branch = construct_hlc_concrete_function("f")
    if_block = HigherLevelCodeIf(condition, true_branch, false_branch)
    return if_block

def function_eq(arguments):
    '''Implements the eq function'''
    logging.debug('Called function eq with arguments %s', str(arguments))
    argument_left = arguments[0]
    argument_right = arguments[1]
    condition = HigherLevelCodeConditionComparision("==",
                                                    argument_left,
                                                    argument_right)
    true_branch = construct_hlc_concrete_function("t")
    false_branch = construct_hlc_concrete_function("f")
    if_block = HigherLevelCodeIf(condition, true_branch, false_branch)
    return if_block


def function_i(arguments):
    '''Implements the i function'''
    logging.debug('Called function i with arguments %s', str(arguments))
    return arguments[0]


def function_car(arguments):
    '''Implements the car function'''
    argument = arguments[0]
    logging.debug('Called function car with argument %s', str(argument))
    if isinstance(argument, HigherLevelCodeLambdaVariable):
        raise CanNotSimplifyFunctionEvaluation
    raise NotImplementedError

def function_cdr(arguments):
    '''Implements the cdr function'''
    argument = arguments[0]
    logging.debug('Called function cdr with argument %s', str(argument))
    try:
        argument = extract_simplify(argument, type_restriction=[list, tuple])
    except (TypeError, NeedToSimplify):
        raise CanNotSimplifyFunctionEvaluation
    else:
        if isinstance(argument, tuple):
            return argument[1]
        if isinstance(argument, list):
            return HigherLevelCodeList(argument[1:])
        raise NotImplementedError


defined_functions = {
    'b': (function_b, 3, [HigherLevelCodeFunction, HigherLevelCodeFunction, None], None),
    'c': (function_c, 3, [HigherLevelCodeFunction, None, None], None),
    's': (function_s, 3, [HigherLevelCodeFunction, HigherLevelCodeFunction, None], None),
    'cons': (function_cons, 2, [None, None], None),
    'neg': (function_neg, 1, [HigherLevelCodeInt], HigherLevelCodeInt),
    'isnil': (function_isnil, 1, [HigherLevelCodeList], HigherLevelCodeIf),
    'car': (function_car, 1, [None], None),
    'cdr': (function_cdr, 1, [None], None),
    't': (function_true, 2, [None, None], None),
    'f': (function_false, 2, [None, None], None),
    'lt': (function_lt, 2, [HigherLevelCodeInt, HigherLevelCodeInt], HigherLevelCodeIf),
    'i': (function_i, 1, [None], None),
    'eq': (function_eq, 2, [None, None], HigherLevelCodeIf),
    'mul': (function_mul, 2, [HigherLevelCodeInt, HigherLevelCodeInt], HigherLevelCodeInt),
    'add': (function_add, 2, [HigherLevelCodeInt, HigherLevelCodeInt], HigherLevelCodeInt),
    'div': (function_div, 2, [HigherLevelCodeInt, HigherLevelCodeInt], HigherLevelCodeInt),
        }

def variable_name_generator(prefix="x"):
    '''Generator that returns distinct names for variables'''
    i = 1
    while True:
        yield prefix + str(i)
        i += 1



class Assembly: # pylint: disable=too-few-public-methods
    '''Holds a message in the alien assembly language and decompiles it'''
    def __init__(self, message):
        self.lines = message.splitlines()
        logging.debug("Loaded assembly with %d lines", len(self.lines))
        self.global_variable_store = dict()

    def get_lhs_global_name(self, line_number):
        tokens = extract_tokens(self.lines[line_number])
        lhs_parse = tokens_to_higher_level_code(tokens)
        if not isinstance(lhs_parse, HigherLevelCodeGlobalVariable):
            raise Exception(f'Line {line_number}: Left hand side is not a variable!')
        return lhs_parse.name()


    def parse_line(self, line_number):
        '''Parses a single line'''
        logging.info('Parsing line %d: "%s"', line_number, self.lines[line_number])
        tokens = extract_tokens(self.lines[line_number])
        lhs_parse = tokens_to_higher_level_code(tokens)
        if not isinstance(lhs_parse, HigherLevelCodeGlobalVariable):
            raise Exception(f'Line {line_number}: Left hand side is not a variable!')
        if not next(tokens) == "=":
            raise Exception(f'Line {line_number}: Second token not "="')
        try:
            rhs_parse = tokens_to_higher_level_code(tokens)
            global global_debug_track
            global_debug_track = rhs_parse
        except RecursionError:
            logging.critical('Hit recursion depth limit of %d. Quitting. '
                             'You might want to increase it.',
                             sys.getrecursionlimit())
            sys.exit()
        else:
            logging.info('Line %d: %s = %s', line_number, str(lhs_parse), str(rhs_parse))
            #rhs_parse = term_rhs.parse_to_higher_level_code()
            simplification_round = 0
            while not rhs_parse.is_fully_simplified():
                simplification_round += 1
                logging.info('Top level parsing; simplification round %d', simplification_round)
                logging.info('Current parse object:\n%s', rhs_parse)
                rhs_parse = rhs_parse.simplify(inject_variable_depth=math.inf)
            rhs_parse.name_variables(variable_name_generator())
            return (lhs_parse, rhs_parse)

    def parse_line_decompiled_string(self, line_number):
        '''Parses a single line into a decompiled strin'''
        term_lhs, rhs_parse = self.parse_line(line_number)
        rhs_parse_string = rhs_parse.fancy_str(pre=INDENTATION*2, depth=DISPLAYDEPTH)
        logging.info('Line %d: %s = %s',
                     line_number, str(term_lhs), rhs_parse_string)
        return "".join([str(term_lhs), ' =\n', rhs_parse_string])

    def sugar_file(self, filename):
        '''Write to that file a sugared version'''
        logging.info('Writing sugared version to %s', filename)
        with open(filename, 'w') as filehandle:
            for line_number in range(0, len(self.lines)):
                filehandle.flush()
                try:
                    term_lhs, rhs_parse = self.parse_line(line_number)
                except: # pylint: disable=bare-except
                    logging.info('Failed to parse line %d, writing original', line_number)
                    filehandle.write(self.lines[line_number])
                    filehandle.write('\n')
                else:
                    try:
                        sugar_string_rhs = rhs_parse.sugar_string()
                    except Exception as exception: # pylint: disable=broad-except
                        logging.info('Failed to make sugar string, exception:\n%s', exception)
                        filehandle.write(self.lines[line_number])
                        filehandle.write('\n')
                    else:
                        logging.info('Sugar string for right hand side of line %d is %s',
                                     line_number, sugar_string_rhs)
                        filehandle.write(':')
                        filehandle.write(str(term_lhs.variable_number))
                        filehandle.write(' = ')
                        filehandle.write(sugar_string_rhs)
                        filehandle.write('\n')

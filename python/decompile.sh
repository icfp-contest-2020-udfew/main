#!/bin/sh

# Copyright 2020 Malte Leip <malte@leip.net>

# Copyright 2020 Mark Pedron

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

# Warning: This script should not be sourced!
cd -- "$(dirname "$(readlink -f -- "$0")")" || \
  { echo "Failed to change to top Python dir of repository!" ; exit 1 ; }

# No need to deactivate, as this script should be run in a subshell
# shellcheck disable=SC1091
. ./activate

echo "Downloading galaxy.txt"
[ -f /tmp/python.galaxy.txt ] ||
  curl https://message-from-space.readthedocs.io/en/latest/_downloads/48286a0b4dac94efc1b23fb2c3a41bef/galaxy.txt --silent >/tmp/python.galaxy.txt
timeout 10m python decompile.py --timeout 3 --input /tmp/python.galaxy.txt --all

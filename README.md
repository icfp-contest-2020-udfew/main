Team udfew's ICFPC 2020 project.

Material in the `third-party` subdirectory is from third parties, and may
be provided under a different license than the rest. See the `LICENSE`,
`COPYING` or similar files in the respective subdirectories.

Most of the rest of the files is part of icfp-contest-mmxx-udfew and provided
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

# Table of contents

- [The Galaxy Pad pipeline](#the-galaxy-pad-pipeline)
  * [Modulator/Demodulator](#modulatordemodulator)
  * [Evaluator](#evaluator)
  * [Visualizer](#visualizer)
- [Playing the game](#playing-the-game)
- [Galaxy reverse engineering](#galaxy-reverse-engineering)
  * [Decompiler](#decompiler)
    + [Usage](#usage)
    + [Example](#example)
    + [Internals](#internals)
      - [Tokenizing](#tokenizing)
      - [Building up the tree](#building-up-the-tree)
      - [Simplification](#simplification)
  * [Value hunt](#value-hunt)
- [Team members](#team-members)

# The Galaxy Pad pipeline

## Modulator/Demodulator

The Haskell module contains a library for dealing with modulation and
demodulation between bitstrings and LISP-like data structures built from
integer literals, Cons pairs and the Nil literal as specified in the alien
language.

The "modem" binary provides a convenient frontend for both functionalities,
to be used in other scripts. It takes a string argument, parses it as data
structure as described above, with Cons pairs are simply given by parentheses
(a,b) and Nil is given by []. It then outputs the result of modulation as
string of '0's and '1's on stdout.

If the optional argument -d was passed, the program instead demodulates its
argument: It parses it as string of '0's and '1's, demodulates the resulting
bitstring, and outputs (a string representing) the resulting data.

Both directions support some mild syntactic sugar in the form of list literals:
Any construct of the form "[a,b,c,d]" or similar gets interpreted as the
corresponding nested application of Cons and Nil, in this case
"(a,(b,(c,(d,[]))))". In demodulation mode, any nested Cons sequence ending in
Nil gets output in list notation.

## Evaluator

There is a Galaxy evaluator written in Haskell in
`haskell/src/AlienCalculus/Evaluation.hs`.

## Visualizer

There is a prototyp of visualization tool based on pygame in
`scripts/visualization.py`. The script takes on stdin a list of pictures in the
demodulated from and displays them in two modes, either blended together or frame by frame.
When the user clicks on a pixel, it sends the corresponding coordinates back to stdout.

Controls:

* Click: sending the mouse position to stdout in the coordinate system of the input
* "s": switch between display modes (blended and frame by frame)
* Left and Right: go through the frames in the frame by frame mode

# Playing the game

We wrote bots in:

* scripting languages like Bash and PHP (see the branch
`submissions/diagnose`),
* Haskell (see the directory `haskell/src/Game` and the branch
`submissions/haskell-orbit`).

# Galaxy reverse engineering

## Decompiler

To help in analyzing `galaxy.txt`, it might be useful to decompile it into a
higher-level language. Ultimately, this might allow reimplementing the galaxy
function in a more performant way, without needing to simulate execution of
`galaxy.txt` from the alien assembly.

With this in mind, a decompiler (at the moment not yet supporting the full
alien assembly language documented in [the alien
messages](https://message-from-space.readthedocs.io/en/latest/index.html))
has been implemented in `python/udfew/alienmessages/decompiler.py`, with a
command-line tool in `python/decompile.py`.


### Usage

To get an explanation of how to use the command-line tool, try `python
decompile.py --help` (run this with Python version `3.8`, preferably from a
venv created by calling `make python-build` - the venv can be activated by
running `. ./activate` in the `python` subdirectory).


### Example

Line 78 (or line 77 when starting to count from 0, as the decompiler does) of
`galaxy.txt` is as follows:

```
:1120 = ap ap s ap ap s ap ap c ap c ap ap s ap ap b s ap ap b ap b ap ap s i i lt eq 0 i neg
```

The decompiler produces the following output for this line:
```
G{:1120} =
    (λ x1<int>:
        if(0 < x1<int>)
        {
            x1<int>
        }
        else
        {
            if(0 == x1<int>)
            {
                x1<int>
            }
            else
            {
                neg1(x1<int>)
            }
        }
    )
```
From which it is now easy to see that `:1120` is the `abs` function.


### Internals

#### Tokenizing

As a first step, the line is split up into tokens. Luckily, this is very easy
in the format the alien assembly language is given in, as it suffices to split
at whitespace.

#### Building up the tree

As a second step, the tokens are scanned one by one and a syntax tree is built
up. For nearly all tokens, this merely means wrapping the token in a class -
for example `int`s are wrapped in a class `HigherLevelCodeInt` that does not do
much more than hold a Python int, while functions like `cons` are wrapped in a
subclass of `HigherLevelCodeFunction`, which has a member function for plugging
in arguments. The one exception is `ap`, which first builds up the next two
nodes, say `f` and `x`, and then calls `f.plug_in(x)`.
The end result will be a nesting of functions, most of which usually not not
have all argument slots filled (they should be interpreted as lambda
functions).

In the example above, the result after this step can be represented as follows:
```
s3(s3(c3(c3(s3(b3(s3, b3(b3(s3(i1, i1, _), _, _), lt2, _), _), eq2, _), _, _), 0, _), i1, _), neg1, _)
```

#### Simplification

As the third and final step, everything is simplified as far as possible. When
trying to simplify itself, every node of the tree calls the `simplify` member
function of its children as well. Those nodes that are merely wrappers, like
for `int`s, can not be simplified. The main simplification step is "evaluating"
a function of arity `n` that happens to have all `n` argument slots filled.  In
some cases, the result of such a simplification is straightforward, for example
`cons(1,[])` simplifies to `[1]`. In other cases, the simplification might be
better described as a transformation into a higher-level representation, or as
an intermediary step needed for further simplifications.  An example would be
`eq(0,x1)` (where `x1` is a lambda variable, described below), which simplifies
to
```
if(0 == x1)
{
  True
}
else
{
  False
}
```
In a further step, two arguments (say `x2` and `x3`) might be plugged into
this, resulting in
```
if(0 == x1)
{
  True(x2, x3)
}
else
{
  False(x2, x3)
}
```
which can again be simplified by evaluating `True` and `False`, giving:
```
if(0 == x1)
{
  x2
}
else
{
  x3
}
```

Often however, the functions do *not* have all their slots already filled. In
this case, they are replaced by an explicit lambda function with variables,
e.g. `cons(0, _)` may be transformed to `λ x1: cons(0, x1)`. This allows
continuing the simplification process. Some evaluations might of course not be
possible with variables, such as summing two variables. In those cases we may
however still deduce something about the type the variable most likely must
have (such as an `int`), and a type annotation is added. The process of
"lambdaifying" functions that can not yet be evaluated starts from the top of
the tree.

There are also a number of less important simplifications, for example
squashing together nested lambda functions, or eliminating branches in nested
`if`s (i.e.  if an `if` within a branch of another `if` uses the same
condition, then we know the truth value that this condition must have in the
inner `if`).

## Value hunt

We also wrote a tool that extracts hard-coded values from Galaxy files (cf.
`haskell/app/GalaxyValues.hs` and `haskell/src/AlienCalculus/Values.hs`)
Some notable such values that we stumbled upon are as the following:

The following list of numbers that shows up in one of the alien species
screens is hard-coded in `galaxy.txt`:
```
[122,203,410,164,444,484,202,77,251,56,456,435,28,329,257,265,501,18,190,423,384,434,266,69,34,437,203,152,160,425,245,428,99,107,192,372,346,344,169,478,393,502,201,497,313,32,281,510,436,22,237,80,325,405,184,358,57,276,359,189,284,277,198,244]
```
We don't know whether this has any meaning beyond being a reverse engineering
hint.

In one of the inlined versions of `galaxy.txt` we created
(cf. `haskell/src/AlienCalculus/Inliner.hs`), we found the list
```
[[2,7],[2,7],[4,21855],[7,560803991675135],66,[5,30309607]]
```
which suspiciously matched the pattern on the top left of the first "Galaxy
screen".
Based on this, we figured out that some "sprites" are encoded as a list
`[s, d]` where `s` is the size of the grid in which the sprite will be drawn
and `d` is the number that will be drawn according to pixel grid
representation of numbers (without the "half frame" that otherwise encodes
in which range the number is in).

There is also a tool to render such sprites
(`haskell/app/GalaxySprites.hs` and `haskell/src/AlienCalculus/Sprites.hs`)
and we have successfully reproduced sprites like the Galaxy symbol
(`[7, 123229502148636]`), the "versus symbol" (`[7, 560819095462143]`)
etc.

As a next step, we looked at "common iterated callers" of all sprites which
are the variables `:1227`, `:1228`, `:1336`, `:1338`, `:1445`, `:1446`.
Based on this and some inspection of `galaxy.txt`, we guess that rendering
logic might be in `:1228` and/or `:1446`.

There are also rather long lists of numbers around 8M. Originally we thought
that these could be some kind of sprites as well, but couldn't render them
into anything meaningful. Another idea is that these could be series of encoded
game states that are used in the tutorials.

# Team members

* Achim Krause
* Aras Ergus
* Bernhard Reinke
* Gustavo Jasso
* HeineBorel
* Malte Leip
* Mark Pedron
* Robin Stoll
* Sílvia Cavadas
* Tashi Walde

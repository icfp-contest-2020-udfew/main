#!/bin/bash

# Copyright 2020 Aras Ergus

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


set -e

echo -e "RES\tEXP\tMODE\tCODE"

compile_and_run () {
  CODE="$(echo 'galaxy =' $2 | stack exec ac-to-hs $1)"
  RESULT="$(echo -e "$CODE\nmain = print galaxy" | stack exec runhaskell)"
  echo -e "$RESULT\t$3\t$1\t$2"
  [ "$RESULT" == "$3" ]
}

# 2 = 2
compile_and_run l "2" 2
compile_and_run n "2" 2
# 4 + 2 = 6
compile_and_run l "ap ap add 4 2" 6
compile_and_run n "ap ap add 4 2" 6
# 3 * (4 + 2) = 18
compile_and_run l "ap ap mul 3 ap ap add 4 2" 18
compile_and_run n "ap ap mul 3 ap ap add 4 2" 18
# (3 * (4 + 2)) / -5 = -3
compile_and_run l "ap ap div ap ap mul 3 ap ap add 4 2 -5" -3
compile_and_run n "ap ap div ap ap mul 3 ap ap add 4 2 -5" -3
# -((3 * (4 + 2)) / -5) = 3
compile_and_run l "ap neg ap ap div ap ap mul 3 ap ap add 4 2 -5" 3
compile_and_run n "ap neg ap ap div ap ap mul 3 ap ap add 4 2 -5" 3
# Id 2 = 2
compile_and_run l "ap i 2" 2
compile_and_run n "ap i 2" 2
# True = fst
compile_and_run l "ap ap t 0 1" 0
compile_and_run n "t" "True"
# 0 == 0 = True
compile_and_run l "ap ap ap ap eq 0 0 5 7" 5
compile_and_run n "ap ap eq 0 0" "True"
# 1 == 0 = False
compile_and_run l "ap ap ap ap eq 1 0 5 7" 7
compile_and_run n "ap ap eq 1 0" "False"
# 0 < 1 = True
compile_and_run l "ap ap ap ap lt 0 1 5 7" 5
compile_and_run n "ap ap lt 0 1" "True"
# 1 < 0 = False
compile_and_run l "ap ap ap ap lt 1 0 5 7" 7
compile_and_run n "ap ap lt 1 0" "False"
# IsNil Nil = True
compile_and_run l "ap ap ap isnil nil 5 7" 5
compile_and_run n "ap isnil nil" "True"
# IsNil (17, Nil) = False
compile_and_run l "ap ap ap isnil ap ap cons 17 nil 5 7" 7
compile_and_run n "ap isnil ap ap cons 17 nil" "False"
# Fst (17, Nil) = 17
compile_and_run l "ap car ap ap cons 17 nil" 17
compile_and_run n "ap car ap ap cons 17 nil" 17
# IsNil (Snd (17, Nil)) = True
compile_and_run l "ap ap ap isnil ap cdr ap ap cons 17 nil 5 7" 5
compile_and_run n "ap isnil ap cdr ap ap cons 17 nil" "True"
# B (add 1) (add 2) 0 = 1 + (2 + 0) =  3
compile_and_run l "ap ap ap b ap add 1 ap add 2 0" 3
compile_and_run n "ap ap ap b ap add 1 ap add 2 0" 3
# C div 3 15 = div 15 3 = 15
compile_and_run l "ap ap ap c div 3 15" 5
compile_and_run n "ap ap ap c div 3 15" 5
# S add neg = const 0
compile_and_run l "ap ap ap s add neg 42" 0
compile_and_run n "ap ap ap s add neg 42" 0

-- Copyright 2020 Achim Krause, Aras Ergus, Bernhard Reinke, Gustavo Jasso, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

import ModulateSpec
import ParseTermSpec
import AlienCalculus.EvaluationSpec
import AlienCalculus.ParserSpec
import AlienCalculus.DumpSpec
import Game.LoopSpec
import KeplerSpec

main :: IO ()
main = do evaluationSpec 
          modulateSpec
          parseTermSpec
          parserSpec
          dumpSpec
          gameLoopSpec
          keplerSpec

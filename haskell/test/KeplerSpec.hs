-- Copyright 2020 Achim Krause, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module KeplerSpec (keplerSpec) where

import Test.Hspec
-- import Test.QuickCheck

import Kepler


examples :: [(Vec, PosV, PosV)]
examples = [
             ((0,0), PosV{position=(10,0), velocity=(0,0)}, PosV{position=(9,0), velocity=(-1,0)}),
             ((-1,0), PosV{position=(10,0), velocity=(0,0)}, PosV{position=(10,0), velocity=(0,0)}),
             ((0,0), PosV{position=(-10,0), velocity=(0,0)}, PosV{position=(-9,0), velocity=(1,0)}),
             ((0,0), PosV{position=(0,10), velocity=(0,0)}, PosV{position=(0,9), velocity=(0,-1)}),
             ((0,0), PosV{position=(0,-10), velocity=(0,0)}, PosV{position=(0,-9), velocity=(0,1)}),
             ((0,0), PosV{position=(10,10), velocity=(0,0)}, PosV{position=(9,9), velocity=(-1,-1)}),
             ((0,0), PosV{position=(-10,-10), velocity=(0,0)}, PosV{position=(-9,-9), velocity=(1,1)}),
             ((0,0), PosV{position=(10,-10), velocity=(0,0)}, PosV{position=(9,-9), velocity=(-1,1)}),
             ((0,0), PosV{position=(-10,10), velocity=(0,0)}, PosV{position=(-9,9), velocity=(1,-1)}),
             ((-1,-1), PosV{position=(5,5), velocity=(2,0)}, PosV{position=(7,5), velocity=(2,0)})
           ]

test :: (Vec , PosV, PosV) -> Bool
test (boost, posV , result) =  update boost posV == result


keplerSpec :: IO ()
keplerSpec = hspec $ do
  describe "kepler" $ do
    it "update correctly" $
      all test examples `shouldBe` True

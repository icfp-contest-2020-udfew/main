#!/bin/bash

# Copyright 2020 Aras Ergus

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


set -e

echo -e "RES\tEXP\tPRELUDE\tCODE"

compile_and_run () {
  CODE="$(echo 'galaxy =' $2 | stack exec ac-to-js $1)"
  RESULT="$(echo -e "$CODE\nconsole.log(galaxy);" | node)"
  echo -e "$RESULT\t$3\t$1\t$2"
  [ "$RESULT" == "$3" ]
}

# 2 = 2
compile_and_run "../../javascript/prelude-church.js" "2" 2
compile_and_run "../../javascript/prelude-native.js" "2" 2
# 4 + 2 = 6
compile_and_run "../../javascript/prelude-church.js" "ap ap add 4 2" 6
compile_and_run "../../javascript/prelude-native.js" "ap ap add 4 2" 6
# 3 * (4 + 2) = 18
compile_and_run "../../javascript/prelude-church.js" "ap ap mul 3 ap ap add 4 2" 18
compile_and_run "../../javascript/prelude-native.js" "ap ap mul 3 ap ap add 4 2" 18
# (3 * (4 + 2)) / -5 = -3
compile_and_run "../../javascript/prelude-church.js" "ap ap div ap ap mul 3 ap ap add 4 2 -5" -3
compile_and_run "../../javascript/prelude-native.js" "ap ap div ap ap mul 3 ap ap add 4 2 -5" -3
# -((3 * (4 + 2)) / -5) = 3
compile_and_run "../../javascript/prelude-church.js" "ap neg ap ap div ap ap mul 3 ap ap add 4 2 -5" 3
compile_and_run "../../javascript/prelude-native.js" "ap neg ap ap div ap ap mul 3 ap ap add 4 2 -5" 3
# Id 2 = 2
compile_and_run "../../javascript/prelude-church.js" "ap i 2" 2
compile_and_run "../../javascript/prelude-native.js" "ap i 2" 2
# True = fst
compile_and_run "../../javascript/prelude-church.js" "ap ap t 0 1" 0
compile_and_run "../../javascript/prelude-native.js" "t" "true"
# 0 == 0 = True
compile_and_run "../../javascript/prelude-church.js" "ap ap ap ap eq 0 0 5 7" 5
compile_and_run "../../javascript/prelude-native.js" "ap ap eq 0 0" "true"
# 1 == 0 = False
compile_and_run "../../javascript/prelude-church.js" "ap ap ap ap eq 1 0 5 7" 7
compile_and_run "../../javascript/prelude-native.js" "ap ap eq 1 0" "false"
# 0 < 1 = True
compile_and_run "../../javascript/prelude-church.js" "ap ap ap ap lt 0 1 5 7" 5
compile_and_run "../../javascript/prelude-native.js" "ap ap lt 0 1" "true"
# 1 < 0 = False
compile_and_run "../../javascript/prelude-church.js" "ap ap ap ap lt 1 0 5 7" 7
compile_and_run "../../javascript/prelude-native.js" "ap ap lt 1 0" "false"
# IsNil nil = True
compile_and_run "../../javascript/prelude-church.js" "ap ap ap isnil nil 5 7" 5
compile_and_run "../../javascript/prelude-native.js" "ap isnil nil" "true"
# IsNil (17, nil) = False
compile_and_run "../../javascript/prelude-church.js" "ap ap ap isnil ap ap cons 17 nil 5 7" 7
compile_and_run "../../javascript/prelude-native.js" "ap isnil ap ap cons 17 nil" "false"
# Fst (17, nil) = 17
compile_and_run "../../javascript/prelude-church.js" "ap car ap ap cons 17 nil" 17
compile_and_run "../../javascript/prelude-native.js" "ap car ap ap cons 17 nil" 17
# IsNil (Snd (17, nil)) = True
compile_and_run "../../javascript/prelude-church.js" "ap ap ap isnil ap cdr ap ap cons 17 nil 5 7" 5
compile_and_run "../../javascript/prelude-native.js" "ap isnil ap cdr ap ap cons 17 nil" "true"
# B (add 1) (add 2) 0 = 1 + (2 + 0) =  3
compile_and_run "../../javascript/prelude-church.js" "ap ap ap b ap add 1 ap add 2 0" 3
compile_and_run "../../javascript/prelude-native.js" "ap ap ap b ap add 1 ap add 2 0" 3
# C div 3 15 = div 15 3 = 15
compile_and_run "../../javascript/prelude-church.js" "ap ap ap c div 3 15" 5
compile_and_run "../../javascript/prelude-native.js" "ap ap ap c div 3 15" 5
# S add neg = const 0
compile_and_run "../../javascript/prelude-church.js" "ap ap ap s add neg 42" 0
compile_and_run "../../javascript/prelude-native.js" "ap ap ap s add neg 42" 0

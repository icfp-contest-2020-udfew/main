#!/bin/bash

# Copyright 2020 Achim Krause, Mark Pedron

# This file is part of icfp-contest-mmxx-udfew.
#
# icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


set -ex

[ "$(stack exec modem -- '(1, (2, []))')" == '1101100001110110001000' ]
[ "$(stack exec modem -- '[1,2]')" == '1101100001110110001000' ]
[ "$(stack exec modem -- -d '1101100001110110001000')" == '[1, 2]' ]

-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.DumpSpec
  ( dumpSpec
  )
where

import           Test.Hspec

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Dump
import           AlienCalculus.Parser

import           Basement.From
import           Data.Either
import           Data.Maybe

import qualified Data.IntMap                   as IM

terms :: [Term]
terms =
  [ 17
  , ApT (ApT (from AddF) 1)         12
  , ApT (ApT (from ConsF) 3)        (from NilF)
  , ApT (VarT 12)                   (from True)
  , ApT (ApT (from BF) (from NegF)) (from NegF)
  ]

testTermDump :: Term -> Bool
testTermDump t = Just t == (parseTerm . dumpTerm) t

testContextDump :: [(Int, Term)] -> Bool
testContextDump ctx = Just True == do
  let ctxMap = IM.fromList ctx
  assigs <- parseAssignments . dumpContext $ ctxMap
  let namedVars = rights assigs
      newCtxMap = IM.fromList . lefts $ assigs
  return $ null namedVars && (ctxMap == newCtxMap)

dumpSpec :: IO ()
dumpSpec = hspec $ do
  describe "dump" $ do
    it "dump terms correctly" $ all testTermDump terms
    it "dump contexts correctly" $ testContextDump (zip [1 ..] terms)

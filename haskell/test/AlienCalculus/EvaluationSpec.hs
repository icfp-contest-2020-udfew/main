-- Copyright 2020 Bernhard Reinke, Robin Stoll, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.EvaluationSpec (evaluationSpec) where

import Test.Hspec
-- import qualified Test.QuickCheck as QC

import AlienCalculus.AlienCalculus
import AlienCalculus.Evaluation
import AlienCalculus.SyntacticSugar

import Basement.From

import qualified Data.IntMap as IM

type OperatorTest = (Term -> Term -> Term, Integer, Integer, Integer)
type CompareTest = (Term -> Term -> Term, Integer, Integer, Bool)

exampleAdd :: [OperatorTest]
exampleAdd =  [ ((+), 1, 2, 3)
              , ((+), 2, 1, 3)
              , ((+), 0, 1, 1)
              , ((+), 2, 3, 5)
              , ((+), 3, 5, 8)
              ]

exampleMul :: [OperatorTest]
exampleMul = [ ((*), 4, 2, 8)
             , ((*), 3, 4, 12)
             , ((*), 3, -2, -6)
             ]


exampleDiv :: [OperatorTest]
exampleDiv = [ (divF, 4, 2, 2)
             , (divF, 4, 3, 1)
             , (divF, 4, 4, 1)
             , (divF, 4, 5, 0)
             , (divF, 5, 2, 2)
             , (divF, 6, -2, -3)
             , (divF, 5, -3, -1)
             , (divF, -5, 3, -1)
             , (divF, -5, -3, 1)
             ]

exampleEq :: [CompareTest]
exampleEq = [ (eqF, x, y, x == y) | x <- [-3..3], y <- [-3..3] ]

exampleLT :: [CompareTest]
exampleLT = [ (lTF, x, y, x < y) | x <- [-3..3], y <- [-3..3] ]

testEvalNumericalOperator :: OperatorTest -> Bool
testEvalNumericalOperator ((#), x, y, z) = eval IM.empty (from x # from y) == Right (from z)

testEvalCompare :: CompareTest -> Bool
testEvalCompare ((#), x, y, z) = eval IM.empty (boolToIntF (from x # from y)) == Right (if z then from (1 :: Integer) else from (0 :: Integer))

evaluationSpec :: IO ()
evaluationSpec = hspec $ do
  describe "eval" $ do
    it "add examples correctly" $
      all testEvalNumericalOperator exampleAdd `shouldBe` True
    it "mul examples correctly" $
      all testEvalNumericalOperator exampleMul `shouldBe` True
    it "div examples correctly" $
      all testEvalNumericalOperator exampleDiv `shouldBe` True
    it "eq examples correctly" $
      all testEvalCompare exampleEq `shouldBe` True
    it "LTF examples correctly" $
      all testEvalCompare exampleLT `shouldBe` True
    it "car . cons = id" $
      eval IM.empty (CarF $$ ((ConsF $$ intT 23) $$ intT 42)) `shouldBe` Right (intT 23)
    it "fstF . cons = id" $
      eval IM.empty (fstF $$ ((ConsF $$ intT 23) $$ intT 42)) `shouldBe` Right (intT 23)
    it "cdr . cons = id" $
      eval IM.empty (CdrF $$ ((ConsF $$ intT 23) $$ intT 42)) `shouldBe` Right (intT 42)
    it "snd . cons = id" $
      eval IM.empty (sndF $$ ((ConsF $$ intT 23) $$ intT 42)) `shouldBe` Right (intT 42)
    it "S example" $
      eval IM.empty (((SF $$ MulF) $$ (AddF $$ intT 1)) $$ intT 6) `shouldBe` Right (intT 42)
    it "true example" $
      eval IM.empty ((TF $$ intT 23) $$ intT 42) `shouldBe` Right (intT 23)
    it "constF example" $
      eval IM.empty ((constF $$ intT 23) $$ intT 42) `shouldBe` Right (intT 23)
    it "false example" $
      eval IM.empty ((falseF $$ intT 23) $$ intT 42) `shouldBe` Right (intT 42)
    it "C example" $
      eval IM.empty (((CF $$ DivF) $$ intT 23) $$ intT 42) `shouldBe` Right (intT 1)
    it "I example" $
      eval IM.empty (IF $$ intT 23) `shouldBe` Right (intT 23)
    it "B example" $
      eval IM.empty (((BF $$ (MulF $$ intT 6)) $$ (AddF $$ intT 1)) $$ intT 6) `shouldBe` Right (intT 42)
    it "nil definition example" $
      eval IM.empty (((NilF $$ intT 23) $$ intT 42) $$ intT 78) `shouldBe` Right (intT 42)
    it "isNil nil = true" $
      eval IM.empty (boolToIntF (IsNilF $$ NilF)) `shouldBe` Right (intT 1)
    it "isNil (cons a b) = false" $
      eval IM.empty (boolToIntF (IsNilF $$ ((ConsF $$ intT 23) $$ intT 42))) `shouldBe` Right (intT 0)




-- Copyright 2020 Gustavo Jasso, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.ParserSpec (parserSpec) where

import Test.Hspec
-- import Test.QuickCheck

import AlienCalculus.AlienCalculus
import AlienCalculus.Parser

exampleFunctions :: [(String, Function)]
exampleFunctions = [ ("add", AddF)]

funT :: Function -> Term
funT = LitT . FunL

intT :: Integer -> Term
intT = LitT . IntL

exampleTerms :: [(String, Term)]
exampleTerms = [ ("ap ap cons 7 ap ap cons 123229502148636 nil"
                 , ApT
                   (ApT (funT ConsF) (intT 7))
                   (ApT
                    (ApT (funT ConsF) (intT 123229502148636))
                    (funT NilF)))
               , (":45" , VarT 45)
               , ("ap :34 ap -15 neg"
                 , ApT (VarT 34) (ApT (intT $ -15) (funT NegF)))
               ]

exampleBadTerms :: [String]
exampleBadTerms = ["conspa"
                  , "ap s s  "
                  , "ap ap ap"
                  , "ap12"
                  , "apcons1"
                  , ":ap"
                  ]

testParseTerm :: (String, Term) -> Bool
testParseTerm (t, term) = parseTermString t == Just term

testParseFunction :: (String, Function) -> Bool
testParseFunction (t, fun) = parseTermString t == Just (funT fun)


parserSpec :: IO ()
parserSpec = hspec $ do
  describe "parseTermString" $ do
    it "parses some example terms correctly" $
      all testParseTerm exampleTerms
    it "parses some functions correctly" $
      all testParseFunction exampleFunctions
    it "correctly fails on some bad inputs" $
      all ((Nothing ==) . parseTermString) exampleBadTerms


-- Copyright 2020 Achim Krause

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module ParseTermSpec (parseTermSpec) where

import Test.Hspec
-- import Test.QuickCheck

import ParseTerm
import Modulate


exampleStrings :: [(String , Term)]
exampleStrings = [("[]", Nil),
                  ("5", Num 5),
                  ("([],[] )", Cons Nil Nil),
                  ("(-1 ,( 2, (3,  [])))", Cons (Num (-1)) (Cons (Num 2) (Cons (Num 3) Nil))),
                  ("[1,-2]", Cons (Num 1) (Cons (Num (-2)) Nil)),
                  ("[ [], [],(1,2) ]", Cons Nil (Cons Nil (Cons (Cons (Num 1) (Num 2)) Nil)))
                 ]

testParse :: (String, Term) -> Bool
testParse (str,t) = runParseTerm str == Right t


parseTermSpec :: IO ()
parseTermSpec = hspec $ do
  describe "parse" $ do
    it "parse example correctly" $
      all testParse exampleStrings `shouldBe` True

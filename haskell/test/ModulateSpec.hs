-- Copyright 2020 Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module ModulateSpec (modulateSpec) where

import Test.Hspec
-- import Test.QuickCheck

import Modulate


exampleNumbers :: [(Term , [Bit])]
exampleNumbers = [ (Num 0 , [O, I, O])
                 , (Num 1 , [O, I
                            , I, O
                            , O, O, O, I])
                 , (Num (-1) , [I, O, I, O, O, O, O, I])
                 ]

exampleConses :: [(Term , [Bit])]
exampleConses = [ (Nil , [O, O])
               , (Cons Nil Nil , [I, I, O, O, O, O])
               , (Cons (Num 1) (Cons (Num 2) Nil)
                 , [ I, I
                   , O, I, I, O, O, O, O, I
                   , I, I
                   , O, I, I, O, O, O, I, O
                   , O, O])
               ]

testModulate :: (Term , [Bit]) -> Bool
testModulate (t , bin) = modulate t == bin

testDemodulate :: (Term , [Bit]) -> Bool
testDemodulate (t , bin) = Just t == demodulate bin


modulateSpec :: IO ()
modulateSpec = hspec $ do
  describe "modulate" $ do
    it "number examples correctly" $
      all testModulate exampleNumbers `shouldBe` True
    it "cons examples correctly" $
      all testModulate exampleConses `shouldBe` True
  describe "demodulate" $ do
    it "number examples correctly" $
      all testDemodulate exampleNumbers `shouldBe` True
    it "cons examples correctly" $
      all testDemodulate exampleConses `shouldBe` True

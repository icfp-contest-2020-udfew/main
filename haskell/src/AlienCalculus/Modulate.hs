-- Copyright 2020 Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Modulate (fromModulateTerm, toModulateTerm) where

import AlienCalculus.AlienCalculus
import AlienCalculus.Evaluation
import AlienCalculus.SyntacticSugar

import qualified Modulate as Mod

import Basement.From
import Control.Monad.Except
import Data.STRef

import qualified Data.IntMap as IM


fromModulateTerm :: Mod.Term -> Term
fromModulateTerm (Mod.Num n) = intT $ fromIntegral n
fromModulateTerm Mod.Nil = from NilF
fromModulateTerm (Mod.Cons a b) =
  (ConsF $$ fromModulateTerm a) $$ fromModulateTerm b

toModulateTerm :: IM.IntMap Term -> Term -> Either String Mod.Term
toModulateTerm varC t =
  runEvalMonad varC $
    toModulateTerm' =<< newSTRef' =<< (liftST . unfreezeTerm) t

toModulateTerm' :: STermRef s -> EvalMonad s Mod.Term
toModulateTerm' tRef =
  do resRef <- evalStrict tRef
     res <- liftST $ readSTRef resRef
     case res
       of (LitST (IntL n)) -> return . Mod.Num $ fromIntegral n
          _ ->
            do isNil <- testNil resRef
               if isNil
                 then return Mod.Nil
                 else
                   do a <- toModulateTerm' =<< newSTRef' =<< ApST <$> newFst' <*> return resRef
                      b <- toModulateTerm' =<< newSTRef' =<< ApST <$> newSnd' <*> return resRef
                      return $ Mod.Cons a b

testNil :: STermRef s -> EvalMonad s Bool
testNil tRef =
  do a <- newSTRef' =<< ApST <$> newF' IsNilF <*> return tRef
     b <- newSTRef' =<< ApST <$> return a <*> newInt' 0
     testRef <- newSTRef' =<< ApST <$> return b <*> newInt' 1
     --res <- eval varC $ ((IsNilF $$ t) $$ intT 0) $$ intT 1
     res <- liftST . readSTRef =<< evalStrict testRef
     case res
       of (LitST (IntL 0)) -> return True
          (LitST (IntL 1)) -> return False
          _ -> throwError "Testing for nil failed"

newFst' :: EvalMonad s (STermRef s)
newFst' = newF' CarF

newSnd' :: EvalMonad s (STermRef s)
newSnd' = newF' CdrF

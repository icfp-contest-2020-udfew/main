-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Inliner where

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Evaluation

import           Control.Arrow
import           Control.Monad
import           Control.Monad.State
import           Control.Monad.Writer

import qualified Data.IntMap                   as IM
import qualified Data.IntSet                   as IS

type VarID = Int
type Inliner a = State VarContext a

callees :: Term -> IS.IntSet
callees (LitT _ ) = IS.empty
callees (ApT t s) = callees t `IS.union` callees s
callees (VarT i ) = IS.singleton i

hasNoVariables :: Term -> Bool
hasNoVariables = IS.null . callees

sinks :: VarContext -> [VarID]
sinks = IM.keys . IM.filter hasNoVariables

inlineTerm :: VarID -> Term -> Term -> Inliner Term
inlineTerm i t t' = case runWriter $ inlineTermW i t t' of
  (s, Any True) -> do
    ctx <- get
    case eval ctx s of
      Left err ->
        error
          $  "Error while inlining variable "
          ++ show i
          ++ " into the following term:\n"
          ++ show t
          ++ "\n"
          ++ show err
      Right s' -> return s'
  (s, Any False) -> return s

inlineTermW :: VarID -> Term -> Term -> Writer Any Term
inlineTermW _ _ s@(LitT _  ) = return s
inlineTermW i t (  ApT s s') = ApT <$> inlineTermW i t s <*> inlineTermW i t s'
inlineTermW i t s@(VarT j) | i == j    = tell (Any True) >> return t
                           | otherwise = return s

inlineVar :: VarID -> Inliner ()
inlineVar i = do
  ctx    <- get
  newCtx <- IM.delete i <$> mapM (inlineTerm i (ctx IM.! i)) ctx
  put newCtx

inlineSinksM :: Inliner Bool
inlineSinksM = do
  ls <- gets sinks
  if null ls
    then return False
    else mapM_ inlineVar ls >> inlineSinksM >> return True

callers :: VarID -> Inliner [VarID]
callers i =
  gets
    $ map fst
    . filter (IS.member i . snd)
    . (map . second) callees
    . IM.toList

varsWithSingleCaller :: Inliner [VarID]
varsWithSingleCaller = do
  keys        <- gets IM.keys
  callerLists <- zip keys <$> mapM callers keys
  return . map fst . filter (\(_, l) -> length l == 1) $ callerLists

reduceCyclesM :: Inliner Bool
reduceCyclesM = do
  is <- varsWithSingleCaller
  if null is
    then return False
    else let i = head is in inlineVar i >> reduceCyclesM >> return True

inlineM :: Inliner ()
inlineM = do
  goOn <- (||) <$> inlineSinksM <*> reduceCyclesM
  when goOn inlineM

inlineWith :: Inliner Bool -> VarContext -> VarContext
inlineWith conditionM = execState (go conditionM)
 where
  go cM = do
    condition <- cM
    when condition (go cM)

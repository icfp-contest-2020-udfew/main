-- Copyright 2020 Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Parser  where

import AlienCalculus.AlienCalculus
import Data.Text (unpack, pack, Text)
import Data.Functor (($>))
import Data.List (sortOn)
import qualified Data.Ord as O
import Control.Applicative ((<|>))
import qualified Data.Attoparsec.Text as AP
-- import qualified Data.IntMap as IM



functionDict :: [(String , Function)]
functionDict = [ ("add", AddF)
               , ("b", BF)
               , ("c", CF)
               , ("car", CarF)
               , ("cdr", CdrF)
               , ("cons", ConsF)
               , ("div", DivF)
               , ("eq", EqF)
               -- , ("galaxy", "F")
               , ("i", IF)
               , ("isnil", IsNilF)
               , ("lt", LTF)
               , ("mul", MulF)
               , ("neg", NegF)
               , ("nil", NilF)
               , ("s", SF)
               , ("t", TF)
               ]

stringNames :: [String]
stringNames = ["galaxy"]

matchString :: String -> a -> AP.Parser a
matchString str x = AP.string (pack str) $> x

functionParser :: AP.Parser Function
functionParser = AP.choice $ map (uncurry matchString)
               $ sortOn (O.Down . length . fst) functionDict

literalParser :: AP.Parser Literal
literalParser = (IntL <$> AP.signed AP.decimal) <|> (FunL <$> functionParser)

apParser :: AP.Parser Term
apParser = do _ <- AP.string (pack "ap")
              _ <- AP.space
              term1 <- termParser
              _ <- AP.space
              ApT term1 <$> termParser

varParser :: AP.Parser Int
varParser = AP.char ':' *> AP.decimal

termParser :: AP.Parser Term
termParser = (LitT <$> literalParser) <|> apParser <|> (VarT <$> varParser)

extractRight :: Either a b -> Maybe b
extractRight (Right t) = Just t
extractRight (Left _) = Nothing

parseTerm :: Text -> Maybe Term
parseTerm = extractRight . AP.parseOnly (termParser <* AP.endOfInput )

parseTermString :: String -> Maybe Term
parseTermString = parseTerm . pack

stringNameParser :: AP.Parser String
stringNameParser = fmap unpack $ AP.choice $ map (AP.string . pack) stringNames


type Assignment = Either (Int, Term) (String, Term)

oneAssignmentParser :: AP.Parser Assignment
oneAssignmentParser = (do lhs <- varParser
                          rhs <- rhsParser
                          return (Left (lhs, rhs))
                      ) <|>
                      (do lhs <- stringNameParser
                          rhs <- rhsParser
                          return (Right (lhs, rhs))
                      )
  where rhsParser = do _ <- AP.space
                       _ <- AP.char '='
                       _ <- AP.space
                       termParser
          

assignmentParser :: AP.Parser [Assignment]
assignmentParser = (AP.endOfInput $> [])
  <|> do x <- oneAssignmentParser
         xs <- assignmentParser
         return (x:xs)
  <|> (AP.endOfLine *> assignmentParser) --accidentally ignore empty lines
  
parseAssignments :: Text -> Maybe [Assignment]
parseAssignments = extractRight
                   . AP.parseOnly assignmentParser

parseAssignmentsString :: String -> Maybe [Assignment]
parseAssignmentsString = parseAssignments . pack

parseAssignmentsFile :: String -> IO (Maybe [Assignment])
parseAssignmentsFile = fmap parseAssignmentsString . readFile

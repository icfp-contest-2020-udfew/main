-- Copyright 2020 Aras Ergus, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module AlienCalculus.Evaluation where

import AlienCalculus.AlienCalculus
import AlienCalculus.SyntacticSugar
import AlienCalculus.Utilities

import Basement.From
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Control.Monad.State
import Control.Monad.Except
import Control.Monad.ST
import Data.STRef

import qualified Data.IntMap as IM


data STerm s =
    LitST Literal
  | ApST (STRef s (STerm s)) (STRef s (STerm s))
  | VarST Int

type STermRef s = STRef s (STerm s)

instance From Function (STerm s) where
  from = LitST . FunL

instance From Integer (STerm s) where
  from = LitST . IntL

unfreezeTerm :: Term -> ST s (STerm s)
unfreezeTerm (LitT l) = return $ LitST l
unfreezeTerm (VarT n) = return $ VarST n
unfreezeTerm (ApT t1 t2) =
  do refT1 <- newSTRef =<< unfreezeTerm t1
     refT2 <- newSTRef =<< unfreezeTerm t2
     return $ ApST refT1 refT2

freezeSTerm :: STerm s -> ST s Term
freezeSTerm (LitST l) = return $ LitT l
freezeSTerm (VarST n) = return $ VarT n
freezeSTerm (ApST refT1 refT2) =
  do t1 <- freezeSTerm =<< readSTRef refT1
     t2 <- freezeSTerm =<< readSTRef refT2
     return $ ApT t1 t2

type ApContext s = [(STermRef s, STermRef s)]

type VarContext = IM.IntMap Term

type Error = String

type EvalMonad s = ReaderT VarContext (ExceptT Error (StateT (ApContext s) (ST s)))
-- reader -> state s -> ST s (Either err a, state s)

liftST :: ST s a -> EvalMonad s a
liftST = lift . lift . lift


runEvalMonad :: VarContext -> (forall s . EvalMonad s a) -> Either String a
runEvalMonad varC m = runST $
  do eRes <- runStateT (runExceptT $ runReaderT m varC) []
     case eRes
       of (Left err, _) -> return $ Left err
          (Right res, []) -> return $ Right res
          _ -> return $ Left "Stack not empty"

runEvalMonadSTermRef :: VarContext -> (forall s . EvalMonad s (STermRef s)) -> Either String Term
runEvalMonadSTermRef varC m = runST $
  do res <- runStateT (runExceptT $ runReaderT m varC) []
     case res
       of (Left err, _) -> return $ Left err
          (Right resRef, []) ->
            do resT <- freezeSTerm =<< readSTRef resRef
               return $ Right resT
          (Right resRef, _) ->
            do resT <- freezeSTerm =<< readSTRef resRef
               return . Left $ "Not a function: " ++ show resT

eval :: IM.IntMap Term -> Term -> Either String Term
eval varC t = runEvalMonadSTermRef varC $
  do inputRef <- liftST . newSTRef =<< (liftST . unfreezeTerm) t
     eval' inputRef

evalStrict' :: VarContext -> STermRef s -> ST s (Either String (STermRef s))
evalStrict' varC inputRef =
  do res <- runStateT (runExceptT $ runReaderT (eval' inputRef) varC) []
     case res
       of (Left err, _) -> return $ Left err
          (Right resRef, []) -> return $ Right resRef
          (Right resRef, _) ->
            do resT <- freezeSTerm =<< readSTRef resRef
               return . Left $ "Not a function: " ++ show resT

evalStrict :: STermRef s -> EvalMonad s (STermRef s)
evalStrict refT =
  do varC <- ask
     resRef <- liftEither =<< liftST (evalStrict' varC refT)
     (liftST . writeSTRef refT) =<< (liftST . readSTRef) resRef
     return refT

eval' :: STermRef s -> EvalMonad s (STermRef s)
eval' ref =
  do t <- liftST $ readSTRef ref
     case t
       of (LitST (FunL f)) -> evalFun f
          (LitST (IntL _)) -> return ref
          (ApST t1 t2) ->
            do modify ((t2, ref) :)
               eval' t1
          (VarST v) ->
            do mT <- reader $ IM.lookup v
               case mT
                 of Nothing -> throwError $ "Variable " ++ show v ++ " not found"
                    Just vt -> eval' =<< liftST (newSTRef =<< unfreezeTerm vt)

intFun1
  :: Function
  -> (Integer -> Term)
  -> STermRef s
  -> EvalMonad s (STermRef s)
intFun1 f impl t1 =
  do x1 <- (liftST . readSTRef) =<< evalStrict t1
     case x1
       of LitST (IntL n) -> liftST $ newSTRef =<< unfreezeTerm (impl n)
          _ -> throwError $ "Integer function " ++ show f ++ " applied to non integers"

intFun2
  :: Function
  -> (Integer -> Integer -> Term)
  -> STermRef s
  -> STermRef s
  -> EvalMonad s (STermRef s)
intFun2 f impl t1 t2 =
  do x1 <- (liftST . readSTRef) =<< evalStrict t1
     x2 <- (liftST . readSTRef) =<< evalStrict t2
     case (x1, x2)
       of (LitST (IntL n), LitST (IntL m)) ->
            liftST $ newSTRef =<< unfreezeTerm (impl n m)
          _ -> throwError $ "Integer function " ++ show f ++ " applied to non integers"

evalFun1 :: Function -> (STermRef s -> EvalMonad s (STermRef s)) -> EvalMonad s (STermRef s)
evalFun1 f impl =
  do c <- get
     case c
       of ((t1, ref) : apC') ->
            do put apC'
               resRef <- impl t1
               (liftST . writeSTRef ref) =<< (liftST . readSTRef) resRef
               eval' resRef
          [] -> newSTRef' $ from f

evalFun2 :: Function -> (STermRef s -> STermRef s -> EvalMonad s (STermRef s)) -> EvalMonad s (STermRef s)
evalFun2 f impl =
  do c <- get
     case c
       of ((t1, _) : (t2, ref) : apC') ->
            do put apC'
               resRef <- impl t1 t2
               (liftST . writeSTRef ref) =<< (liftST . readSTRef) resRef
               eval' resRef
          [(_, ref)] ->
            do put []
               return ref
          [] -> newSTRef' $ from f

evalFun3 :: Function -> (STermRef s -> STermRef s -> STermRef s -> EvalMonad s (STermRef s)) -> EvalMonad s (STermRef s)
evalFun3 f impl =
  do c <- get
     case c
       of ((t1, _) : (t2, _) : (t3, ref) : apC') ->
            do put apC'
               resRef <- impl t1 t2 t3
               (liftST . writeSTRef ref) =<< (liftST . readSTRef) resRef
               eval' resRef
          [_, (_, ref)] ->
            do put []
               return ref
          [(_, ref)] ->
            do put []
               return ref
          [] -> newSTRef' $ from f

evalIntFun1 :: Function -> (Integer -> Term) -> EvalMonad s (STermRef s)
evalIntFun1 f impl = evalFun1 f $ intFun1 f impl

evalIntFun2 :: Function -> (Integer -> Integer -> Term) -> EvalMonad s (STermRef s)
evalIntFun2 f impl = evalFun2 f $ intFun2 f impl

returnsInt1 :: (Integer -> Integer) -> Integer -> Term
returnsInt1 f = LitT . IntL . f

returnsInt2 :: (Integer -> Integer -> Integer) -> Integer -> Integer -> Term
returnsInt2 f = (LitT . IntL) .: f

returnsBool2 :: (Integer -> Integer -> Bool) -> Integer -> Integer -> Term
returnsBool2 f = (\ x -> if x then trueF else falseF) .: f

evalFun :: Function -> EvalMonad s (STermRef s)
evalFun AddF = evalIntFun2 AddF $ returnsInt2 (+)
evalFun BF = evalFun3 BF $ \ f g x ->
  do inner <- newSTRef' $ ApST g x
     newSTRef' $ ApST f inner
evalFun CF = evalFun3 CF $ \ f x y ->
  do inner <- newSTRef' $ ApST f y
     newSTRef' $ ApST inner x
evalFun CarF = evalFun1 CarF $ \ f -> newSTRef' =<< ApST f <$> newTrue'
evalFun CdrF = evalFun1 CarF $ \ f -> newSTRef' =<< ApST f <$> newFalse'
evalFun ConsF = evalFun3 ConsF $ \ x y f ->
  do inner <- newSTRef' $ ApST f x
     newSTRef' $ ApST inner y
evalFun DivF = evalIntFun2 DivF $ returnsInt2 divToZero
evalFun EqF = evalIntFun2 EqF $ returnsBool2 (==)
evalFun IF = evalFun1 IF $ \ x -> return x
evalFun IsNilF =
  evalFun1 IsNilF $ \ l ->
    do innerst <- newSTRef' =<< ApST <$> newConst' <*> newFalse'
       inner <- newSTRef' =<< ApST <$> newConst' <*> return innerst
       newSTRef' $ ApST l inner
evalFun LTF = evalIntFun2 LTF $ returnsBool2 (<)
evalFun MulF = evalIntFun2 MulF $ returnsInt2 (*)
evalFun NegF = evalIntFun1 NegF $ returnsInt1 negate
evalFun NilF = evalFun1 NilF $ const newTrue'
evalFun SF = evalFun3 SF $ \ f g x ->
  do inner1 <- newSTRef' $ ApST f x
     inner2 <- newSTRef' $ ApST g x
     newSTRef' $ ApST inner1 inner2
evalFun TF = evalFun2 TF $ \ x _ -> return x

newF' :: Function -> EvalMonad s (STermRef s)
newF' = newSTRef' . from

newInt' :: Integer -> EvalMonad s (STermRef s)
newInt' = newSTRef' . from

newTrue' :: EvalMonad s (STermRef s)
newTrue' = newF' TF

newConst' :: EvalMonad s (STermRef s)
newConst' = newTrue'

newFalse' ::  EvalMonad s (STermRef s)
newFalse' = newSTRef' =<< ApST <$> newF' SF <*> newF' TF

newSTRef' :: a -> EvalMonad s (STRef s a)
newSTRef' = liftST . newSTRef

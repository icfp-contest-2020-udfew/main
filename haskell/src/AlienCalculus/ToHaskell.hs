-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.ToHaskell
  ( EmissionMode
  , assignmentsToHs
  , assignmentsToHs'
  , termToHs
  , termToHs'
  , funToHs
  )
where

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Parser

import           Control.Arrow
import           Data.Either
import           Data.List


type VarPrefix = String
type EmissionMode = Bool

nilCheckHeader :: String
nilCheckHeader =
  "class NilCheck a where isNil :: a -> Bool\n"
    ++ "instance NilCheck () where isNil () = True\n"
    ++ "instance NilCheck (a, b) where isNil = const False\n"

assignmentsToHs :: EmissionMode -> [Assignment] -> String
assignmentsToHs = assignmentsToHs' "var"

assignmentsToHs' :: VarPrefix -> EmissionMode -> [Assignment] -> String
assignmentsToHs' pre emit assignments =
  let namedPairs = rights assignments
      varPairs   = first ((pre ++) . show) <$> lefts assignments
      render (s, t) = s ++ " = " ++ termToHs' pre emit t
      renderAll = intercalate "\n" . map render
  in  (if emit then nilCheckHeader else "")
        ++ renderAll (namedPairs ++ varPairs)

termToHs :: EmissionMode -> Term -> String
termToHs = termToHs' "var"

termToHs' :: VarPrefix -> EmissionMode -> Term -> String
termToHs' _ _    (LitT (IntL i)) = paranthesis $ show i ++ ":: Integer"
termToHs' _ emit (LitT (FunL f)) = paranthesis $ funToHs emit f
termToHs' pre emit (ApT t s) =
  paranthesis $ termToHs' pre emit t ++ " " ++ termToHs' pre emit s
termToHs' pre _ (VarT i) = paranthesis $ pre ++ show i

-- Note that this _does not_ necessarily put paranthesis around the result.
funToHs :: EmissionMode -> Function -> String
funToHs _ AddF = "(+)"
funToHs _ BF   = "\\ f g x -> f (g x)" -- b x0 x1 x2 = ap x0 ap x1 x2
funToHs _ CF   = "\\ f x y -> (f y) x" -- c x0 x1 x2 = ap ap x0 x2 x1
funToHs emit CarF | emit      = "fst"
                  | otherwise = "\\ f -> f " ++ boolToHs True
funToHs emit CdrF | emit      = "snd"
                  | otherwise = "\\ f -> f " ++ boolToHs False
funToHs emit ConsF | emit      = "\\ x y -> (x, y)"
                   | otherwise = "\\ x y f -> (f x) y"
                                 -- cons x0 x1 x2 = ap ap x2 x0 x1
funToHs _ DivF = "\\ x y -> signum x * signum y * (abs x `div` abs y)"
funToHs emit EqF
  | emit
  = "(==)"
  | otherwise
  = "\\ n m -> if n == m then " ++ boolToHs True ++ " else " ++ boolToHs False
funToHs _ IF = "id"
funToHs emit IsNilF
  | emit      = "isNil"
  | otherwise = "\\ x-> x (const (const " ++ boolToHs False ++ "))"
funToHs emit LTF
  | emit
  = "(<)"
  | otherwise
  = "\\ n m -> if n < m then " ++ boolToHs True ++ " else " ++ boolToHs False
funToHs _ MulF = "(*)"
funToHs _ NegF = "negate"
funToHs emit NilF | emit      = "()"
                  | otherwise = "const " ++ boolToHs True
funToHs _ SF = "\\ f g x -> (f x) (g x)" -- s x0 x1 x2 = ap ap x0 x2 ap x1 x2
funToHs emit TF | emit      = "True"
                | otherwise = boolToHs True -- t x0 x1 = x0

boolToHs :: Bool -> String
boolToHs True  = "(const)"
boolToHs False = "(flip const)"

paranthesis :: String -> String
paranthesis s = "(" ++ s ++ ")"

-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Sprites where

import           Codec.Picture
import           Data.Bifunctor
import           Data.List

import qualified Data.Set                      as S

type PixelSize = Int
type SpriteSize = Int
type SpriteData = Integer
type Sprite = (Int, Integer)


renderSetASCII :: S.Set (Int, Int) -> (Int, Int) -> String
renderSetASCII set (xSize, ySize) = intercalate
  "\n"
  [ [ if (x, y) `S.member` set then '#' else ' ' | x <- [0 .. (xSize - 1)] ]
  | y <- [0 .. (ySize - 1)]
  ]

renderASCII :: PixelSize -> Sprite -> String
renderASCII ps sp@(s, _) = renderSetASCII set (imS, imS)
 where
  set = pixels ps sp
  imS = s * ps

black :: Pixel8
black = 0

white :: Pixel8
white = 255

renderSetJP :: S.Set (Int, Int) -> (Int, Int) -> Image Pixel8
renderSetJP set (xSize, ySize) = generateImage
  (\x y -> if (x, y) `S.member` set then white else black)
  xSize
  ySize

renderJP :: PixelSize -> Sprite -> Image Pixel8
renderJP ps sp@(s, _) = renderSetJP set (imS, imS)
 where
  set = pixels ps sp
  imS = s * ps

pixels :: PixelSize -> Sprite -> S.Set (Int, Int)
pixels ps = S.fromList . concatMap (pixelBlock ps) . rawPixels

rawPixels :: Sprite -> [(Int, Int)]
rawPixels (siz, dat) = rawPixels' dat (0, 0)
 where
  rawPixels' d (x, y) = if (x >= siz) || (y >= siz)
    then []
    else
      let newD     = d `div` 2
          bit      = (d `mod` 2) == 1
          nextLine = x + 1 >= siz
          newX     = if nextLine then 0 else x + 1
          newY     = if nextLine then y + 1 else y
      in  [ (x, y) | bit ] ++ rawPixels' newD (newX, newY)

pixelBlock :: PixelSize -> (Int, Int) -> [(Int, Int)]
pixelBlock ps (x, y) =
  [ (x * ps + x', y * ps + y') | x' <- [0 .. (ps - 1)], y' <- [0 .. (ps - 1)] ]

innerSprite :: Integer -> Sprite
innerSprite i = (frameSizeFor i, i)

renderNumberASCII :: PixelSize -> Integer -> String
renderNumberASCII ps = uncurry renderSetASCII . numberData ps

renderNumberJP :: PixelSize -> Integer -> Image Pixel8
renderNumberJP ps = uncurry renderSetJP . numberData ps

numberData :: PixelSize -> Integer -> (S.Set (Int, Int), (Int, Int))
numberData ps i =
  let
    isNeg     = i < 0
    rawI      = abs i
    innerSize = frameSizeFor rawI
    size      = innerSize + if isNeg then 2 else 1
    innerData =
      S.map (\(x, y) -> (x + ps, y + ps)) $ pixels ps (innerSize, rawI)
    outerData =
      S.fromList
        $  [ (x, y)
           | x <- [ps .. (ps * (innerSize + 1) - 1)]
           , y <- [0 .. (ps - 1)]
           ]
        ++ [ (x, y) | x <- [0 .. (ps - 1)], y <- [ps .. (ps * size - 1)] ]
    imSize = ps * size
  in
    (innerData `S.union` outerData, (imSize, imSize))

innerNumberData :: PixelSize -> Integer -> (S.Set (Int, Int), (Int, Int))
innerNumberData ps i = (pixels fs (innerSprite i), (size, size))
  where fs = frameSizeFor i
        size = ps * fs

log2 :: Integer -> Int
log2 = log2' 0
 where
  log2' n 0 = n
  log2' n 1 = n
  log2' n i = log2' (n + 1) (i `div` 2)

iSqrt :: Int -> Int
iSqrt = iSqrt' 0 where iSqrt' n k = if k <= n * n then n else iSqrt' (n + 1) k

frameSizeFor :: Integer -> Int
frameSizeFor = iSqrt . (+ 1) . log2 -- Magic!

shift :: (Int, Int) -> S.Set (Int, Int) -> S.Set (Int, Int)
shift (x, y) = S.map (bimap (+ x) (+ y))

concatenateBy :: (Int, Int) -> [S.Set (Int, Int)] -> S.Set (Int, Int)
concatenateBy (x, y) =
  S.unions . zipWith (\i s -> shift (i * x, i * y) s) [0 ..]

renderNumbersASCII :: Bool -> PixelSize -> [Integer] -> String
renderNumbersASCII dr ps = uncurry renderSetASCII . concatInners dr ps

renderNumbersJP :: Bool -> PixelSize -> [Integer] -> Image Pixel8
renderNumbersJP dr ps = uncurry renderSetJP . concatInners dr ps

concatInners :: Bool -> PixelSize -> [Integer] -> (S.Set (Int, Int), (Int, Int))
concatInners _ _ [] = (S.empty, (0, 0))
concatInners direction ps is =
  let
    (sets  , sizes ) = unzip . map (innerNumberData ps) $ is
    (xSizes, ySizes) = unzip sizes
    singleSize       = if direction then maximum xSizes else maximum ySizes
    size             = singleSize * length sets
    diff             = if direction then (singleSize, 0) else (0, singleSize)
    set              = concatenateBy diff sets
    (xSize, ySize) =
      if direction then (size, singleSize) else (singleSize, size)
  in
    (set, (xSize, ySize))

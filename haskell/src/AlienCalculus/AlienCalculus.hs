-- Copyright 2020 Aras Ergus, Gustavo Jasso, Mark Pedron, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# Language MultiParamTypeClasses #-}
{-# Language FlexibleContexts      #-}

module AlienCalculus.AlienCalculus where

import Basement.From

data Function =
    NegF -- hope : int -> int
  | AddF -- hope : int -> int -> int
  | DivF -- hope : int -> int -> int
  | MulF -- hope : int -> int -> int, tound towards zero
  | BF -- concat
  | CF -- flip
  | CarF -- (first el of pair) hope : pair -> thing
  | CdrF -- (second el of pair) hope : pair -> thing
  | ConsF -- pair constructor
  | EqF -- eq hope : thing -> thing -> bool
  | IF -- identity
  | IsNilF -- hope: pair | nil -> bool
  | LTF -- strict less than hope : int -> int -> bool
  | NilF -- nil
  | SF -- s x y z = x z (y z)
  | TF -- true
  deriving (Show, Eq)

data Literal =
    IntL Integer
  | FunL Function
--  | CanvasL ?
--  | BinaryL [Bool]
  deriving (Show, Eq)

data Term =
    LitT Literal
  | ApT Term Term
  | VarT Int
  deriving (Show, Eq)
--  | ListT [Term]


-- Please excuse this Hack...
instance Num Term where
  t + s = ApT (ApT (from AddF) t) s
  t * s = ApT (ApT (from MulF) t) s
  abs t = t
  signum _ = LitT (IntL 1)
  fromInteger = from
  negate t = ApT (from NegF) t

-- Putting these to another module was considered, but that causes orphan
-- instances...
instance From Function Term where
  from = LitT . FunL

instance From Integer Term where
  from = LitT . IntL

instance From Bool Term where
  from True  = from TF
  from False = ApT (from SF) (from TF)

instance From a Term => From [a] Term where
  from []       = from NilF
  from (x : xs) = ApT (ApT (from ConsF) (from x)) (from xs)

instance (From a Term, From b Term) => From (a, b) Term where
  from (x, y) = ApT (ApT (from ConsF) (from x)) (from y)

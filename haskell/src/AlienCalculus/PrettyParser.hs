-- Copyright 2020 Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.PrettyParser where

import Control.Applicative ((<|>))
import qualified Data.Attoparsec.Text as AP
import Data.Text (pack)


import AlienCalculus.Parser
import AlienCalculus.Sugared







boolParser :: AP.Parser SugaredTerm
boolParser = matchString "True" (BoolST False)
             <|>
             matchString "False" (BoolST False)

listParser :: AP.Parser SugaredTerm
listParser = ListST <$> AP.sepBy sugaredTermParser (AP.string $ pack ", ") 

pairParser :: AP.Parser SugaredTerm
pairParser = PairST <$> (AP.char '(' *> sugaredTermParser) <*>
  (AP.string (pack ". ") *> sugaredTermParser <* AP.char ')') 
    
varSTParser :: AP.Parser Int
varSTParser = AP.char 'v' *> AP.decimal
                           
apSTParser :: AP.Parser SugaredTerm
apSTParser = ApST <$> (AP.char '(' *> sugaredTermParser) <*>
  (AP.skipSpace *> AP.sepBy sugaredTermParser AP.space <* AP.char ')') 

freeVarSTParser :: AP.Parser Int
freeVarSTParser =  AP.char 'x' *> AP.decimal

lambdaParser :: AP.Parser SugaredTerm
lambdaParser = LamST
               <$>
               (AP.string (pack "(\\")
                *> AP.sepBy freeVarSTParser AP.space
                <* AP.string (pack " -> ")
               ) 
               <*> 
               (sugaredTermParser
                <*
                AP.char ')'
               )

sugaredTermParser :: AP.Parser SugaredTerm
sugaredTermParser = AP.choice
  [ boolParser
  , listParser
  , pairParser
  , VarST <$> varSTParser
  , apSTParser
  , FreeVarST <$> freeVarSTParser
  , lambdaParser]



prettyParseST :: String -> Maybe SugaredTerm
prettyParseST = extractRight
                . AP.parseOnly (sugaredTermParser <* AP.endOfInput)
                . pack

-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.ToJavaScript
  ( assignmentsToJS
  , assignmentsToJS'
  , termToJS
  , termToJS'
  , funToJS
  )
where

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Parser

import           Data.Either
import           Data.List
import           Data.Char


type ContextName = String

assignmentsToJS :: [Assignment] -> String
assignmentsToJS = assignmentsToJS' "context"

assignmentsToJS' :: ContextName -> [Assignment] -> String
assignmentsToJS' pre assignments =
  let renderContextVar (i, t) = "    " ++ show i ++ " : " ++ termToJS' pre t
      contextVars =
          intercalate ",\n" . map renderContextVar . lefts $ assignments
      varContext =
        "var " ++ pre ++ " = function(n) {\n"
          ++ "  return {\n"
          ++ contextVars
          ++ "\n  }[n];\n"
          ++ "\n};" 
      renderNamedVar (s, t) = "var " ++ s ++ " = " ++ termToJS' pre t ++ ";"
      namedVars = intercalate "\n" . map renderNamedVar . rights $ assignments
  in  varContext ++ "\n\n" ++ namedVars

termToJS :: Term -> String
termToJS = termToJS' "context"

termToJS' :: ContextName -> Term -> String
termToJS' _   (LitT (IntL i)) = show i
termToJS' _   (LitT (FunL f)) = funToJS f
termToJS' pre (ApT t s      ) = termToJS' pre t ++ paranthesis (termToJS' pre s)
termToJS' pre (VarT i       ) = pre ++ "(" ++ show i ++ ")"

-- The definitions of these symbols are in javascript/prelude.js.
funToJS :: Function -> String
funToJS f = toLower (head capName) : tail capName where capName = show f

paranthesis :: String -> String
paranthesis s = "(" ++ s ++ ")"

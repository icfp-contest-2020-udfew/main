-- Copyright 2020 Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Utilities where

divToZero :: Integer -> Integer -> Integer
divToZero x y = signum x * signum y * (abs x `div` abs y)

(.:) :: (a -> b) -> (c -> d -> a) -> c -> d -> b
(.:) f g x y = f $ g x y

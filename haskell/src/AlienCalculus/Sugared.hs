-- Copyright 2020 Achim Krause, Aras Ergus, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Sugared where

import AlienCalculus.AlienCalculus

data SugaredTerm =
    BoolST Bool
  | ListST [SugaredTerm]
  | PairST SugaredTerm SugaredTerm
  | RawLitST Literal
  | VarST Int
  | ApST SugaredTerm [SugaredTerm]
  | FreeVarST Int
  | LamST [Int] SugaredTerm
  deriving (Eq, Show)


sugar :: Term -> SugaredTerm
sugar (LitT (FunL TF)) = BoolST True
sugar (ApT (LitT (FunL SF)) (LitT (FunL TF))) = BoolST False
sugar (ApT (ApT (LitT (FunL ConsF)) t) s) = case sugar s of
  ListST l -> ListST $ sugar t : l
  s'       -> PairST (sugar t) s'
sugar (LitT (FunL NilF)) = ListST []
sugar (LitT l          ) = RawLitST l
sugar (ApT t s         ) = case sugar t of
                             ApST f args -> ApST f (args ++ [sugar s])
                             f           -> ApST f [sugar s]
sugar (VarT i          ) = VarST i

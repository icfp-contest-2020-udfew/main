-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.Values where

import           AlienCalculus.AlienCalculus

data Value =
    IntV Integer
  | BoolV Bool
  | ListV [Value]
  | PairV Value Value
  deriving Eq

instance Show Value where
  show (IntV  i  ) = show i
  show (BoolV b  ) = show b
  show (PairV v w) = show (v, w)
  show (ListV vs ) = show vs

valuesIn :: Term -> [Value]
valuesIn (LitT (IntL i )                       ) = [IntV i]
valuesIn (LitT (FunL TF)                       ) = [BoolV True]
valuesIn (ApT (LitT (FunL SF)) (LitT (FunL TF))) = [BoolV False]
valuesIn (LitT (FunL NilF)                     ) = [ListV []]
valuesIn (ApT (ApT (LitT (FunL ConsF)) t) t'   ) = case valuesIn t of
  [v] -> case valuesIn t' of
    [ListV ws] -> [ListV (v : ws)]
    [w       ] -> [PairV v w]
    ws         -> v : ws
  vs -> vs ++ valuesIn t'
valuesIn (ApT t t') = valuesIn t ++ valuesIn t'
valuesIn _          = []

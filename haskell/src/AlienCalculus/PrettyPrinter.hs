-- Copyright 2020 Gustavo Jasso, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module AlienCalculus.PrettyPrinter where

import Data.Maybe (fromJust)

import AlienCalculus.AlienCalculus
import AlienCalculus.Sugared
import AlienCalculus.Parser


prettyPrintST :: SugaredTerm -> String
prettyPrintST (BoolST x) = show x
prettyPrintST (VarST x) = "v" ++ show x
prettyPrintST (FreeVarST x) = "x" ++ show x
prettyPrintST (PairST x y) = "(" ++ prettyPrintST x ++ ", "
                             ++ prettyPrintST y ++ ")"
prettyPrintST (ListST xs) = "[" ++ f xs ++ "]"
  where
    f :: [SugaredTerm] -> String
    f [] = ""
    f [x] = prettyPrintST x
    f (x:xs') = prettyPrintST x ++ ", " ++ f xs'
prettyPrintST (ApST x []) = "(" ++ prettyPrintST x ++ ")"
prettyPrintST (ApST x ys) = "(" ++ prettyPrintST x ++ " "
                            ++ unwords (map prettyPrintST ys) ++ ")"
prettyPrintST (LamST xs y) = "(\\" ++ f xs ++ " -> " ++ prettyPrintST y ++ ")"
  where
    f [] = ""
    f [x] = "x" ++ show x
    f (x:xs') = "x" ++ show x ++ " " ++ f xs'
prettyPrintST (RawLitST (IntL x)) = show x
prettyPrintST (RawLitST (FunL x)) = f x
  where
    f :: Function -> String
    f AddF = "add"
    f BF = "b"
    f CF = "c"
    f CarF = "car"
    f CdrF = "cdr"
    f ConsF = "cons"
    f DivF = "div"
    f EqF = "eq"
    f IF = "i"
    f IsNilF = "isnil"
    f LTF = "lt"
    f MulF = "mul"
    f NegF = "neg"
    f NilF = "nil"
    f SF = "s"
    f TF = "t"

prettyPrintAssignment :: Assignment -> String
prettyPrintAssignment (Left (n, term)) = prettyPrintST (VarST n) ++ " = "
                                       ++ prettyPrintST (sugar term)
prettyPrintAssignment (Right (str, term)) = str ++ " = "
                                       ++ prettyPrintST (sugar term)

prettyPrintAssignmentFile :: String -> String -> IO ()
prettyPrintAssignmentFile inp outp =
  do assignments <- fromJust <$> parseAssignmentsFile inp
     let str = unlines $ fmap prettyPrintAssignment assignments
     writeFile outp str

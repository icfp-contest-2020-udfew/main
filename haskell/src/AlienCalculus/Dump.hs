-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE OverloadedStrings #-}

module AlienCalculus.Dump where

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Evaluation

import           Data.Char
import           Data.String

import qualified Data.IntMap                   as IM

dumpContext :: (IsString s, Monoid s) => VarContext -> s
dumpContext = mconcat . map ((<> "\n") . dump') . IM.toList
  where dump' (i, t) = ":" <> showS i <> " = " <> dumpTerm t

dumpTerm :: (IsString s, Monoid s) => Term -> s
dumpTerm (LitT (IntL i)) = showS i
dumpTerm (LitT (FunL f)) = fromString . map toLower . init . show $ f
dumpTerm (ApT t s      ) = "ap " <> dumpTerm t <> " " <> dumpTerm s
dumpTerm (VarT i       ) = ":" <> showS i

showS :: (Show a, IsString s) => a -> s
showS = fromString . show

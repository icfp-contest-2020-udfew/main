-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.


-- Beware, there be dragons.
-- Intended for interactive use.

{-# LANGUAGE TupleSections #-}

module AlienCalculus.AnalysisTools where

import           AlienCalculus.AlienCalculus
import           AlienCalculus.Evaluation
import           AlienCalculus.Inliner
import           AlienCalculus.Parser
import           AlienCalculus.PrettyPrinter
import           AlienCalculus.Sprites
import           AlienCalculus.Sugared
import           AlienCalculus.Values

import           Control.Monad.State
import           Data.Either
import           Data.Maybe
import           Data.Monoid

import qualified Data.Map                      as M
import qualified Data.IntMap                   as IM
import qualified Data.IntSet                   as IS

readContexts
  :: String
  -> IO
       ( VarContext
       , M.Map VarID Term
       , M.Map VarID [VarID]
       , M.Map VarID [VarID]
       , [(VarID, Value)]
       )
readContexts fileName = do
  assignments <- lefts . fromJust <$> parseAssignmentsFile fileName
  let ctxIM      = IM.fromList assignments
      ctxM       = M.fromList assignments
      ctxCallers = M.fromList $ evalState
        (zip (IM.keys ctxIM) <$> mapM callers (IM.keys ctxIM))
        ctxIM
      ctxCallees = M.map (IS.toList . callees) ctxM
      ctxValues  = concatMap (\(i, t) -> map (i, ) . valuesIn $ t) assignments
  return (ctxIM, ctxM, ctxCallers, ctxCallees, ctxValues)

ppT :: Term -> String
ppT = prettyPrintST . sugar

ppV :: VarContext -> VarID -> String
ppV ctx = prettyPrintST . sugar . (ctx IM.!)

dfs :: IM.IntMap [Int] -> Int -> IS.IntSet
dfs adjacencyLists node = dfs' adjacencyLists [node] IS.empty
 where
  dfs' _   []       visitedNodes = visitedNodes
  dfs' als (n : ns) visitedNodes = if n `IS.member` visitedNodes
    then dfs' als ns visitedNodes
    else dfs' als (als IM.! n ++ ns) (n `IS.insert` visitedNodes)

filterAny :: (a -> Any) -> [a] -> [a]
filterAny f = filter (getAny . f)

filterAll :: (a -> All) -> [a] -> [a]
filterAll f = filter (getAll . f)

getInt :: Value -> Maybe Integer
getInt (IntV i) = Just i
getInt _        = Nothing

canFrame :: Integer -> Integer -> Bool
canFrame siz dat = (siz > 0) && check (siz * siz) dat
   where check 0 0 = True
         check 0 _ = False
         check _ 0 = True
         check e n = check (e - 1) (n `div` 2)

getSprite :: [Value] -> [Sprite]
getSprite [IntV s, IntV d] = [ (fromInteger s, d) | s `canFrame` d ]
getSprite _                = []

isSprite :: [Value] -> Bool
isSprite [IntV s, IntV d] = s `canFrame` d
isSprite _                = False

-- One hypothesis was that one could possibly "overlay" sprite data on top of
-- each other by appending them to the list representing the sprite, but that
-- looks unlikely in hindsight.
getFancySprite :: [Value] -> [(Int, [Integer])]
getFancySprite (IntV s : vs) = case dat of
  Just is -> [ (fromInteger s, is) | not (null is), all (s `canFrame`) is ]
  Nothing -> []
 where
  dat = foldr
    (\v mL -> do
      l <- mL
      i <- getInt v
      return $ i : l
    )
    (Just [])
    vs
getFancySprite _ = []

isFancySprite :: [Value] -> Bool
isFancySprite = not . null . getFancySprite

lengthHasProp :: (Int -> Bool) -> [Value] -> Bool
lengthHasProp p = p . length

hasLength :: Int -> [Value] -> Bool
hasLength n = lengthHasProp (n ==)

liftICond :: Monoid m => (Integer -> m) -> Value -> m
liftICond c (IntV  i  ) = c i
liftICond _ (BoolV _  ) = mempty
liftICond c (PairV v w) = liftICond c v <> liftICond c w
liftICond c (ListV vs ) = mconcat (liftICond c <$> vs)

liftBCond :: Monoid m => (Bool -> m) -> Value -> m
liftBCond _ (IntV  _  ) = mempty
liftBCond c (BoolV x  ) = c x
liftBCond c (PairV v w) = liftBCond c v <> liftBCond c w
liftBCond c (ListV vs ) = mconcat (liftBCond c <$> vs)

liftPCond :: Monoid m => (Value -> Value -> m) -> Value -> m
liftPCond _ (IntV  _  ) = mempty
liftPCond _ (BoolV _  ) = mempty
liftPCond c (PairV v w) = c v w <> liftPCond c v <> liftPCond c w
liftPCond c (ListV vs ) = mconcat (liftPCond c <$> vs)

liftLCond :: Monoid m => ([Value] -> m) -> Value -> m
liftLCond _ (IntV  _  ) = mempty
liftLCond _ (BoolV _  ) = mempty
liftLCond c (PairV v w) = liftLCond c v <> liftLCond c w
liftLCond c (ListV vs ) = c vs <> mconcat (liftLCond c <$> vs)

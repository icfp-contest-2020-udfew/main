-- Copyright 2020 Bernhard Reinke, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE FlexibleContexts #-}

module AlienCalculus.SyntacticSugar where

import AlienCalculus.AlienCalculus

import Basement.From


trueF :: Term
trueF = from TF

falseF :: Term
falseF = ApT (LitT $ FunL SF) (LitT $ FunL TF)

constF :: Term
constF = trueF

fstF :: Term
fstF = from CarF

sndF :: Term
sndF = from CdrF

-- we don't define Fractional Term

eqF :: Term -> Term -> Term
eqF s = ApT (ApT (LitT $ FunL EqF) s)

lTF :: Term -> Term -> Term
lTF s = ApT (ApT (LitT $ FunL LTF) s)

divF :: Term -> Term -> Term
divF s = ApT (ApT (LitT $ FunL DivF) s)

boolToIntF :: Term -> Term
boolToIntF s = ApT (ApT s (LitT $ IntL 1)) (LitT $ IntL 0)

infixr 1 $$
($$) :: (From a Term, From b Term) => a -> b -> Term
f $$ t = ApT (from f) (from t)

intT :: Integer -> Term
intT = from

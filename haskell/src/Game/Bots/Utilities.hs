-- Copyright 2020 Robin Stoll, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Game.Bots.Utilities where

import Game.Types
import Game.Loop
import Kepler

type StatelessBot = GameResponse -> [Command]

type StarterBot = GameResponse -> ShipSpec

type BasicBot = (StarterBot, StatelessBot)

modulateSLBot :: StatelessBot -> PlayerKey -> String -> Maybe String
modulateSLBot bt pk = fmap (createCommandsRequest pk . bt) . parseGameResponse

modulateStBot :: StarterBot -> PlayerKey -> String -> Maybe String
modulateStBot sb pk = fmap (createStartRequest pk . sb) . parseGameResponse


constantStarterBot :: Int -> Int -> Int -> Int -> StarterBot
constantStarterBot fl fp co lf = const $ ShipSpec fl fp co lf


getShips :: GameResponse -> [Ship]
getShips = maybe [] (map fst . _shipsAndCommands) . _gameState


getOurRole :: GameResponse -> Role
getOurRole = _ourRole . _sgi

getOurShips :: GameResponse -> [Ship]
getOurShips g = filter ((getOurRole g ==) . _shipRole) $ getShips g

getEnemyShips :: GameResponse -> [Ship]
getEnemyShips g = filter ((getOurRole g /=) . _shipRole) $ getShips g

getShipX :: Ship -> Int
getShipX = fst . _shipPosition

getShipY :: Ship -> Int
getShipY = snd . _shipPosition

getShipPV :: Ship -> PosV
getShipPV ship = PosV {position = _shipPosition ship, velocity = _shipVelocity ship}

getNextShipPos :: Ship -> Position
getNextShipPos ship = position . update (0, 0) $ getShipPV ship

shipAngle :: Ship -> Ship -> Double
shipAngle ship ship' =
  atan2 (fromIntegral $ getShipY ship - getShipY ship') (fromIntegral $ getShipX ship - getShipX ship')

awayFromSun :: Position -> Vector
awayFromSun (x, y)
        | x < y && -x < y   = (0, -1)
        | x < y && -x >= y  = (1, 0)
        | x >= y && -x < y  = (-1, 0)
        | x >= y && -x >= y = (0, 1)
        | otherwise = undefined

shipDistance :: Ship -> Ship -> Int
shipDistance shipA shipB = f
                         (_shipPosition shipA)
                         (_shipPosition shipB)
  where f (a1, a2) (b1, b2) = max (abs $ a1 - b1) (abs $ a2 - b2)

updatedShipDistance :: Ship -> Ship -> Int
updatedShipDistance shipA shipB = max
                                  (abs $ a1 + av1 - b1 - bv1)
                                  (abs $ a2 + av2 - b2 - bv2)
  where
    (a1, a2) = _shipPosition shipA
    (b1, b2) = _shipPosition shipB
    (av1, av2) = _shipVelocity shipA
    (bv1, bv2) = _shipVelocity shipB

module Game.Bots.BoostStrat where

import Game.Types
import Game.Bots.OrbitBoostData
import Kepler

import Control.Arrow
import Data.Maybe

type OrbitStrategy = PosV -> Vec

runStrategy :: OrbitStrategy -> PosV -> [PosV]
runStrategy f = iterate (\ p -> update (f p) p)

surivalTime' :: Int -> [PosV] -> Int
surivalTime' cap = length . takeWhile (not . inSunPosV) . take cap

surivalTime :: [PosV] -> Int
surivalTime = surivalTime' 255

noBoostStrat :: OrbitStrategy
noBoostStrat = const (0, 0)

simpleStrat :: OrbitStrategy
simpleStrat pv@(PosV {position = pos, velocity = _}) =
  if surivalTime' 20 (runStrategy noBoostStrat pv) >= 20 then (0,0) else (negate *** negate) $ away $ gravity $ pos

simpleTickStrat :: GameTick -> OrbitStrategy
simpleTickStrat tick pv
  | odd tick = if surivalTime' 10 (runStrategy simpleStrat (update (0,0) pv)) >= 10 then (0, 0) else simpleStrat pv
  | even tick = simpleStrat pv

databaseStrat :: GameTick -> OrbitStrategy
databaseStrat tick pv =
  if tick <= 8
    then fromMaybe (0,0) $ listToMaybe =<< lookup pv orbitBoosts''
    else (0,0)

startPosVs :: [PosV]
startPosVs = map (\p -> PosV p (0,0)) $ concat [[(48,i),(-48,i),(i,48),(i,-48)] | i <- [-48..48]]

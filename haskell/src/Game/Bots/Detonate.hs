module Game.Bots.Detonate where

import Game.Types
import Game.Bots.Utilities

blastRadiusFromTot :: Int -> Int
blastRadiusFromTot n
  | n < 2 = 4
  | n < 15 = 5
  | n < 511 = 8
  | otherwise = 12

getBlastRadius :: Ship -> Int
getBlastRadius ship = blastRadiusFromTot . sum . map ($ _shipSpec ship)
                    $ [ _fuel , _firePower , _cooling , _life]

willBeInBlastRadiusOf :: Ship -> Ship -> Bool
willBeInBlastRadiusOf defer atker = updatedShipDistance atker defer
                                    <= getBlastRadius atker

areBlastableBy :: [Ship] -> [Ship] -> Bool
areBlastableBy defers atkers = all f defers
  where f :: Ship -> Bool
        f defer = any (defer `willBeInBlastRadiusOf`) atkers

detonateShip :: Ship -> Command
detonateShip ship = ( _shipId ship, Detonate)

detonateAllOpponents :: GameResponse -> [Command]
detonateAllOpponents gr = case getOurRole gr of
  Defender -> []
  Attacker -> if getEnemyShips gr `areBlastableBy` getOurShips gr
              then
                map detonateShip (getOurShips gr)
              else
                []
                 
addBasicDetonationModule :: StatelessBot -> StatelessBot
addBasicDetonationModule bot gr = detonateAllOpponents gr ++ bot gr

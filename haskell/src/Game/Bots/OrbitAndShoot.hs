-- Copyright 2020 Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE TupleSections #-}

module Game.Bots.OrbitAndShoot where

import Control.Arrow
import Data.Maybe

import Game.Types
import Game.Bots.Utilities
import Game.Bots.BoostStrat
import Game.Bots.Detonate
import Kepler


safeAt :: Int -> [a] -> Maybe a
safeAt 0 (x : _) = Just x
safeAt _ [] = Nothing
safeAt n _ | n < 0 = error "Negative list index"
safeAt n (_ : xs) = safeAt (n-1) xs

isShootingAngle :: Double -> Bool
isShootingAngle angle =
  (<= 0.25) . abs . (snd :: (Int, a) -> a) . properFraction $ angle * 2 / pi

maybeShootAt :: GameResponse -> Ship -> Ship -> Maybe RawCommand
maybeShootAt _ we enemy =
  if strength > 0
    then Just $ Shoot (getNextShipPos enemy) strength
    else Nothing
  where
    strength = min (_firePower $ _shipSpec we) $ 64 - _heat we

orbitAndShootShip :: GameResponse -> Ship -> Maybe RawCommand
orbitAndShootShip response we =
  case mBoost
    of Just boost -> Just $ Accelerate boost
       Nothing ->
        case role
          of Attacker -> mShoot
             Defender ->
               if tick >= 9 && tick `mod` 3 == 0 && _life spec >= 2
                 then Just $ Split (ShipSpec { _fuel = _fuel spec `div` 2
                                          , _firePower = _firePower spec `div` 2
                                          , _cooling = _cooling spec `div` 2
                                          , _life = _life spec `div` 2
                                          })
                 else mShoot
  where
    ourID = _shipId we
    spec = _shipSpec we
    role = _ourRole $ _sgi response
    tick = maybe 0 _gameTick (_gameState response)
    mBoost =
      case simpleTickStrat tick $ PosV {position = _shipPosition we, velocity = _shipVelocity we}
        of (0, 0) ->
             if any (\ ship -> getShipPV ship == getShipPV we && _shipId ship < ourID) $ getOurShips response
               then Just . (negate *** negate) . away . gravity $ _shipPosition we
               else Nothing
           dontDieBoost -> Just dontDieBoost
    smallestAngleEnemy = minimum . fmap (shipAngle we &&& id) $ getEnemyShips response
    mShoot =
      case smallestAngleEnemy
        of (angle, enemy) | isShootingAngle angle -> maybeShootAt response we enemy
           _ -> Nothing

orbitAndShootBot :: StatelessBot
orbitAndShootBot = addBasicDetonationModule orbitAndShootBot'

orbitAndShootBot' :: StatelessBot
orbitAndShootBot' response =
  mapMaybe mCmd $ getOurShips response
  where
    mCmd ship = (_shipId ship, ) <$> orbitAndShootShip response ship

orbitAndShootStartSpecs :: StarterBot
orbitAndShootStartSpecs response =
  if role == Attacker
    then ShipSpec 30 54 22 1
    else ShipSpec 208 0 4 96
  where role = _ourRole $ _sgi response

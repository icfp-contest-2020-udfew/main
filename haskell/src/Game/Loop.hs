-- Copyright 2020 Robin Stoll, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Game.Loop where


import Game.Types
import qualified Modulate as M

maybefyST :: Monad m => (M.SugaredTerm -> m a) -> M.SugaredTerm -> m (Maybe a)
maybefyST _ (M.SList []) = pure Nothing
maybefyST f x = Just <$> f x

gameResponseFromMSTerm :: M.SugaredTerm -> Maybe GameResponse
gameResponseFromMSTerm (M.SList [ M.SNum 0]) = Just Fail
gameResponseFromMSTerm (M.SList [ M.SNum 1
                                , M.SNum rawGameStage
                                , M.SList
                                  [ -- staticGameInfo
                                    M.SNum rawRoundLength -- u0
                                  , M.SNum rawSgiRole -- role
                                  , _ -- u2
                                  , _ -- u3
                                  , rawMaybeEnemyShipSpec
                                  ]
                                , rawMaybeGameState
                                ]
                       )
  = do
       gstg <- toGameStage rawGameStage
       sgir <- toRole rawSgiRole
       ess <- maybefyST readShipSpec rawMaybeEnemyShipSpec
       gs <- maybefyST readGameState rawMaybeGameState
       return $
         Success { _gameStage = gstg
                 , _sgi = StaticGameInfo { _roundLength = rawRoundLength
                                         , _ourRole = sgir
                                         , _u2 = Unknown
                                         , _u3 = Unknown
                                         , _enemySpec = ess
                                         }
                 , _gameState = gs
                 }
  where
    readGameState :: M.SugaredTerm -> Maybe GameState
    readGameState (M.SList [ M.SNum rawGameTick
                           , _ -- u1
                           , M.SList rawShipsAndCommands
                           ]
                  ) = do snc <- mapM f rawShipsAndCommands
                         return $
                           GameState { _gameTick = rawGameTick
                                     , _u1 = Unknown
                                     , _shipsAndCommands = snc
                                     }
    readGameState _ = Nothing
    readShipSpec :: M.SugaredTerm -> Maybe ShipSpec
    readShipSpec (M.SList [ M.SNum rawFuel
                          , M.SNum rawFirePower
                          , M.SNum rawCooling
                          , M.SNum rawLife
                          ]
                 ) = Just $ ShipSpec { _fuel = rawFuel
                                     , _firePower = rawFirePower
                                     , _cooling = rawCooling
                                     , _life = rawLife
                                     }
    readShipSpec _ = Nothing
    f :: M.SugaredTerm -> Maybe (Ship, [AppliedCommand])
    f (M.SList [ M.SList
                 [ --ship
                   M.SNum rawShipRole
                 , M.SNum rawShipId
                 , M.SCons (M.SNum posX) (M.SNum posY)
                 , M.SCons (M.SNum velX) (M.SNum velY)
                 , rawShipSpec
                 , M.SNum rawHeat -- u5
                 , M.SNum rawu6 -- u6
                 , M.SNum rawu7 -- u7
                 ]
               , M.SList rawListOfCommands
               ]
      ) = do loc <- mapM g rawListOfCommands
             sr <- toRole rawShipRole
             ss <- readShipSpec rawShipSpec
             return
               (Ship { _shipRole = sr
                     , _shipId = rawShipId
                     , _shipPosition = (posX, posY)
                     , _shipVelocity = (velX, velY)
                     , _shipSpec = ss
                     , _heat = rawHeat
                     , _u6 = rawu6
                     , _u7 = rawu7
                     }
               , loc
               )
    f _ = Nothing
    g :: M.SugaredTerm -> Maybe AppliedCommand
    g _ = Just AC
gameResponseFromMSTerm _ = Nothing



parseGameResponse :: String -> Maybe GameResponse
parseGameResponse str = gameResponseFromMSTerm . M.sugar
                        =<< M.demodulateString str


commandsToMSTerm :: PlayerKey -> [Command] -> M.SugaredTerm
commandsToMSTerm pk cs = M.SList [ M.SNum 4
                                 , M.SNum pk
                                 , M.SList (map f cs)
                                 ]
  where f :: Command -> M.SugaredTerm
        f (si, Accelerate (x, y)) = M.SList [ M.SNum 0
                                            , M.SNum si
                                            , M.SCons (M.SNum x) (M.SNum y)
                                            ]
        f (si, Detonate) = M.SList [ M.SNum 1
                                   , M.SNum si
                                   ]
        f (si, Shoot (x, y) fp) = M.SList [ M.SNum 2
                                          , M.SNum si
                                          , M.SCons (M.SNum x) (M.SNum y)
                                          , M.SNum fp
                                          ]
        f (si, Split ss) = M.SList [ M.SNum 3
                                   , M.SNum si
                                   , shipSpecToMSTerm ss
                                   ]

shipSpecToMSTerm :: ShipSpec -> M.SugaredTerm
shipSpecToMSTerm ss = M.SList . map (M.SNum . ($ ss))
                      $ [ _fuel
                        , _firePower
                        , _cooling
                        , _life
                        ]


createCommandsRequest :: PlayerKey -> [Command] -> String --String of 01
createCommandsRequest pk = M.modulateToString . M.desugar . commandsToMSTerm pk


-- we still do not know what to put as a second argument
createJoinRequest :: PlayerKey -> [Unknown] -> String
createJoinRequest pk [] = M.modulateToString . M.desugar
                          $ M.SList [ M.SNum 2
                                    , M.SNum pk
                                    , M.SList []
                                    ]
createJoinRequest _ _ = undefined

createStartRequest :: PlayerKey -> ShipSpec -> String
createStartRequest pk ss
  = M.modulateToString . M.desugar
    $ M.SList [ M.SNum 3
              , M.SNum pk
              , shipSpecToMSTerm ss
              ]

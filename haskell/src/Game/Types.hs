-- Copyright 2020 Robin Stoll, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE TemplateHaskell #-}
module Game.Types where

import Control.Lens

type PlayerKey = Int

type Vector = (Int, Int)

data GameStage = NotStarted | Started | Ended deriving (Eq, Show)

fromGameStage :: GameStage -> Int
fromGameStage NotStarted = 0
fromGameStage Started = 1
fromGameStage Ended = 2

toGameStage :: Int -> Maybe GameStage
toGameStage 0 = Just NotStarted
toGameStage 1 = Just Started
toGameStage 2 = Just Ended
toGameStage _ = Nothing


data GameResponse = Fail | Success { _gameStage :: GameStage
                                   , _sgi :: StaticGameInfo
                                   , _gameState :: Maybe GameState} deriving (Eq, Show)

 -- so far 256; number of turns?
data Unknown = Unknown deriving (Eq, Show)

data StaticGameInfo = StaticGameInfo { _roundLength :: Int
                                     , _ourRole :: Role
                                     , _u2 :: Unknown
                                     , _u3 :: Unknown
                                     , _enemySpec :: Maybe ShipSpec
                                     } deriving (Eq, Show)

data Role = Attacker | Defender deriving (Eq, Show, Ord)

toRole :: Int -> Maybe Role
toRole 0 = Just Attacker
toRole 1 = Just Defender
toRole _ = Nothing

type GameTick = Int

data GameState = GameState { _gameTick :: GameTick
                           , _u1 :: Unknown
                           , _shipsAndCommands :: ShipsAndCommands}
  deriving (Eq, Show)

type ShipsAndCommands = [(Ship, [AppliedCommand])]

type ShipId = Int
type Position = Vector
type Velocity = Vector

data ShipSpec = ShipSpec { _fuel :: Int
                         , _firePower :: Int
                         , _cooling :: Int
                         , _life :: Int --should be >0
                         } deriving (Eq, Show, Ord)

data Ship = Ship { _shipRole :: Role
                 , _shipId :: ShipId
                 , _shipPosition :: Position
                 , _shipVelocity :: Velocity
                 , _shipSpec :: ShipSpec
                 , _heat :: Int
                 , _u6 :: Int
                 , _u7 :: Int} deriving (Eq, Show, Ord)

data RawCommand = Accelerate Vector
                | Detonate
                | Shoot Vector Int
                | Split ShipSpec
  deriving (Eq, Show)
type Command = (ShipId, RawCommand)

data AppliedCommand = AC deriving (Eq, Show)

makeLenses ''GameResponse
makeLenses ''StaticGameInfo
makeLenses ''GameState
makeLenses ''Ship
makeLenses ''ShipSpec






  -- [ 1 -- Fail/Success
  -- , 1 -- GameStage
  -- , [ -- StaticGameInfo
  --     256 -- u0
  --   , 0 -- Role
  --   , [512, 1, 64] -- u2
  --   , [16, 128] -- u3
  --   , [1, 1, 1, 3] -- u4
  --   ]
  -- , [ -- Game State
  --     0 -- GameTick
  --   , [16, 128] -- u1
  --   , [ -- ShipsAndCommands
  --       [  -- ShipAndCommands 1
  --         [ -- Ship1
  --           1 -- Role
  --         , 0 -- ShipId
  --         , (48, -18), (0, 0)
  --         , [1, 1, 1, 3] -- u4 = initial state?
  --         , 0, 64, 1]
  --       , [] -- Commands1
  --       ]
  --     , [ -- Ship 2
  --         [ -- Ship
  --           0, 1, (-48, 18), (0, 0), [0, 0, 1, 1], 0, 64, 1]
  --       , []
  --       ]
  --     ]
  --   ]
  -- ]

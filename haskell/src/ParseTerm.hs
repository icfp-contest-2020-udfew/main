-- Copyright 2020 Achim Krause

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module ParseTerm where

import Modulate
import Text.Parsec


runParseTerm :: String -> Either ParseError Term
runParseTerm = parse (parseTerm <* eof) ""


parseInt :: Parsec String () Int
parseInt = read <$> many1 digit
 
parseTerm :: Parsec String () Term
parseTerm = let --nil = Nil <$ string "[]"
                pair = Cons <$ char '('
                            <*> parseTerm
                            <* char ','
                            <*> parseTerm
                            <* char ')'
                num = Num <$> parseInt
                list = char '[' *> spaces *>
                  ((Nil <$ char ']')
                 <|> (Cons <$> parseTerm <*> listrest))
                listrest =  (Nil <$ char ']')
                        <|> (Cons <$ char ','
                                  <*> parseTerm
                                  <*> listrest)
                negnum = (Num . negate) <$ char '-'
                                        <*> parseInt
             in    spaces
                *> (pair <|> num <|> negnum <|> list)
                <* spaces




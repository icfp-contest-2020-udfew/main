-- Copyright 2020 Achim Krause, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Modulate where

import Data.List (foldl')

data Term = Num Int | Nil | Cons Term Term deriving (Show, Eq)

data Bit = O | I deriving (Show, Eq)

bitFromChar :: Char -> Maybe Bit
bitFromChar '0' = Just O
bitFromChar '1' = Just I
bitFromChar _ = Nothing

revBinary :: Int -> [Bit]
revBinary 0 = []
revBinary n = case n `divMod` 2 of
               (n',0) -> O : revBinary n'
               (n',_) -> I : revBinary n' -- only other possible value is 1

binary :: Int -> [Bit]
binary = reverse . revBinary

unbinary :: [Bit] -> Int
unbinary = foldl' (\x y -> 2*x + y) 0 . map d where
             d O = 0
             d I = 1

--left-pads bits to number divisible by 4, records number of blocks of size 4.
pad :: [Bit] -> (Int,[Bit])
pad bs = let l = length bs
             (n,off) = (-l) `divMod` 4
          in (-n, replicate off O ++ bs)

modulate :: Term -> [Bit]
modulate Nil = [O,O]
modulate (Cons t1 t2) = [I,I] ++ modulate t1 ++ modulate t2
modulate (Num n)
  | n >= 0 = let (blocks, bits) = pad (binary n)
              in [O,I] ++ replicate blocks I ++ [O] ++ bits 
  | otherwise = let (blocks, bits) = pad (binary (-n))
              in [I,O] ++ replicate blocks I ++ [O] ++ bits

modulateToString :: Term -> String
modulateToString = map s . modulate where
           s O = '0'
           s I = '1'


countToOHelper :: [Bit] -> Maybe (Int,[Bit])
countToOHelper [] = Nothing
countToOHelper (O:rs) = Just (0,rs)
countToOHelper (I:rs) = case countToOHelper rs of
                         Nothing -> Nothing
                         Just (n,rs') -> Just (n+1,rs')

splitMaybe :: Int -> [a] -> Maybe ([a],[a])
splitMaybe n xs | length xs < n = Nothing
                | otherwise = Just (splitAt n xs)

demodulatePart :: [Bit] -> Maybe (Term,[Bit])
demodulatePart []       = Nothing
demodulatePart [_]      = Nothing
demodulatePart (O:O:rs) = Just (Nil, rs)
demodulatePart (I:I:rs) = do (t1, rs') <- demodulatePart rs
                             (t2, rs'') <- demodulatePart rs'
                             return (Cons t1 t2, rs'')
demodulatePart (O:I:rs) = do (n,rs') <- countToOHelper rs
                             (bits, rs'') <- splitMaybe (4*n) rs' 
                             return (Num (unbinary bits), rs'')
demodulatePart (I:O:rs) = do (n,rs') <- countToOHelper rs
                             (bits, rs'') <- splitMaybe (4*n) rs'
                             return (Num (- (unbinary bits)), rs'')

demodulate :: [Bit] -> Maybe Term
demodulate bs = case demodulatePart bs of
                  Just (t,[]) -> Just t
                  _           -> Nothing

demodulateString :: String -> Maybe Term
demodulateString str = do bs <- mapM from01 str
                          demodulate bs
                  where
                    from01 '0' = Just O
                    from01 '1' = Just I
                    from01 _   = Nothing

data SugaredTerm =
  SNum Int | SCons SugaredTerm SugaredTerm | SList [SugaredTerm]
  deriving (Eq, Show)

sugar :: Term -> SugaredTerm
sugar (Num n) = SNum n
sugar Nil = SList []
sugar (Cons t s) = case sugar s of
                     SList xs -> SList (sugar t : xs)
                     _         -> SCons (sugar t) (sugar s)

desugar :: SugaredTerm -> Term
desugar (SNum n) = Num n
desugar (SList []) = Nil
desugar (SList (x:xs)) = Cons (desugar x) (desugar $ SList xs)
desugar (SCons a b) = Cons (desugar a) (desugar b)

-- Copyright 2020 Achim Krause, Bernhard Reinke, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Kepler where


type Vec = (Int,Int)

data PosV = PosV {position :: Vec, velocity :: Vec} deriving (Show,Eq)


addVec :: Vec -> Vec -> Vec
addVec (x1,y1) (x2,y2) = (x1+x2, y1+y2)


gravity :: Vec -> Vec
gravity (x,y)
  | x == y    = if x>0
                   then (-1,-1)
                   else (1,1)
  | x == -y   = if x>0
                   then (-1,1)
                   else (1,-1)
  | x > y && x > -y = (-1,0)
  | x > y && x < -y = (0,1)
  | x < y && x > -y = (0,-1)
  | otherwise {- x < y & x < -y -} = (1,0)

ortho :: Vec -> Vec
ortho (x, y) = (-y, x)

away :: Vec -> Vec
away (1, 0) = (-1, 1)
away (1, 1) = (-1, 0)
away (0, 1) = (-1, -1)
away (-1, 1) = (0, -1)
away (-1, 0) = (1,  -1)
away (-1, -1) = (1, 0)
away (0, -1) = (1, 1)
away (1, -1) = (0, 1)
away _ = (0, 0)

update :: Vec -> PosV -> PosV
update (boostX, boostY) (PosV {position = pos, velocity = vel})
  = let a = addVec (-boostX, -boostY) (gravity pos)
        v = addVec vel a
        p = addVec pos v
     in PosV {position = p, velocity = v}

inSunVec :: Vec -> Bool
inSunVec (x,y) = abs x <= 20 && abs y <= 20

inSunPosV :: PosV -> Bool
inSunPosV = inSunVec . position

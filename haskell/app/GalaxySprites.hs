-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE TupleSections #-}

module GalaxySprites where

import           AlienCalculus.AnalysisTools
import           AlienCalculus.Sprites
import           AlienCalculus.Parser
import           AlienCalculus.Values

import           Codec.Picture
import           Control.Monad
import           Data.Either
import           Data.Foldable
import           Data.List
import           Options.Applicative
import           System.IO

import qualified Data.Set                      as S
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO

main :: IO ()
main = do
  GSO md ps <- execParser optionsInfo
  case md of
    Single sp -> writeSprite ps sp
    Stdin     -> writeSpritesIn ps =<< TIO.getContents
    File s    -> writeSpritesIn ps =<< TIO.readFile s

writeSpritesIn :: PixelSize -> T.Text -> IO ()
writeSpritesIn ps txt = case parseAssignments txt of
  Just assignments -> do
    hPutStrLn stderr "Parsed galaxy file, looking for sprites..."
    let ctxValues =
          concatMap
              (\(i, t) ->
                map (i, )
                  . nub
                  . concatMap (liftLCond getSprite)
                  . nub
                  . valuesIn
                  $ t
              )
            . lefts
            $ assignments
    void $ foldrM (uncurry (updateVarSprites ps)) S.empty ctxValues
  Nothing -> hPutStrLn stderr "Couldn't parse input file."

updateVarSprites
  :: PixelSize -> Int -> Sprite -> S.Set Sprite -> IO (S.Set Sprite)
updateVarSprites ps i sp written = do
  hPutStr stderr $ show sp ++ " in variable " ++ show i ++ ": "
  appendFile (prefixFor sp ++ ".vars") (show i ++ "\n")
  if sp `S.member` written
    then hPutStrLn stderr "Already rendered." >> return written
    else hPutStrLn stderr "Rendering..." >> writeSprite ps sp >> return
      (sp `S.insert` written)

writeSprite :: PixelSize -> Sprite -> IO ()
writeSprite ps sp = writePng fileName image
 where
  fileName = prefixFor sp ++ ".png"
  image    = renderJP ps sp

prefixFor :: Sprite -> String
prefixFor (s, d) = show s ++ "-" ++ show d

data InputMode = File String | Stdin | Single Sprite deriving (Eq, Show)
data GSOptions = GSO
  { mode      :: InputMode
  , pixelSize :: Int
  } deriving (Eq, Show)

handleParser :: Parser (Maybe String)
handleParser = optional $ strOption
  (long "file" <> short 'f' <> metavar "FILE" <> help
    "Path to the file to extract values from (read from stdin if not present)"
  )

spriteSize :: Parser Int
spriteSize = option auto (long "size" <> short 's' <> metavar "SPRITE_SIZE")

spriteData :: Parser Integer
spriteData = option auto (long "data" <> short 'd' <> metavar "SPRITE_DATA")

modeParser :: Parser InputMode
modeParser =
  (curry Single <$> spriteSize <*> spriteData)
    <|> (maybe Stdin File <$> handleParser)

pixelSizeParser :: Parser Int
pixelSizeParser = option
  auto
  (long "pixel-size" <> metavar "PIXEL_SIZE" <> showDefault <> value 10)

optionsParser :: Parser GSOptions
optionsParser = GSO <$> modeParser <*> pixelSizeParser


optionsInfo :: ParserInfo GSOptions
optionsInfo = info (optionsParser <**> helper)
                   (fullDesc <> progDesc "Render sprites in \"alien code\".")

-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module GalaxyValues where

import           AlienCalculus.Parser
import           AlienCalculus.Values

import           Data.Either
import           Options.Applicative
import           System.IO

import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO

main :: IO ()
main = do
  GVO mHandle pv <- execParser optionsInfo
  txt            <- maybe TIO.getContents TIO.readFile mHandle
  dumpValues txt pv

dumpValues :: T.Text -> Bool -> IO ()
dumpValues txt pv = case parseAssignments txt of
  Just assignments -> mapM_ putStrLn . concatMap process . lefts $ assignments
   where
    process (i, t) = if pv
      then map (((show i ++ ": ") ++) . show) $ valuesIn t
      else map show $ valuesIn t
  Nothing -> hPutStrLn stderr "Couldn't parse input file."

data GVOptions = GVO
  { handleGVO    :: Maybe String
  , printVarsGVO :: Bool
  }

handleParser :: Parser (Maybe String)
handleParser = optional $ strOption
  (long "file" <> short 'f' <> metavar "FILE" <> help
    "Path to the file to extract values from (read from stdin if not present)"
  )

printParser :: Parser Bool
printParser = switch
  (long "print-vars" <> short 'p' <> help
    "Whether to print the identifiers of the variables containing the values"
  )

optionsParser :: Parser GVOptions
optionsParser = GVO <$> handleParser <*> printParser

optionsInfo :: ParserInfo GVOptions
optionsInfo = info
  (optionsParser <**> helper)
  (fullDesc <> progDesc "Find hard coded values in \"alien code\".")

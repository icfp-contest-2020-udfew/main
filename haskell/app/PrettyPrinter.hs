-- Copyright 2020 Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module PrettyPrinter where

import System.Environment (getArgs)
import System.IO (hPutStrLn, stderr)
import System.Exit
import AlienCalculus.PrettyPrinter (prettyPrintAssignmentFile)

main :: IO ()
main = do args <- getArgs 
          case args of
            [inp, outp] -> prettyPrintAssignmentFile inp outp
            _           -> do hPutStrLn stderr
                                "syntax: pretty-print inputPath outputPath"
                              exitWith (ExitFailure 1)

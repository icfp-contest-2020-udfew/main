-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module InlineGalaxy where

import           AlienCalculus.Dump
import           AlienCalculus.Inliner
import           AlienCalculus.Parser

import           Data.List
import           Data.Either
import           System.Environment
import           System.IO

import qualified Data.IntMap                   as IM
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO

main :: IO ()
main = do
  args <- getArgs
  case args of
    [inliner] -> do
      txt <- TIO.getContents
      dumpInlinedText inliner txt
    [inliner, filePath] -> do
      txt <- TIO.readFile filePath
      dumpInlinedText inliner txt
    _ ->
      hPutStrLn stderr
        $  "Invalid arguments.\n"
        ++ "The first argument must be the inliner identifier "
        ++ "('s' for inlining sinks, 'c' for reducing cycles, "
        ++ "'b' for both).\n"
        ++ "If the second argument is present, it is used as the path "
        ++ "to the file to be compiled, otherwise code is read from stdin."

dumpInlinedText :: String -> T.Text -> IO ()
dumpInlinedText s txt = case parseAssignments txt of
  Just assignments -> maybe
    (putStrLn $ "Unrecognized inliner identifier: " ++ s)
    (runInliner assignments)
    (arg2Inliner s)
  Nothing -> hPutStrLn stderr "Couldn't parse input file."

runInliner :: [Assignment] -> Inliner Bool -> IO ()
runInliner assignments inliner = do
  putStrLn
    . dumpContext
    . inlineWith inliner
    . IM.fromList
    . lefts
    $ assignments
  let dumpNamed (n, t) = n ++ " = " ++ dumpTerm t
  putStrLn . intercalate "\n" . map dumpNamed . rights $ assignments

-- Please excuse the hack...
arg2Inliner :: String -> Maybe (Inliner Bool)
arg2Inliner "s" = Just inlineSinksM
arg2Inliner "c" = Just reduceCyclesM
arg2Inliner "b" = Just $ (||) <$> inlineSinksM <*> reduceCyclesM
arg2Inliner _   = Nothing

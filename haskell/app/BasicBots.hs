-- Copyright 2020 Achim Krause, Mark Pedron, Robin Stoll, Tashi Walde

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE TupleSections #-}

module BasicBots where

import Game.Bots.Basic
import Game.Bots.OrbitAndShoot
import Game.Bots.Utilities
import Game.Types
import Game.Loop

import System.Environment (getArgs)
import System.IO (hPutStrLn, stderr, stdout, hSetBuffering, BufferMode(NoBuffering))
import System.Exit
import Text.Read (readMaybe)



playSBot :: StatelessBot -> StarterBot -> PlayerKey -> IO ()
playSBot bt sb pk = do _ <- getLine
                       putStrLn $ createJoinRequest pk []
                       -- read initial game response
                       igr <- getLine
                       -- try to create start request
                       let mss = fmap (createStartRequest pk . sb)
                                 . parseGameResponse $ igr
                       case mss of
                         (Just ss) -> do putStrLn ss
                                         playSLBot bt pk
                         Nothing -> return ()


playSLBot :: StatelessBot -> PlayerKey -> IO ()
playSLBot bt pk = do
  gr <- getLine -- read game response
  -- create command request
  let mcs = fmap (createCommandsRequest pk . bt)
            . parseGameResponse $ gr
  case mcs of
    (Just cs) -> do putStrLn cs
                    playSLBot bt pk
    Nothing -> return ()

process :: [String] -> Maybe (String, Int)
process [bot, pk] = (bot, ) <$> readMaybe pk
process _ = Nothing

main :: IO ()
main = do args <- getArgs
          hSetBuffering stdout NoBuffering
          case process args of
            Just ("accelerate-bot", pk)
              -> playSBot
                   justAccelerateAwayBot
                   justAccelerateAwayStartSpecs
                   pk
            Just ("orbit-shoot-bot", pk)
              -> playSBot
                   orbitAndShootBot
                   orbitAndShootStartSpecs
                   pk
            _ -> do hPutStrLn stderr
                      "syntax: play-haskell-bot botname player-key args"
                    hPutStrLn stderr
                      "e.g. play-haskell-bot accelerate-bot player-key"
                    hPutStrLn stderr
                      "e.g. play-haskell-bot orbit-shoot-bot player-key"
                    exitWith (ExitFailure 1)

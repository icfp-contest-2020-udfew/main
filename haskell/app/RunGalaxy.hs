-- Copyright 2020 Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module RunGalaxy where

import AlienCalculus.Evaluation
import AlienCalculus.Parser
import AlienCalculus.SyntacticSugar
import AlienCalculus.Modulate

import qualified Modulate as Mod

import Data.Either
import System.Environment (getArgs)

import qualified Data.IntMap as IM


returnResult :: Mod.Term -> IO ()
returnResult modTerm =
  case modTerm
    of (Mod.Cons flag (Mod.Cons newState (Mod.Cons dat rest))) ->
         do putStrLn $ Mod.modulateToString flag
            putStrLn $ Mod.modulateToString newState
            putStrLn $ Mod.modulateToString dat
            putStrLn $ Mod.modulateToString rest
       _ -> error "Didn't return a list with at least three elements"

main :: IO ()
main =
  do [file, stateArg, vectorArg] <- getArgs
     mAss <- parseAssignmentsFile file
     case mAss
       of Nothing -> error "Error parsing file"
          Just ass ->
            case rights ass
              of [("galaxy", galaxy)] ->
                   let varC = IM.fromList $ lefts ass
                    in either error returnResult $
                         do state <- demod stateArg
                            vec <- demod vectorArg
                            res <- eval varC $ (galaxy $$ state) $$ vec
                            toModulateTerm varC res
                 _ -> error "Named variables are not exactly \"galaxy\""
  where
    demod = fmap fromModulateTerm . maybe (Left "Failed demodulating") Right . Mod.demodulateString

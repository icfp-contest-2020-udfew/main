-- Copyright 2020 Aras Ergus

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module ACToJS where

import           AlienCalculus.Parser
import           AlienCalculus.ToJavaScript

import           System.Environment
import           System.IO
import           Data.Text

import qualified Data.Text.IO                  as TIO

main :: IO ()
main = do
  args <- getArgs
  case args of
    [pre] -> readFile pre >>= putStrLn >> TIO.getContents >>= compileText
    [pre, fileName] ->
      readFile pre >>= putStrLn >> TIO.readFile fileName >>= compileText
    _ ->
      hPutStrLn stderr $
         "Invalid arguments.\n"
           ++ "The first argument should be the path to a JavaScript " 
           ++ "\"prelude\" file in which function literals are defined.\n"
           ++ "If the second argument is present, it is used as the path "
           ++ "to the file to be compiled, otherwise code is read from stdin."

compileText :: Text -> IO ()
compileText txt = case parseAssignments txt of
  Just assignments -> putStrLn $ assignmentsToJS assignments
  Nothing          -> hPrint stderr "Couldn't parse input file."

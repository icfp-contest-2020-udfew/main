-- Copyright 2020 Achim Krause, Mark Pedron, Robin Stoll

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Modem where

import Modulate
import ParseTerm (runParseTerm)
import System.Environment (getArgs)
import System.Exit
import System.IO (hPutStrLn, hPrint, stderr)

main :: IO ()
main = do args <- getArgs
          case args of
            ["-d", str] -> demodAct str
            [str]     -> modAct str
            _         -> do hPutStrLn stderr "syntax: modem [-d] string"
                            exitWith (ExitFailure 1)

modAct :: String -> IO ()
modAct str = case runParseTerm str of
             Left e -> do hPrint stderr e
                          exitWith (ExitFailure 1)
             Right t -> do putStrLn (modulateToString t)
                           exitSuccess

demodAct :: String -> IO ()
demodAct str = case demodulateString str of
                 Nothing -> do hPutStrLn stderr "syntax error in bitstring"
                               exitWith (ExitFailure 1)
                 Just term -> do putStrLn $ prettyprint term
                                 exitSuccess

prettyprint :: Term -> String
prettyprint (Num n) = show n
prettyprint Nil     = "[]"
prettyprint l@(Cons t1 t2)
  | isList t2 = "[" ++ prettyprintElems l ++ "]"
  | otherwise = "(" ++ prettyprint t1 ++ ", " ++ prettyprint t2 ++ ")"

prettyprintElems :: Term -> String
prettyprintElems Nil          = ""
prettyprintElems (Cons t1 Nil) = prettyprint t1
prettyprintElems (Cons t1 t2) = prettyprint t1 ++ ", " ++ prettyprintElems t2
prettyprintElems (Num _) = undefined

isList :: Term -> Bool
isList Nil = True
isList (Cons _ t2) = isList t2
isList _ = False

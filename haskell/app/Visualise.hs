-- Copyright 2020 Achim Krause

-- This file is part of icfp-contest-mmxx-udfew.
--
-- icfp-contest-mmxx-udfew is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- icfp-contest-mmxx-udfew is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with icfp-contest-mmxx-udfew.  If not, see <https://www.gnu.org/licenses/>.

module Visualise where

import Data.List (sort)
import Modulate (Term(..), demodulateString, modulateToString)
import System.Exit  
import System.Environment (getArgs)
import System.IO (hPutStr, hPutStrLn, stderr)
import qualified Data.Map as M
import qualified Data.Set as S

main :: IO ()
main = getArgs >>= act

failMsg :: String -> IO ()
failMsg str = hPutStrLn stderr str >> exitWith (ExitFailure 1)

act :: [String] -> IO ()
act [bits] = case demodulateString bits of
               Nothing -> failMsg "syntax error in bitstring"
               Just term -> case processTerm term of
                              Nothing -> failMsg $ "bitstring doesn't encode a list of lists of coordinates, instead " ++ show term 
                              Just xss -> mainLoop (S.fromList [1..length xss]) (M.fromList $ zip [1..length xss] xss)
act _ = failMsg "usage: visualise \"bitstring\""

mainLoop :: S.Set Int -> M.Map Int [(Int,Int)] -> IO ()
mainLoop active xss = do draw (getFrame xss) (M.toList $ M.restrictKeys xss active)
                         str <- getLine
                         actOnInput str active xss

actOnInput :: String -> S.Set Int -> M.Map Int [(Int,Int)] -> IO () 
actOnInput ":q" _ _ = putStrLn "11010010"
actOnInput (':':'t':' ':str) active xss = mainLoop (toggle (parseNumbers str) active) xss
actOnInput (':':'s':' ':str) active xss = case parseNumbers str of
                                            [x,y] -> putStrLn $ modulateToString (Cons (Num x) (Num y))
                                            _ -> usage active xss
actOnInput (':':'h':' ':str) active xss = case parseNumbers str of
                                            [x,y] -> mainLoop (S.insert 0 active) (highlight (x,y) xss)
                                            _ -> usage active xss
actOnInput _ active xss = usage active xss

highlight :: (Int,Int) -> M.Map Int [(Int,Int)] -> M.Map Int [(Int,Int)]
highlight coord = M.insert 0 [coord]

usage :: S.Set Int -> M.Map Int [(Int,Int)] -> IO ()
usage active xss = hPutStrLn stderr ("usage:\n :q to quit (sends (0,0))\n :t [numbers] to toggle channels, from 1 to " ++ show (length xss) ++ "\n :s [numbers] to send coordinates,\n :h [numbers] to highlight coordinates") >> mainLoop active xss   

toggle :: [Int] -> S.Set Int -> S.Set Int
toggle is flags = let indices = S.fromList (filter (>0) is)
                      diff1 = S.difference indices flags
                      diff2 = S.difference flags indices
                   in S.union diff1 diff2

parseNumbers :: String -> [Int]
parseNumbers str = [n | [(n,"")] <- map reads (words str)]

termAsList :: Term -> Maybe [Term]
termAsList (Num _) = Nothing
termAsList Nil = Just []
termAsList (Cons t1 t2) = (t1 :) <$> termAsList t2

termAsCoord :: Term -> Maybe (Int,Int)
termAsCoord (Cons (Num n) (Num m)) = Just (n,m)
termAsCoord _ = Nothing

processTerm :: Term -> Maybe [[(Int,Int)]] 
processTerm term = do ts <- termAsList term
                      tss <- mapM termAsList ts
                      mapM (mapM termAsCoord) tss

symbols :: [Char]
symbols = 'X' : ( concat . repeat $ "123456789abcdefghijklmnopqrstuvwxyz")

draw :: ((Int,Int),(Int,Int)) -> [(Int, [(Int,Int)])] -> IO ()
draw (xint, yint) coordss = let str = mergeframes [renderWithFrame (symbols !! n) xint yint xs | (n,xs) <- coordss]
                             in hPutStr stderr str

mergeframes :: [String] -> String
mergeframes [] = ""
mergeframes strs = foldr1 (zipWith f) strs where
  f ' ' c = c
  f c ' ' = c
  f 'X' _ = 'X'
  f _ 'X' = 'X'
  f c1 c2 | c1 == c2 = c1
          | otherwise = '#'


getFrame :: M.Map Int [(Int,Int)] -> ((Int,Int), (Int,Int))
getFrame coordss = let (xs,ys) = unzip $ concatMap snd . M.toList $ coordss
                    in ((minimum xs-1, maximum xs+1), (minimum ys-1, maximum ys+1))

groupRows :: [(Int,Int)] -> [(Int,[Int])]
groupRows coords = M.toList . M.map sort . M.fromListWith (++) $ [(y,[x]) | (x,y) <- coords]

renderWithFrame :: Char -> (Int,Int) -> (Int,Int) -> [(Int,Int)] -> String
renderWithFrame c (minX, maxX) (minY, maxY) dat
  = let rows = groupRows dat 
     in unlines [renderHelp (minX, maxX) ' ' (zip row (repeat c)) | row <- renderHelp (minY,maxY) [] rows] 

renderHelp :: (Int,Int) -> a -> [(Int,a)] -> [a]
renderHelp (minI, maxI) _ _ | minI > maxI = []
renderHelp (minI, maxI) def [] = replicate (maxI - minI) def
renderHelp (minI, maxI) def ((n,x):xs) 
  | n<minI  = renderHelp (minI,maxI) def xs
  | n==minI = x : renderHelp (minI+1,maxI) def xs
  | otherwise  = replicate (n-minI) def ++ renderHelp (n,maxI) def ((n,x):xs)

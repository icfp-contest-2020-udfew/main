default: all

all: build profile test lint docs benchmark

build: haskell-build python-build
profile: haskell-profile
test: haskell-test python-test
lint: haskell-lint python-lint
docs: haskell-docs
benchmark: haskell-benchmark

.PHONY: haskell-build haskell-profile haskell-test haskell-lint haskell-docs \
  haskell-benchmark hlint
haskell-build:
	cd haskell && stack build
haskell-profile:
	cd haskell && stack build --profile
haskell-test:
	cd haskell && stack test
	cd haskell && ./test/test_modem.sh
haskell-lint: hlint
	cd haskell && hlint app src test
haskell-docs:
	cd haskell && \
	  find app src -type f -name '*.hs' -print0 | \
	    xargs -0 \
	      stack exec -- \
	        haddock --html --hyperlinked-source --odir=dist/docs
haskell-benchmark:
	cd haskell && stack bench
hlint:
	which hlint || ( cd haskell && stack install hlint )

.PHONY: python-build python-test python-lint
python-build:
	cd python && ./build.sh

python-test:
	cd python && ./test.sh

python-lint:
	cd python && ./lint.sh --all
